
### Коллекции
- Коллекции в Java: о чём многие забывают
https://habr.com/ru/post/267389/
- Справочник по Java Collections Framework
https://habr.com/ru/post/237043/
- Структуры данных в картинках. LinkedHashMap (серия статей)
https://habr.com/en/post/129037/  


### Интересно:
- Мягкие ссылки на страже доступной памяти или как экономить память правильно
https://habr.com/ru/post/169883/
- Беззнаковая арифметика в Java
https://habr.com/ru/post/225901/
- Java Bytecode Fundamentals
https://habr.com/ru/post/111456/
- Тонкости оператора switch
https://habr.com/ru/post/174065/
- Как работает hashCode() по умолчанию?
https://habr.com/ru/company/mailru/blog/321306/


### Не забудь
- 10 ошибок зачастую допускаемых Java разработчиками
https://javarush.ru/groups/posts/855-10-oshibok-zachastuju-dopuskaemihkh-java-razrabotchikami 

### Gitlab CI
- Gitlab-CI
https://habr.com/ru/company/southbridge/blog/306596/
- Введение в GitLab CI
https://habr.com/ru/company/softmart/blog/309380/
- Непрерывная интеграция и развертывание Docker в GitLab CI
https://habr.com/ru/post/344324/

