package hw2;

import java.util.ArrayList;

public class City {
    private final String name;
    private final Coordinate coordinate;
    private final Country country;
    private ArrayList<Hotel> hotels;

    public City(String name, Coordinate coordinate, Country country) {
        this.name = name;
        this.coordinate = coordinate;
        this.country = country;
        hotels = new ArrayList<Hotel>();
    }

    public void addHotel(Hotel hotel) {
        this.hotels.add(hotel);
    }

    public ArrayList<Hotel> getHotels() {
        return hotels;
    }

    public String getName() {
        return name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Country getCountry() {
        return country;
    }
}
