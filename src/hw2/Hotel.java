package hw2;

public class Hotel {
    private final String name;
    private final int stars;
    private final long price;
    private final City city;

    public Hotel(String name, int stars, long price, City city) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getStars() {
        return stars;
    }

    public long getPrice() {
        return price;
    }

    public City getCity() {
        return city;
    }
}
