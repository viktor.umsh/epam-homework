package hw2;

import java.util.ArrayList;

public class TourProvider {
    private final long DISTANCE_COST;
    private final long SERVICE_COST;

    private ArrayList<Country> countries;

    public TourProvider(long DISTANCE_COST, long SERVICE_COST) {
        this.DISTANCE_COST = DISTANCE_COST;
        this.SERVICE_COST = SERVICE_COST;
        countries = new ArrayList<Country>();
    }

    public ArrayList<City> getCities(ArrayList<Country> requiredCountryes) {
        ArrayList<City> requiredCities = new ArrayList<City>();
        for (Country country : requiredCountryes) {
            requiredCities.addAll(country.getCities());
        }
        return requiredCities;
    }

    public ArrayList<Hotel> getHotels(ArrayList<City> requiredCity) {
        ArrayList<Hotel> requiredHotels = new ArrayList<Hotel>();
        for (City city : requiredCity) {
            requiredHotels.addAll(city.getHotels());
        }
        return requiredHotels;
    }

    public ArrayList<Order> generateOrder(User user, ArrayList<Hotel> hotels) {
        ArrayList<Order> orders = new ArrayList<Order>();
        for (Hotel hotel : hotels) {
            orders.add(new Order(calcOrder(user, hotel), hotel));
        }
        return orders;
    }

    private long calcOrder(User user, Hotel hotel) {
        return SERVICE_COST + hotel.getPrice() +
                DISTANCE_COST * user.getCity().getCoordinate().lengthTo(hotel.getCity().getCoordinate());
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }
}
