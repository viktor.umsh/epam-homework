package hw8_tour.city.service;

import hw8_tour.base.service.BaseService;
import hw8_tour.city.domain.City;

public abstract class CityService extends BaseService<City> {
}
