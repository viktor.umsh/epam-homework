package hw8_tour.city.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.city.domain.City;

public interface CityRepo extends BaseRepo<City> {
}
