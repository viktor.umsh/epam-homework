package hw8_tour.city.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.city.domain.City;
import hw8_tour.city.repo.CityIdGenerator;
import hw8_tour.city.repo.CityRepo;

import static hw8_tour.storage.data.ArrayDataStore.cities;

public class CityArrayRepo extends ArrayRepo<City> implements CityRepo {
    public CityArrayRepo() {
        dataArray = cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }
}
