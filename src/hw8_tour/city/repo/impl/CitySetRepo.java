package hw8_tour.city.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.city.domain.City;
import hw8_tour.city.repo.CityIdGenerator;
import hw8_tour.city.repo.CityRepo;
import hw8_tour.storage.data.SetDataStore;

public class CitySetRepo extends SetRepo<City> implements CityRepo {
    public CitySetRepo() {
        dataSet = SetDataStore.cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }

}
