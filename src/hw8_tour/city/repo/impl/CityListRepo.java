package hw8_tour.city.repo.impl;

import hw8_tour.base.repo.BaseIdGenerator;
import hw8_tour.base.repo.ListRepo;
import hw8_tour.city.domain.City;
import hw8_tour.city.repo.CityRepo;

import static hw8_tour.storage.data.ListDataStorage.cities;

public class CityListRepo extends ListRepo<City> implements CityRepo {
    public CityListRepo() {
        dataList = cities;
    }

    @Override
    protected long generateNextId() {
        return BaseIdGenerator.nextId();
    }

}
