package hw8_tour.city.sort;

import hw8_tour.base.sort.BaseField;

public enum CitySortedField implements BaseField {
    NAME,
    COUNTRY,
    CLIMATE
}

