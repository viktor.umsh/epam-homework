package hw8_tour.city.search;

import hw8_tour.city.domain.City;
import hw8_tour.country.domain.Country;

public class CityCountrySearchCondition implements CitySearchCondition {
    private Country soughtCountry;

    public CityCountrySearchCondition(Country soughtCountry) {
        this.soughtCountry = soughtCountry;
    }

    @Override
    public boolean isFit(City city) {
        if (city.getCountry() == null) {
            return false;
        }
        return (city.getCountry().equals(soughtCountry));
    }
}
