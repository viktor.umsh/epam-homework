package hw8_tour.city.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.city.domain.City;

public interface CitySearchCondition extends BaseSearchCondition<City> {
}
