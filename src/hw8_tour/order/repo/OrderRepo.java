package hw8_tour.order.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.order.domain.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
