package hw8_tour.order.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.order.domain.Order;
import hw8_tour.order.repo.OrderIdGenerator;
import hw8_tour.order.repo.OrderRepo;

import static hw8_tour.storage.data.ArrayDataStore.orders;

public class OrderArrayRepo extends ArrayRepo<Order> implements OrderRepo {
    public OrderArrayRepo() {
        dataArray = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
