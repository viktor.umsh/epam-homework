package hw8_tour.order.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.order.domain.Order;
import hw8_tour.order.repo.OrderIdGenerator;
import hw8_tour.order.repo.OrderRepo;
import hw8_tour.storage.data.SetDataStore;

public class OrderSetRepo extends SetRepo<Order> implements OrderRepo {
    public OrderSetRepo() {
        dataSet = SetDataStore.orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
