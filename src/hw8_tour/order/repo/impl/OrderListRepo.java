package hw8_tour.order.repo.impl;

import hw8_tour.base.repo.ListRepo;
import hw8_tour.order.domain.Order;
import hw8_tour.order.repo.OrderIdGenerator;
import hw8_tour.order.repo.OrderRepo;

import static hw8_tour.storage.data.ListDataStorage.orders;

public class OrderListRepo extends ListRepo<Order> implements OrderRepo {
    public OrderListRepo() {
        dataList = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
