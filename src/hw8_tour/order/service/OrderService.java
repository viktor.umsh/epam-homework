package hw8_tour.order.service;

import hw8_tour.base.service.BaseService;
import hw8_tour.order.domain.Order;

public abstract class OrderService extends BaseService<Order> {
}
