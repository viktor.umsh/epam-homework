package hw8_tour.order.sort;

import hw8_tour.base.sort.BaseField;

public enum OrderSortedField implements BaseField {
    PRICE,
    HOTEL,
    CITY
}
