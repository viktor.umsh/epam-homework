package hw8_tour.order.sort;

import hw8_tour.base.sort.BaseSortCondition;
import hw8_tour.order.domain.Order;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class OrderSortCondition extends BaseSortCondition<Order> {
    private HashMap<Integer, OrderSortedField> sortedFields = new HashMap<>();

    public void addField(int priority, OrderSortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<Order> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    OrderSortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case HOTEL:
                            compareResult = o1.getHotel().getName().compareTo(o2.getHotel().getName());
                            break;

                        case CITY:
                            compareResult = o1.getHotel().getCity().getName().compareTo(o2.getHotel().getCity().getName());
                            break;

                        case PRICE:
                            compareResult = Long.compare(o1.getPrice(), o2.getPrice());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }
}
