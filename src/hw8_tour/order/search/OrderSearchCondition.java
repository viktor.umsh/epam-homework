package hw8_tour.order.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.order.domain.Order;

public interface OrderSearchCondition extends BaseSearchCondition<Order> {
}
