package hw8_tour.country.service;

import hw8_tour.base.service.BaseService;
import hw8_tour.country.domain.Country;

public abstract class CountryService extends BaseService<Country> {
}
