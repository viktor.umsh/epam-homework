package hw8_tour.country.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.country.domain.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
