package hw8_tour.country.repo.impl;

import hw8_tour.base.repo.ListRepo;
import hw8_tour.country.domain.Country;
import hw8_tour.country.repo.CountryIdGenerator;
import hw8_tour.country.repo.CountryRepo;

import static hw8_tour.storage.data.ListDataStorage.countries;

public class CountryListRepo extends ListRepo<Country> implements CountryRepo {
    public CountryListRepo() {
        dataList = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
