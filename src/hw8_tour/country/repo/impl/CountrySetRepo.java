package hw8_tour.country.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.country.domain.Country;
import hw8_tour.country.repo.CountryIdGenerator;
import hw8_tour.country.repo.CountryRepo;
import hw8_tour.storage.data.SetDataStore;

public class CountrySetRepo extends SetRepo<Country> implements CountryRepo {
    public CountrySetRepo() {
        dataSet = SetDataStore.countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
