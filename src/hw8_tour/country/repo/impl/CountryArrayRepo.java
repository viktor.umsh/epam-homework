package hw8_tour.country.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.country.domain.Country;
import hw8_tour.country.repo.CountryIdGenerator;
import hw8_tour.country.repo.CountryRepo;

import static hw8_tour.storage.data.ArrayDataStore.countries;

public class CountryArrayRepo extends ArrayRepo<Country> implements CountryRepo {
    public CountryArrayRepo() {
        dataArray = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
