package hw8_tour.country.domain;

import hw8_tour.base.domain.BaseDomain;
import hw8_tour.city.domain.City;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Country extends BaseDomain {
    private final String name;
    private ArrayList<City> cities;

    public Country(String name) {
        this.name = name;
        cities = new ArrayList<>();
    }

    public void addCity(City city) {
        if (!cities.contains(city)) {
            cities.add(city);
        }
    }

    public boolean deleteCity(City city) {
        return cities.remove(city);
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof Country) || this == something) {
            return;
        }
        Country someCountry = (Country) something;
        someCountry.getCities().removeAll(cities);
        cities.addAll(someCountry.getCities());
    }

    public Collection<City> getCities() {
        return cities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
