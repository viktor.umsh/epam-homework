package hw8_tour.country.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.country.domain.Country;

public interface CountrySearchCondition extends BaseSearchCondition<Country> {
}
