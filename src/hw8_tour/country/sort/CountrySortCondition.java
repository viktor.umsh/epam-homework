package hw8_tour.country.sort;

import hw8_tour.base.sort.BaseSortCondition;
import hw8_tour.country.domain.Country;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class CountrySortCondition extends BaseSortCondition<Country> {

    private HashMap<Integer, CountrySortedField> sortedFields = new HashMap<>();

    public void addField(int priority, CountrySortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<Country> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<Country>() {
            @Override
            public int compare(Country o1, Country o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    CountrySortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case NAME:
                            compareResult = o1.getName().compareTo(o2.getName());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }
}
