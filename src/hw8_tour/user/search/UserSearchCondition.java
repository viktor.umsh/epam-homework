package hw8_tour.user.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.user.domain.AbstractUser;

public interface UserSearchCondition extends BaseSearchCondition<AbstractUser> {
}
