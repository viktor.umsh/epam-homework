package hw8_tour.user.search;

import hw8_tour.user.domain.AbstractUser;

public class UserNameSearchCondition implements UserSearchCondition {
    private String namePattern;

    public UserNameSearchCondition(String namePattern) {
        this.namePattern = namePattern;
    }

    @Override
    public boolean isFit(AbstractUser user) {
        if (user.getName() == null) {
            return false;
        }
        return user.getName().contains(namePattern);
    }
}
