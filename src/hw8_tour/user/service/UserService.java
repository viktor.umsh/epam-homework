package hw8_tour.user.service;

import hw8_tour.base.service.BaseService;
import hw8_tour.user.domain.AbstractUser;

public abstract class UserService extends BaseService<AbstractUser> {
}
