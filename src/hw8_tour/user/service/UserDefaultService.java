package hw8_tour.user.service;

import hw8_tour.city.service.CityService;
import hw8_tour.storage.services.ServicesStorage;
import hw8_tour.user.domain.AbstractUser;
import hw8_tour.user.repo.UserRepo;

public class UserDefaultService extends UserService {
    private CityService cityService;

    public UserDefaultService(UserRepo userRepo) {
        repository = userRepo;
    }

    @Override
    public void add(AbstractUser user) {
        if (basicAdd(user)) {
            if (cityService.findByEntity(user.getCity()) == null) {
                cityService.add(user.getCity());
            }
        }
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
    }

}
