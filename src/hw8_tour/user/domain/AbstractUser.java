package hw8_tour.user.domain;

import hw8_tour.base.domain.BaseDomain;
import hw8_tour.city.domain.City;
import hw8_tour.country.domain.Country;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.order.domain.Order;

import java.util.List;
import java.util.Objects;

abstract public class AbstractUser extends BaseDomain {
    private String name;
    private City city;

    public AbstractUser(String name, City city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    abstract public List<Country> chooseCountries(List<Country> countries);

    abstract public List<City> chooseCities(List<City> cities);

    abstract public List<Hotel> chooseHotels(List<Hotel> hotels);

    abstract public Order chooseTour(List<Order> orders);

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof AbstractUser) || this == something) {
            return;
        }
        AbstractUser someUser = (AbstractUser) something;
        if (someUser.city != null) {
            this.city = someUser.city;
        }
        if (someUser.name != null) {
            this.name = someUser.name;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractUser that = (AbstractUser) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }

    @Override
    public String toString() {
        return "AbstractUser{" +
                "name='" + name + '\'' +
                ", city=" + city +
                ", id=" + id +
                '}';
    }
}
