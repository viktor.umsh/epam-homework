package hw8_tour.user.sort;

import hw8_tour.base.sort.BaseField;

public enum UserSortedField implements BaseField {
    NAME,
    CITY,
    COUNTRY
}
