package hw8_tour.user.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.storage.data.SetDataStore;
import hw8_tour.user.domain.AbstractUser;
import hw8_tour.user.repo.UserIdGenerator;
import hw8_tour.user.repo.UserRepo;

public class UserSetRepo extends SetRepo<AbstractUser> implements UserRepo {
    public UserSetRepo() {
        dataSet = SetDataStore.users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
