package hw8_tour.user.repo.impl;

import hw8_tour.base.repo.ListRepo;
import hw8_tour.user.domain.AbstractUser;
import hw8_tour.user.repo.UserIdGenerator;
import hw8_tour.user.repo.UserRepo;

import static hw8_tour.storage.data.ListDataStorage.users;

public class UserListRepo extends ListRepo<AbstractUser> implements UserRepo {
    public UserListRepo() {
        dataList = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
