package hw8_tour.user.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.user.domain.AbstractUser;
import hw8_tour.user.repo.UserIdGenerator;
import hw8_tour.user.repo.UserRepo;

import static hw8_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo extends ArrayRepo<AbstractUser> implements UserRepo {
    public UserArrayRepo() {
        dataArray = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
