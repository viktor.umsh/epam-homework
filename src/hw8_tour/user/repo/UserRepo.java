package hw8_tour.user.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.user.domain.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
