package hw8_tour.storage.data;

import hw8_tour.storage.StorageType;

public class DataStoreFactory {

    public static void init(StorageType storageType) {
        switch (storageType) {
            case LIST:
                ListDataStorage.initStore();
                break;
            case ARRAY:
                ArrayDataStore.initStore();
                break;
            case SET:
                SetDataStore.initStore();
                break;
        }
    }
}
