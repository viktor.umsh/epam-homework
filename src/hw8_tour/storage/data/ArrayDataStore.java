package hw8_tour.storage.data;

import hw8_tour.city.domain.City;
import hw8_tour.country.domain.Country;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.order.domain.Order;
import hw8_tour.statistic.Statistic;
import hw8_tour.tourprovider.domain.TourProvider;
import hw8_tour.user.domain.AbstractUser;
import hw8_tour.util.Array;

public class ArrayDataStore {
    public static Array<Country> countries;
    public static Array<City> cities;
    public static Array<Hotel> hotels;
    public static Array<TourProvider> tourProviders;
    public static Array<Order> orders;
    public static Array<AbstractUser> users;
    public static Array<Statistic> statistics;


    static void initStore() {
        countries = new Array<>();
        cities = new Array<>();
        hotels = new Array<>();
        tourProviders = new Array<>();
        orders = new Array<>();
        users = new Array<>();
        statistics = new Array<>();
    }
}
