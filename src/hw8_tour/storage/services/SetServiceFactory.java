package hw8_tour.storage.services;

import hw8_tour.city.repo.CityRepo;
import hw8_tour.city.repo.impl.CitySetRepo;
import hw8_tour.city.service.CityDefaultService;
import hw8_tour.country.repo.CountryRepo;
import hw8_tour.country.repo.impl.CountrySetRepo;
import hw8_tour.country.service.CountryDefaultService;
import hw8_tour.hotel.repo.HotelRepo;
import hw8_tour.hotel.repo.impl.HotelSetRepo;
import hw8_tour.hotel.service.HotelDefaultService;
import hw8_tour.order.repo.OrderRepo;
import hw8_tour.order.repo.impl.OrderSetRepo;
import hw8_tour.order.service.OrderDefaultService;
import hw8_tour.tourprovider.repo.TourProviderRepo;
import hw8_tour.tourprovider.repo.impl.TourProviderSetRepo;
import hw8_tour.tourprovider.service.TourProviderDefaultService;
import hw8_tour.user.repo.UserRepo;
import hw8_tour.user.repo.impl.UserSetRepo;
import hw8_tour.user.service.UserDefaultService;


public class SetServiceFactory extends ServiceFactory {
    public SetServiceFactory() {
        UserRepo userRepo = new UserSetRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CitySetRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountrySetRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelSetRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderSetRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderSetRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
