package hw8_tour.storage.services;

import hw8_tour.city.repo.CityRepo;
import hw8_tour.city.repo.impl.CityArrayRepo;
import hw8_tour.city.service.CityDefaultService;
import hw8_tour.country.repo.CountryRepo;
import hw8_tour.country.repo.impl.CountryArrayRepo;
import hw8_tour.country.service.CountryDefaultService;
import hw8_tour.hotel.repo.HotelRepo;
import hw8_tour.hotel.repo.impl.HotelArrayRepo;
import hw8_tour.hotel.service.HotelDefaultService;
import hw8_tour.order.repo.OrderRepo;
import hw8_tour.order.repo.impl.OrderArrayRepo;
import hw8_tour.order.service.OrderDefaultService;
import hw8_tour.tourprovider.repo.TourProviderRepo;
import hw8_tour.tourprovider.repo.impl.TourProviderArrayRepo;
import hw8_tour.tourprovider.service.TourProviderDefaultService;
import hw8_tour.user.repo.UserRepo;
import hw8_tour.user.repo.impl.UserArrayRepo;
import hw8_tour.user.service.UserDefaultService;


public class ArrayServiceFactory extends ServiceFactory {

    public ArrayServiceFactory() {
        UserRepo userRepo = new UserArrayRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityArrayRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryArrayRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelArrayRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderArrayRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderArrayRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
