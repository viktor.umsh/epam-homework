package hw8_tour.storage.services;

import hw8_tour.city.repo.CityRepo;
import hw8_tour.city.repo.impl.CityListRepo;
import hw8_tour.city.service.CityDefaultService;
import hw8_tour.country.repo.CountryRepo;
import hw8_tour.country.repo.impl.CountryListRepo;
import hw8_tour.country.service.CountryDefaultService;
import hw8_tour.hotel.repo.HotelRepo;
import hw8_tour.hotel.repo.impl.HotelListRepo;
import hw8_tour.hotel.service.HotelDefaultService;
import hw8_tour.order.repo.OrderRepo;
import hw8_tour.order.repo.impl.OrderListRepo;
import hw8_tour.order.service.OrderDefaultService;
import hw8_tour.tourprovider.repo.TourProviderRepo;
import hw8_tour.tourprovider.repo.impl.TourProviderListRepo;
import hw8_tour.tourprovider.service.TourProviderDefaultService;
import hw8_tour.user.repo.UserRepo;
import hw8_tour.user.repo.impl.UserListRepo;
import hw8_tour.user.service.UserDefaultService;


public class ListServiceFactory extends ServiceFactory {
    public ListServiceFactory() {
        UserRepo userRepo = new UserListRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityListRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryListRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelListRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderListRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderListRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
