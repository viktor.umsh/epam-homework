package hw8_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
