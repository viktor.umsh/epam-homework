package hw8_tour.hotel.search;

import hw8_tour.hotel.domain.Hotel;

public class HotelPriceMoreThenSearchCondition implements HotelSearchCondition {
    private long priceMoreThen;

    public HotelPriceMoreThenSearchCondition(long priceMoreThen) {
        this.priceMoreThen = priceMoreThen;
    }

    @Override
    public boolean isFit(Hotel hotel) {
        return (hotel.getPrice() >= priceMoreThen);
    }
}
