package hw8_tour.hotel.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.hotel.domain.Hotel;

public interface HotelSearchCondition extends BaseSearchCondition<Hotel> {
}
