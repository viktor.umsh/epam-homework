package hw8_tour.hotel.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.hotel.domain.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
