package hw8_tour.hotel.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.hotel.repo.HotelIdGenerator;
import hw8_tour.hotel.repo.HotelRepo;

import static hw8_tour.storage.data.ArrayDataStore.hotels;

public class HotelArrayRepo extends ArrayRepo<Hotel> implements HotelRepo {
    public HotelArrayRepo() {
        dataArray = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
