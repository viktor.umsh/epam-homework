package hw8_tour.hotel.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.hotel.repo.HotelIdGenerator;
import hw8_tour.hotel.repo.HotelRepo;
import hw8_tour.storage.data.SetDataStore;

public class HotelSetRepo extends SetRepo<Hotel> implements HotelRepo {
    public HotelSetRepo() {
        dataSet = SetDataStore.hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
