package hw8_tour.hotel.repo.impl;

import hw8_tour.base.repo.ListRepo;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.hotel.repo.HotelIdGenerator;
import hw8_tour.hotel.repo.HotelRepo;

import static hw8_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo extends ListRepo<Hotel> implements HotelRepo {
    public HotelListRepo() {
        dataList = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
