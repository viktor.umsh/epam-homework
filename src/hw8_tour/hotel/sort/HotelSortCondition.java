package hw8_tour.hotel.sort;

import hw8_tour.base.sort.BaseSortCondition;
import hw8_tour.hotel.domain.Hotel;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class HotelSortCondition extends BaseSortCondition<Hotel> {

    private HashMap<Integer, HotelSortedField> sortedFields = new HashMap<>();

    public void addField(int priority, HotelSortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<Hotel> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<Hotel>() {
            @Override
            public int compare(Hotel o1, Hotel o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    HotelSortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case NAME:
                            compareResult = o1.getName().compareTo(o2.getName());
                            break;

                        case CITY:
                            compareResult = o1.getCity().getName().compareTo(o2.getCity().getName());
                            break;

                        case COUNTRY:
                            compareResult = o1.getCity().getCountry().getName().compareTo(o2.getCity().getCountry().getName());
                            break;

                        case PRICE:
                            compareResult = Long.compare(o1.getPrice(), o2.getPrice());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }
}
