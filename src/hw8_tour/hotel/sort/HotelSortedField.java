package hw8_tour.hotel.sort;

import hw8_tour.base.sort.BaseField;

public enum HotelSortedField implements BaseField {
    NAME,
    COUNTRY,
    CITY,
    PRICE
}
