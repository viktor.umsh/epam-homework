package hw8_tour.hotel.service;

import hw8_tour.base.service.BaseService;
import hw8_tour.hotel.domain.Hotel;

public abstract class HotelService extends BaseService<Hotel> {
}
