package hw8_tour.hotel.service;

import hw8_tour.city.service.CityService;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.hotel.repo.HotelRepo;
import hw8_tour.storage.services.ServicesStorage;

public class HotelDefaultService extends HotelService {

    private CityService cityService;

    public HotelDefaultService(HotelRepo hotelRepo) {
        repository = hotelRepo;
    }

    @Override
    public void add(Hotel hotel) {
        if (basicAdd(hotel)) {
            if (hotel.getCity() != null) {
                if (cityService.findByEntity(hotel.getCity()) == null) {
                    cityService.add(hotel.getCity());
                }
                if (!hotel.getCity().getHotels().contains(hotel)) {
                    hotel.getCity().addHotel(hotel);
                }
            }
        }
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
    }


    @Override
    public boolean delete(Hotel hotel) {
        hotel.getCity().deleteHotel(hotel);
//        if (hotel.getCity().getHotels().size() == 0) {
//            cityService.delete(hotel.getCity());
//        }
        return repository.delete(hotel);
    }
}
