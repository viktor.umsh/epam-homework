package hw8_tour.tourprovider.repo;

import hw8_tour.base.repo.BaseRepo;
import hw8_tour.tourprovider.domain.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
