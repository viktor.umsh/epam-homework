package hw8_tour.tourprovider.repo.impl;

import hw8_tour.base.repo.ListRepo;
import hw8_tour.tourprovider.domain.TourProvider;
import hw8_tour.tourprovider.repo.TourProviderIdGenerator;
import hw8_tour.tourprovider.repo.TourProviderRepo;

import static hw8_tour.storage.data.ListDataStorage.tourProviders;

public class TourProviderListRepo extends ListRepo<TourProvider> implements TourProviderRepo {
    public TourProviderListRepo() {
        dataList = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
