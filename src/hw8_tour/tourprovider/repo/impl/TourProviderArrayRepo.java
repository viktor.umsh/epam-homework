package hw8_tour.tourprovider.repo.impl;

import hw8_tour.base.repo.ArrayRepo;
import hw8_tour.tourprovider.domain.TourProvider;
import hw8_tour.tourprovider.repo.TourProviderIdGenerator;
import hw8_tour.tourprovider.repo.TourProviderRepo;

import static hw8_tour.storage.data.ArrayDataStore.tourProviders;


public class TourProviderArrayRepo extends ArrayRepo<TourProvider> implements TourProviderRepo {
    public TourProviderArrayRepo() {
        dataArray = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
