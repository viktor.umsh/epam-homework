package hw8_tour.tourprovider.repo.impl;

import hw8_tour.base.repo.SetRepo;
import hw8_tour.storage.data.SetDataStore;
import hw8_tour.tourprovider.domain.TourProvider;
import hw8_tour.tourprovider.repo.TourProviderIdGenerator;
import hw8_tour.tourprovider.repo.TourProviderRepo;

public class TourProviderSetRepo extends SetRepo<TourProvider> implements TourProviderRepo {
    public TourProviderSetRepo() {
        dataSet = SetDataStore.tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
