package hw8_tour.tourprovider.domain;

import hw8_tour.base.domain.BaseDomain;
import hw8_tour.city.domain.City;
import hw8_tour.country.domain.Country;
import hw8_tour.hotel.domain.Hotel;
import hw8_tour.order.domain.Order;
import hw8_tour.user.domain.AbstractUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TourProvider extends BaseDomain {
    private long DISTANCE_COST;
    private long SERVICE_COST;
    private String name;
    private ArrayList<Country> countries;

    public TourProvider(String name, long DISTANCE_COST, long SERVICE_COST) {
        this.name = name;
        this.DISTANCE_COST = DISTANCE_COST;
        this.SERVICE_COST = SERVICE_COST;
        countries = new ArrayList<>();
    }

    public List<City> getCities(List<Country> requiredCountries) {
        ArrayList<City> requiredCities = new ArrayList<>();
        for (Country country : requiredCountries) {
            requiredCities.addAll(country.getCities());
        }
        return requiredCities;
    }

    public List<Hotel> getHotels(List<City> requiredCity) {
        ArrayList<Hotel> requiredHotels = new ArrayList<>();
        for (City city : requiredCity) {
            requiredHotels.addAll(city.getHotels());
        }
        return requiredHotels;
    }

    public List<Order> generateOrder(AbstractUser user, List<Hotel> hotels) {
        ArrayList<Order> orders = new ArrayList<>();
        for (Hotel hotel : hotels) {
            orders.add(new Order(calcOrder(user, hotel), hotel));
        }
        return orders;
    }

    private long calcOrder(AbstractUser user, Hotel hotel) {
        return SERVICE_COST + hotel.getPrice() +
                DISTANCE_COST * user.getCity().getCoordinate().lengthTo(hotel.getCity().getCoordinate());
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof TourProvider) || this == something) {
            return;
        }
        TourProvider someProvider = (TourProvider) something;
        if (someProvider.DISTANCE_COST != 0) {
            this.DISTANCE_COST = someProvider.DISTANCE_COST;
        }
        if (someProvider.SERVICE_COST != 0) {
            this.SERVICE_COST = someProvider.SERVICE_COST;
        }
        if (someProvider.name != null) {
            this.name = someProvider.name;
        }

        someProvider.countries.removeAll(this.countries);
        this.countries.addAll(someProvider.countries);
    }

    public long getDISTANCE_COST() {
        return DISTANCE_COST;
    }

    public long getSERVICE_COST() {
        return SERVICE_COST;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TourProvider that = (TourProvider) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TourProvider{" +
                "DISTANCE_COST=" + DISTANCE_COST +
                ", SERVICE_COST=" + SERVICE_COST +
                ", name='" + name + '\'' +
                ", countries=" + countries +
                ", id=" + id +
                '}';
    }
}
