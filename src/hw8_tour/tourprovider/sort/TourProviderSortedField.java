package hw8_tour.tourprovider.sort;

import hw8_tour.base.sort.BaseField;

public enum TourProviderSortedField implements BaseField {
    NAME,
    SERVICE_COST
}
