package hw8_tour.tourprovider.search;

import hw8_tour.base.search.BaseSearchCondition;
import hw8_tour.tourprovider.domain.TourProvider;

public interface TourProviderSearchCondition extends BaseSearchCondition<TourProvider> {

}
