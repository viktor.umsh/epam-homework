package hw8_tour.base.parser;

public abstract class BasicParser {
    private String regex = "\\|";

    protected String[] split(String line) {
        line = line.replaceAll("\\s", "");
        return line.split(regex);
    }

    public abstract void parseString(String line);
}
