package hw8_tour.base.repo;

import hw8_tour.base.domain.BaseDomain;
import hw8_tour.util.Array;

import java.util.Iterator;

public abstract class ArrayRepo<Type extends BaseDomain> implements BaseRepo<Type> {

    protected Array<Type> dataArray;

    protected abstract long generateNextId();

    public void add(Type something) {
        if (findByEntity(something) == null) {
            dataArray.add(something);
            something.setId(generateNextId());
        }
    }

    public Type get(int index) {
        return dataArray.get(index);
    }

    public Type findById(long id) {
        for (Type something : dataArray) {
            if (something.getId() == id) {
                return something;
            }
        }
        return null;
    }

    public Type findByName(String name) {
        for (Type something : dataArray) {
            if (something.getName().equals(name)) {
                return something;
            }
        }
        return null;
    }

    public Type findByEntity(Type something) {
        for (Type elem : dataArray) {
            if (elem.equals(something)) {
                return elem;
            }
        }
        return null;
    }

    @Override
    public void update(Type something) {
        Type whatUpdate = findByEntity(something);
        if (whatUpdate == null) {
            add(something);
        } else {
            whatUpdate.update(something);
        }
    }

    public boolean delete(Type something) {
        return dataArray.remove(something);
    }

    public Type delete(int index) {
        return dataArray.remove(index);
    }

    @Override
    public Iterator<Type> iterator() {
        return new Iterator<Type>() {
            private int current = 0;

            @Override
            public boolean hasNext() {
                return current < dataArray.size();
            }

            @Override
            public Type next() {
                return dataArray.get(current++);
            }
        };
    }

    @Override
    public int size() {
        return dataArray.size();
    }
}
