package hw8_tour.base.repo;

public interface BaseRepo<Type> extends Iterable<Type> { // extends Iterable<Type>

    public void add(Type something);

    public Type get(int index);

    public Type findById(long id);

    public Type findByName(String name);

    public Type findByEntity(Type something);

    public void update(Type something);

    public boolean delete(Type something);

    public Type delete(int index);

    public int size();
}
