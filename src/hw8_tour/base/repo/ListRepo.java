package hw8_tour.base.repo;

import hw8_tour.base.domain.BaseDomain;

import java.util.Iterator;
import java.util.List;

public abstract class ListRepo<Type extends BaseDomain> implements BaseRepo<Type> {
    protected List<Type> dataList;

    protected abstract long generateNextId();

    public void add(Type something) {
        if (findByEntity(something) == null) {
            dataList.add(something);
            something.setId(generateNextId());
        }
    }

    public Type get(int index) {
        return dataList.get(index);
    }

    public Type findById(long id) {
        for (Type something : dataList) {
            if (something.getId() == id) {
                return something;
            }
        }
        return null;
    }

    public Type findByName(String name) {
        for (Type something : dataList) {
            if (something.getName().equals(name)) {
                return something;
            }
        }
        return null;
    }

    @Override
    public void update(Type something) {
        Type whatUpdate = findByEntity(something);
        if (whatUpdate == null) {
            add(something);
        } else {
            whatUpdate.update(something);
        }
    }

    @Override
    public Type findByEntity(Type something) {
        int somethingIndex = dataList.indexOf(something);
        if (somethingIndex == -1) {
            return null;
        }
        return dataList.get(somethingIndex);
    }

    public boolean delete(Type something) {
        return dataList.remove(something);
    }

    public Type delete(int index) {
        return dataList.remove(index);
    }

    @Override
    public Iterator<Type> iterator() {
        return dataList.iterator();
    }

    @Override
    public int size() {
        return dataList.size();
    }
}
