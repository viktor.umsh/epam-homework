package hw8_tour.base.repo;

import hw8_tour.base.domain.BaseDomain;

import java.util.Iterator;
import java.util.Set;

public abstract class SetRepo<Type extends BaseDomain> implements BaseRepo<Type> {

    protected Set<Type> dataSet;

    protected abstract long generateNextId();

    @Override
    public void add(Type something) {
        dataSet.add(something);
        something.setId(generateNextId());
    }

    /**
     * NOT SUPPORTED!
     */
    @Override
    public Type get(int index) {
        return null;
    }

    @Override
    public Type findById(long id) {
        for (Type something : dataSet) {
            if (something.getId() == id) {
                return something;
            }
        }
        return null;
    }

    @Override
    public Type findByName(String name) {
        for (Type something : dataSet) {
            if (something.getName().equals(name)) {
                return something;
            }
        }
        return null;
    }

    @Override
    public Type findByEntity(Type something) {
        if (dataSet.contains(something)) {
            for (Type elem : dataSet) {
                if (elem.equals(something)) {
                    return elem;
                }
            }
        }
        return null;
    }

    @Override
    public void update(Type something) {
        Type whatUpdate = findByEntity(something);
        if (whatUpdate == null) {
            add(something);
        } else {
            whatUpdate.update(something);
        }
    }

    @Override
    public boolean delete(Type something) {
        return dataSet.remove(something);
    }

    /**
     * NOT SUPPORTED!
     */
    @Override
    public Type delete(int index) {
        return null;
    }

    @Override
    public Iterator<Type> iterator() {
        return dataSet.iterator();
    }

    @Override
    public int size() {
        return dataSet.size();
    }
}
