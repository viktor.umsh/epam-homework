package hw8_tour.base.domain;

import hw8_tour.statistic.Statistic;

public abstract class Statistical {
    private Statistic statistic = new Statistic();

    public Statistic getStatistic() {
        return statistic;
    }

    public void updateStatistic(long newValue) {
        statistic.updateStatistic(newValue);
    }
}

