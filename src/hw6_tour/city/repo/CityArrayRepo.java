package hw6_tour.city.repo;

import hw6_tour.city.City;

import static hw6_tour.storage.data.ArrayDataStore.cities;


public class CityArrayRepo implements CityRepo {
    @Override
    public void add(City city) {
        cities.add(city);
    }

    @Override
    public City get(int index) {
        return cities.get(index);
    }

    @Override
    public City findById(long id) {
        for (City city : cities) {
            if (city.getId() == id) {
                return city;
            }
        }
        return null;
    }

    @Override
    public City findByName(String name) {
        for (City city : cities) {
            if (city.getName().equals(name)) {
                return city;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(City city) {
        for (int i = 0; i < cities.size(); i++) {
            if (cities.get(i).equals(city)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean delete(City city) {
        int index = findByEntity(city);
        if (index != -1) {
            delete(index);
            return true;
        }
        return false;
    }

    @Override
    public City delete(int index) {
        return cities.remove(index);
    }
}
