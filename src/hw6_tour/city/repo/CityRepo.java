package hw6_tour.city.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.city.City;

public interface CityRepo extends BaseRepo<City> {
}
