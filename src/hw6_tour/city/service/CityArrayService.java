package hw6_tour.city.service;

import hw6_tour.city.City;
import hw6_tour.country.service.CountryService;
import hw6_tour.hotel.Hotel;
import hw6_tour.hotel.repo.HotelRepo;
import hw6_tour.storage.RepoStorage;
import hw6_tour.storage.ServicesStorage;

public class CityArrayService extends CityService {
    private CountryService countryMemoryService;
    private HotelRepo hotelMemoryRepo = RepoStorage.hotelRepo;

    @Override
    public void initServices() {
        countryMemoryService = ServicesStorage.getCountryService();
    }

    @Override
    public void add(City city) {
        if (basicAdd(city)) {
            if (!city.getCountry().getCities().contains(city)) {
                city.getCountry().addCity(city);
            }
            if (countryMemoryService.findByEntity(city.getCountry()) == -1) {
                countryMemoryService.add(city.getCountry());
            }
            for (Hotel hotel : city.getHotels()) {
                hotelMemoryRepo.add(hotel);
            }
        }
    }

    @Override
    public boolean delete(City city) {
        for (Hotel hotel : city.getHotels()) {
            hotelMemoryRepo.delete(hotel);
        }
        city.getCountry().deleteCity(city);
        return repository.delete(city);
    }
}
