package hw6_tour.city.service;

import hw6_tour.base.BaseService;
import hw6_tour.city.City;

public abstract class CityService extends BaseService<City> {
}
