package hw6_tour.user;

import hw6_tour.city.City;
import hw6_tour.country.Country;
import hw6_tour.hotel.Hotel;
import hw6_tour.order.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomMan extends AbstractUser {
    private Random random;

    public RandomMan(String name, City city) {
        super(name, city);
        random = new Random();
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        ArrayList<Country> choice = new ArrayList<Country>();
        choice.add(countries.get(random.nextInt(countries.size())));
        return choice;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        ArrayList<City> choice = new ArrayList<City>();
//        cities.
        choice.add(cities.get(random.nextInt(cities.size())));
        return choice;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        ArrayList<Hotel> choice = new ArrayList<Hotel>();
        choice.add(hotels.get(random.nextInt(hotels.size())));
        return choice;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return orders.get(random.nextInt(orders.size()));
    }
}
