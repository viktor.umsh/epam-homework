package hw6_tour.user.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.user.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
