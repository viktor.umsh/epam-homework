package hw6_tour.user.repo;

import hw6_tour.user.AbstractUser;

import static hw6_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo implements UserRepo {
    @Override
    public void add(AbstractUser abstractUser) {
        users.add(abstractUser);
    }

    @Override
    public AbstractUser get(int index) {
        return users.get(index);
    }

    @Override
    public AbstractUser findById(long id) {
        for (AbstractUser user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public AbstractUser findByName(String name) {
        for (AbstractUser user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(AbstractUser user) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).equals(user)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean delete(AbstractUser user) {
        int index = findByEntity(user);
        if (index != -1) {
            delete(index);
            return true;
        }
        return false;
    }

    @Override
    public AbstractUser delete(int index) {
        return users.remove(index);
    }
}
