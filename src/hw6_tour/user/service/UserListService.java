package hw6_tour.user.service;

import hw6_tour.city.service.CityService;
import hw6_tour.storage.RepoStorage;
import hw6_tour.storage.ServicesStorage;
import hw6_tour.user.AbstractUser;

public class UserListService extends UserService {
    private CityService cityMemoryService;

    public UserListService() {
        repository = RepoStorage.userRepo;
    }

    @Override
    public void add(AbstractUser user) {
        if (basicAdd(user)) {
            if (cityMemoryService.findByEntity(user.getCity()) == -1) {
                cityMemoryService.add(user.getCity());
            }
        }
    }

    @Override
    public void initServices() {
        cityMemoryService = ServicesStorage.getCityService();
    }


}
