package hw6_tour.user;

import hw6_tour.city.City;
import hw6_tour.country.Country;
import hw6_tour.hotel.Hotel;
import hw6_tour.order.Order;

import java.util.List;

public class RichMan extends AbstractUser {

    public RichMan(String name, City city) {
        super(name, city);
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        return null;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        return null;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        return null;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return null;
    }
}
