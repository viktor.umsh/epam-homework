package hw6_tour.hotel.service;

import hw6_tour.base.BaseService;
import hw6_tour.hotel.Hotel;

public abstract class HotelService extends BaseService<Hotel> {
}
