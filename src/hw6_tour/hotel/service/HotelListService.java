package hw6_tour.hotel.service;

import hw6_tour.hotel.Hotel;
import hw6_tour.storage.RepoStorage;

public class HotelListService extends HotelService {

    public HotelListService() {
        repository = RepoStorage.hotelRepo;
    }

    @Override
    public void add(Hotel hotel) {
        if (basicAdd(hotel)) {
            hotel.getCity().addHotel(hotel);
        }
    }

    @Override
    public void initServices() {
    }


    @Override
    public boolean delete(Hotel hotel) {
        hotel.getCity().deleteHotel(hotel);
        return repository.delete(hotel);
    }
}
