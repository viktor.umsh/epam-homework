package hw6_tour.hotel;

import hw6_tour.base.IdentifiableAndNamed;
import hw6_tour.city.City;

import java.util.Objects;

public class Hotel extends IdentifiableAndNamed {
    private static long lastId = 0;
    private final String name;
    private final int stars;
    private final long price;
    private final City city;

    public Hotel(String name, int stars, long price, City city) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.city = city;
        createId();
    }

    @Override
    protected void createId() {
        this.id = Hotel.lastId;
        Hotel.lastId++;
    }


    public String getName() {
        return name;
    }

    public int getStars() {
        return stars;
    }

    public long getPrice() {
        return price;
    }

    public City getCity() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return stars == hotel.stars &&
                price == hotel.price &&
                Objects.equals(name, hotel.name) &&
                Objects.equals(city, hotel.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, stars, price, city);
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "name='" + name + '\'' +
                ", stars=" + stars +
                ", price=" + price +
                ", city=" + city +
                ", id=" + id +
                '}';
    }
}
