package hw6_tour.hotel.repo;

import hw6_tour.hotel.Hotel;

import static hw6_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo implements HotelRepo {

    @Override
    public void add(Hotel hotel) {
        hotels.add(hotel);
    }

    @Override
    public Hotel get(int index) {
        return hotels.get(index);
    }

    @Override
    public Hotel findById(long id) {
        for (Hotel hotel : hotels) {
            if (hotel.getId() == id) {
                return hotel;
            }
        }
        return null;
    }

    @Override
    public Hotel findByName(String name) {
        for (Hotel hotel : hotels) {
            if (hotel.getName().equals(name)) {
                return hotel;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(Hotel hotel) {
        return hotels.indexOf(hotel);
    }

    @Override
    public boolean delete(Hotel hotel) {
        return hotels.remove(hotel);
    }

    @Override
    public Hotel delete(int index) {
        return hotels.remove(index);
    }
}
