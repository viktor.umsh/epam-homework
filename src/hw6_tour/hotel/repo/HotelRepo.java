package hw6_tour.hotel.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.hotel.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
