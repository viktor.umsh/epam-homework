package hw6_tour.tourProvider.service;

import hw6_tour.country.Country;
import hw6_tour.country.service.CountryService;
import hw6_tour.storage.RepoStorage;
import hw6_tour.storage.ServicesStorage;
import hw6_tour.tourProvider.TourProvider;


public class TourProviderListService extends TourProviderService {
    private CountryService countryMemoryService;

    public TourProviderListService() {
        repository = RepoStorage.tourProviderRepo;
    }

    @Override
    public void add(TourProvider tourProvider) {
        if (basicAdd(tourProvider)) {
            for (Country country : tourProvider.getCountries()) {
                countryMemoryService.add(country);
            }
        }
    }

    @Override
    public void initServices() {
        countryMemoryService = ServicesStorage.getCountryService();
    }

    @Override
    public boolean delete(TourProvider tourProvider) {
        for (Country country : tourProvider.getCountries()) {
            countryMemoryService.delete(country);
        }
        return tourProviderRepo.delete(tourProvider);
    }
}
