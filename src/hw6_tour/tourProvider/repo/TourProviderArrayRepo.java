package hw6_tour.tourProvider.repo;

import hw6_tour.tourProvider.TourProvider;

import static hw6_tour.storage.data.ArrayDataStore.tourProviders;

public class TourProviderArrayRepo implements TourProviderRepo {
    @Override
    public void add(TourProvider tourProvider) {
        tourProviders.add(tourProvider);
    }

    @Override
    public TourProvider get(int index) {
        return tourProviders.get(index);
    }

    @Override
    public TourProvider findById(long id) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getId() == id) {
                return tourProvider;
            }
        }
        return null;
    }

    @Override
    public TourProvider findByName(String name) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getName().equals(name)) {
                return tourProvider;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(TourProvider tourProvider) {
        for (int i = 0; i < tourProviders.size(); i++) {
            if (tourProviders.get(i).equals(tourProvider)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean delete(TourProvider tourProvider) {
        int index = findByEntity(tourProvider);
        if (index != -1) {
            delete(index);
            return true;
        }
        return false;
    }

    @Override
    public TourProvider delete(int index) {
        return tourProviders.remove(index);
    }
}
