package hw6_tour.tourProvider.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.tourProvider.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
