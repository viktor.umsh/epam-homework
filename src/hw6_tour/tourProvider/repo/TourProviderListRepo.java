package hw6_tour.tourProvider.repo;

import hw6_tour.tourProvider.TourProvider;

import static hw6_tour.storage.data.ListDataStorage.tourProviders;

public class TourProviderListRepo implements TourProviderRepo {
    @Override
    public void add(TourProvider tourProvider) {
        tourProviders.add(tourProvider);
    }

    @Override
    public TourProvider get(int index) {
        return tourProviders.get(index);
    }

    @Override
    public TourProvider findById(long id) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getId() == id) {
                return tourProvider;
            }
        }
        return null;
    }

    @Override
    public TourProvider findByName(String name) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getName().equals(name)) {
                return tourProvider;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(TourProvider tourProvider) {
        return tourProviders.indexOf(tourProvider);
    }

    @Override
    public boolean delete(TourProvider tourProvider) {
        return tourProviders.remove(tourProvider);
    }

    @Override
    public TourProvider delete(int index) {
        return tourProviders.remove(index);
    }

}
