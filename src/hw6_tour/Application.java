package hw6_tour;

import hw6_tour.city.City;
import hw6_tour.city.service.CityService;
import hw6_tour.country.Country;
import hw6_tour.country.service.CountryService;
import hw6_tour.dataReader.DataGenerator;
import hw6_tour.hotel.Hotel;
import hw6_tour.hotel.service.HotelService;
import hw6_tour.order.Order;
import hw6_tour.order.service.OrderService;
import hw6_tour.storage.ServicesStorage;
import hw6_tour.storage.StorageType;
import hw6_tour.tourProvider.TourProvider;
import hw6_tour.tourProvider.service.TourProviderService;
import hw6_tour.user.AbstractUser;
import hw6_tour.user.service.UserService;

import java.util.List;

public class Application {
    private UserService userService;
    private TourProviderService tourProviderService;
    private CountryService countryService;
    private CityService cityService;
    private HotelService hotelService;
    private OrderService orderService;

    public Order UserTourProviderInteraction(TourProvider tourProvider, AbstractUser user) {
        List<Country> chosenCountries = user.chooseCountries(tourProvider.getCountries());
        List<City> chosenCities = user.chooseCities(tourProvider.getCities(chosenCountries));
        List<Hotel> chosenHotels = user.chooseHotels(tourProvider.getHotels(chosenCities));
        return user.chooseTour(tourProvider.generateOrder(user, chosenHotels));
    }


    public Application() {
        ServicesStorage.setStorageType(StorageType.LIST);
        initServices();
        DataGenerator generator = new DataGenerator();
        generator.readFile();
    }

    private void initServices() {
        userService = ServicesStorage.getUserService();
        tourProviderService = ServicesStorage.getTourProviderService();
        countryService = ServicesStorage.getCountryService();
        cityService = ServicesStorage.getCityService();
        hotelService = ServicesStorage.getHotelService();
        orderService = ServicesStorage.getOrderService();
    }

    public void runApplication() {
        AbstractUser vasiliy = userService.findByName("Vasiliy");
        TourProvider grandTours = tourProviderService.findByName("GrandTours");
        Order order = UserTourProviderInteraction(grandTours, vasiliy);
        System.out.println(order);
    }

    public static void main(String[] args) {
        Application application = new Application();
        application.runApplication();
    }
}
