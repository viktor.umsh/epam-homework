package hw6_tour.storage.data;

import hw6_tour.city.City;
import hw6_tour.country.Country;
import hw6_tour.hotel.Hotel;
import hw6_tour.order.Order;
import hw6_tour.statistic.Statistic;
import hw6_tour.tourProvider.TourProvider;
import hw6_tour.user.AbstractUser;
import hw6_tour.utils.Array;

public class ArrayDataStore {
    public static Array<Country> countries;
    public static Array<City> cities;
    public static Array<Hotel> hotels;
    public static Array<TourProvider> tourProviders;
    public static Array<Order> orders;
    public static Array<AbstractUser> users;
    public static Array<Statistic> statistics;


    static void initStore() {
        countries = new Array<Country>();
        cities = new Array<City>();
        hotels = new Array<Hotel>();
        tourProviders = new Array<TourProvider>();
        orders = new Array<Order>();
        users = new Array<AbstractUser>();
        statistics = new Array<Statistic>();
    }
}
