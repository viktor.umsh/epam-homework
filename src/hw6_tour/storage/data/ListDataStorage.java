package hw6_tour.storage.data;

import hw6_tour.city.City;
import hw6_tour.country.Country;
import hw6_tour.hotel.Hotel;
import hw6_tour.order.Order;
import hw6_tour.statistic.Statistic;
import hw6_tour.tourProvider.TourProvider;
import hw6_tour.user.AbstractUser;

import java.util.ArrayList;
import java.util.List;

public class ListDataStorage {
    public static List<Country> countries;
    public static List<City> cities;
    public static List<Hotel> hotels;
    public static List<TourProvider> tourProviders;
    public static List<Order> orders;
    public static List<AbstractUser> users;
    public static List<Statistic> statistics;

    static void initStore() {
        countries = new ArrayList<Country>();
        cities = new ArrayList<City>();
        hotels = new ArrayList<Hotel>();
        tourProviders = new ArrayList<TourProvider>();
        orders = new ArrayList<Order>();
        users = new ArrayList<AbstractUser>();
        statistics = new ArrayList<Statistic>();
        users = new ArrayList<AbstractUser>();
    }

}
