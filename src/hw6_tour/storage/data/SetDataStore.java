package hw6_tour.storage.data;

import hw6_tour.city.City;
import hw6_tour.country.Country;
import hw6_tour.hotel.Hotel;
import hw6_tour.order.Order;
import hw6_tour.statistic.Statistic;
import hw6_tour.tourProvider.TourProvider;
import hw6_tour.user.AbstractUser;

import java.util.HashSet;
import java.util.Set;

public class SetDataStore {
    public static Set<Country> countries;
    public static Set<City> cities;
    public static Set<Hotel> hotels;
    public static Set<TourProvider> tourProviders;
    public static Set<Order> orders;
    public static Set<AbstractUser> users;
    public static Set<Statistic> statistics;

    static void initStore() {
        countries = new HashSet<Country>();
        cities = new HashSet<City>();
        hotels = new HashSet<Hotel>();
        tourProviders = new HashSet<TourProvider>();
        orders = new HashSet<Order>();
        users = new HashSet<AbstractUser>();
        statistics = new HashSet<Statistic>();
        users = new HashSet<AbstractUser>();
    }

}
