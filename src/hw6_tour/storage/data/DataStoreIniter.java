package hw6_tour.storage.data;

import hw6_tour.storage.StorageType;

public class DataStoreIniter {

    public static void init(StorageType storageType) {
        switch (storageType) {
            case LIST:
                ListDataStorage.initStore();
                break;
            case ARRAY:
                ArrayDataStore.initStore();
                break;
            case SET:
                SetDataStore.initStore();
                break;
        }
    }
}
