package hw6_tour.storage;

import hw6_tour.base.BaseService;
import hw6_tour.base.Entities;
import hw6_tour.city.service.CityListService;
import hw6_tour.city.service.CityService;
import hw6_tour.country.service.CountryListService;
import hw6_tour.country.service.CountryService;
import hw6_tour.hotel.service.HotelListService;
import hw6_tour.hotel.service.HotelService;
import hw6_tour.order.service.OrderListService;
import hw6_tour.order.service.OrderService;
import hw6_tour.storage.data.DataStoreIniter;
import hw6_tour.tourProvider.service.TourProviderListService;
import hw6_tour.tourProvider.service.TourProviderService;
import hw6_tour.user.service.UserListService;
import hw6_tour.user.service.UserService;

import java.util.HashMap;

public class ServicesStorage {
    private static StorageType storageType = null;
    private static HashMap<Entities, BaseService> servicesMap = new HashMap<>();

    public static void setStorageType(StorageType storageType) {
        ServicesStorage.storageType = storageType;
        DataStoreIniter.init(storageType);
        initService();
        setInitServices();
    }

    private static void setInitServices() {
        for (BaseService baseService : servicesMap.values()) {
            baseService.initServices();
        }
    }

    private static void initService() {
        switch (storageType) {
            case ARRAY:
                initListServices();
                break;
            case LIST:
                initListServices();
                break;
        }
    }

    private static void initListServices() {
        for (Entities entity : Entities.values()) {
            switch (entity) {
                case USER:
                    servicesMap.put(entity, new UserListService());
                    break;

                case CITY:
                    servicesMap.put(entity, new CityListService());
                    break;

                case COUNTRY:
                    servicesMap.put(entity, new CountryListService());
                    break;

                case HOTEL:
                    servicesMap.put(entity, new HotelListService());
                    break;

                case ORDER:
                    servicesMap.put(entity, new OrderListService());
                    break;

                case TOUR_PROVIDER:
                    servicesMap.put(entity, new TourProviderListService());
                    break;
            }
        }
    }

    public static BaseService getService(Entities entity) {
        return servicesMap.get(entity);
    }

    public static CountryService getCountryService() {
        return (CountryService) getService(Entities.COUNTRY);
    }

    public static CityService getCityService() {
        return (CityService) getService(Entities.CITY);
    }

    public static HotelService getHotelService() {
        return (HotelService) getService(Entities.HOTEL);
    }

    public static TourProviderService getTourProviderService() {
        return (TourProviderService) getService(Entities.TOUR_PROVIDER);
    }

    public static OrderService getOrderService() {
        return (OrderService) getService(Entities.ORDER);
    }

    public static UserService getUserService() {
        return (UserService) getService(Entities.USER);
    }

    public static StorageType getStorageType() {
        return storageType;
    }
}

