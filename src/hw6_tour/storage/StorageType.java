package hw6_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
