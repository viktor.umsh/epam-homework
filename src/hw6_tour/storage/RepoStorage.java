package hw6_tour.storage;

import hw6_tour.city.repo.CityListRepo;
import hw6_tour.city.repo.CityRepo;
import hw6_tour.country.repo.CountryListRepo;
import hw6_tour.country.repo.CountryRepo;
import hw6_tour.hotel.repo.HotelListRepo;
import hw6_tour.hotel.repo.HotelRepo;
import hw6_tour.order.repo.OrderListRepo;
import hw6_tour.order.repo.OrderRepo;
import hw6_tour.tourProvider.repo.TourProviderListRepo;
import hw6_tour.tourProvider.repo.TourProviderRepo;
import hw6_tour.user.repo.UserListRepo;
import hw6_tour.user.repo.UserRepo;

public class RepoStorage {
    public static CountryRepo countryRepo;
    public static CityRepo cityRepo;
    public static HotelRepo hotelRepo;
    public static TourProviderRepo tourProviderRepo;
    public static OrderRepo orderRepo;
    public static UserRepo userRepo;
//    public static StatisticRepo statisticRepo;

    static {
        countryRepo = new CountryListRepo();
        cityRepo = new CityListRepo();
        hotelRepo = new HotelListRepo();
        tourProviderRepo = new TourProviderListRepo();
        orderRepo = new OrderListRepo();
        userRepo = new UserListRepo();
        // statistic
    }
}
