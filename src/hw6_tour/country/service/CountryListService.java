package hw6_tour.country.service;

import hw6_tour.city.City;
import hw6_tour.city.service.CityService;
import hw6_tour.country.Country;
import hw6_tour.storage.RepoStorage;
import hw6_tour.storage.ServicesStorage;

public class CountryListService extends CountryService {

    private CityService cityMemoryService;

    public CountryListService() {
        repository = RepoStorage.countryRepo;
    }

    @Override
    public void initServices() {
        cityMemoryService = ServicesStorage.getCityService();
    }

    @Override
    public void add(Country country) {
        if (basicAdd(country)) {
            for (City city : country.getCities()) {
                if (cityMemoryService.findByEntity(city) == -1) {
                    cityMemoryService.add(city);
                }
            }
        }
    }

    @Override
    public boolean delete(Country country) {
        for (City city : country.getCities()) {
            cityMemoryService.delete(city);
        }
        return repository.delete(country);
    }

}
