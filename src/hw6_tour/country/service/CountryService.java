package hw6_tour.country.service;

import hw6_tour.base.BaseService;
import hw6_tour.country.Country;

public abstract class CountryService extends BaseService<Country> {
}
