package hw6_tour.country.repo;

import hw6_tour.country.Country;

import static hw6_tour.storage.data.ArrayDataStore.countries;


public class CountryArrayRepo implements CountryRepo {
    @Override
    public void add(Country country) {
        countries.add(country);
    }

    @Override
    public Country get(int index) {
        return countries.get(index);
    }

    @Override
    public Country findById(long id) {
        for (Country country : countries) {
            if (country.getId() == id) {
                return country;
            }
        }
        return null;
    }

    @Override
    public Country findByName(String name) {
        for (Country country : countries) {
            if (country.getName().equals(name)) {
                return country;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(Country country) {
        for (int i = 0; i < countries.size(); i++) {
            if (countries.get(i).equals(country)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean delete(Country country) {
        return false;
    }

    @Override
    public Country delete(int index) {
        return null;
    }
}
