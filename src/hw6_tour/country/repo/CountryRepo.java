package hw6_tour.country.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.country.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
