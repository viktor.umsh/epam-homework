package hw6_tour.base;

import hw6_tour.statistic.Statistic;

public abstract class Statistical {
    Statistic statistic = new Statistic();

    public Statistic getStatistic() {
        return statistic;
    }

    public void updateStatistic(long newValue) {
        statistic.updateStatistic(newValue);
    }
}

