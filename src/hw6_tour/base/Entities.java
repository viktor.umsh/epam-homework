package hw6_tour.base;

public enum Entities {
    USER,
    COUNTRY,
    CITY,
    HOTEL,
    TOUR_PROVIDER,
    ORDER
}
