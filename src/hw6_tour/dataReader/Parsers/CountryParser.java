package hw6_tour.dataReader.Parsers;

import hw6_tour.country.Country;
import hw6_tour.country.service.CountryService;
import hw6_tour.storage.ServicesStorage;

public class CountryParser extends BasicParser {

    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 1) {
            countryMemoryService.add(new Country(args[0]));
        }
    }

}
