package hw6_tour.dataReader.Parsers;

public abstract class BasicParser {
    private String regex = "\\|";

    protected String[] split(String line) {
        line = line.replaceAll("\\s", "");
        return line.split(regex);
    }

    public abstract void parseString(String line);
}
