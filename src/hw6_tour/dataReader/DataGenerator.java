package hw6_tour.dataReader;

import hw6_tour.tourProvider.TourProvider;
import hw6_tour.user.AbstractUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class DataGenerator {
    private TourProvider tourProvider;
    private AbstractUser vasiliy;

    public DataGenerator() {
    }

    private String checkInLine(String inLine) {
        String[] accepted = {"Users:", "Countries:", "Cities:", "Hotels:", "TourProviders:", "Orders:"};
        for (String oneAccepted : accepted) {
            if (oneAccepted.equals(inLine)) {
                return oneAccepted;
            }
        }
        return null;
    }

    public void readFile() {
        Scanner in = null;
        System.out.println(System.getProperty("user.dir"));
        try {

            in = new Scanner(new File("input.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        String inLine, whatRead = null;

        Parser parser = new Parser();
        while (in.hasNextLine()) {
            inLine = in.nextLine();
            if (checkInLine(inLine) != null) {
                whatRead = checkInLine(inLine);
                continue;
            }
            if (whatRead == null) {
                continue;
            }
            parser.parse(whatRead, inLine);
        }
    }
}
