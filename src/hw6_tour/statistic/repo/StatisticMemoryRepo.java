package hw6_tour.statistic.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.statistic.Statistic;

import static hw6_tour.storage.data.ListDataStorage.statistics;

public class StatisticMemoryRepo implements BaseRepo<Statistic> {
    @Override
    public void add(Statistic statistic) {
        statistics.add(statistic);
    }

    @Override
    public Statistic get(int index) {
        return statistics.get(index);
    }

    @Override
    public Statistic findById(long id) {
        for (Statistic statistic : statistics) {
            if (statistic.getId() == id) {
                return statistic;
            }
        }
        return null;
    }

    @Override
    public Statistic findByName(String name) {
        return null;
    }

    @Override
    public int findByEntity(Statistic statistic) {
        return statistics.indexOf(statistic);
    }

    @Override
    public boolean delete(Statistic statistic) {
        return statistics.remove(statistic);
    }

    @Override
    public Statistic delete(int index) {
        return statistics.remove(index);
    }
}
