package hw6_tour.order.repo;

import hw6_tour.base.BaseRepo;
import hw6_tour.order.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
