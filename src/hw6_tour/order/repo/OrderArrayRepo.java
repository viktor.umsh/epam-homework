package hw6_tour.order.repo;

import hw6_tour.order.Order;

import static hw6_tour.storage.data.ArrayDataStore.orders;

public class OrderArrayRepo implements OrderRepo {
    @Override
    public void add(Order order) {
        orders.add(order);
    }

    @Override
    public Order get(int index) {
        return orders.get(index);
    }

    @Override
    public Order findById(long id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                return order;
            }
        }
        return null;
    }

    @Override
    public Order findByName(String name) {
        for (Order order : orders) {
            if (order.getName().equals(name)) {
                return order;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(Order order) {
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).equals(order)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean delete(Order order) {
        int index = findByEntity(order);
        if (index != -1) {
            delete(index);
            return true;
        }
        return false;
    }

    @Override
    public Order delete(int index) {
        return orders.remove(index);
    }
}
