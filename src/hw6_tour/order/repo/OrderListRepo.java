package hw6_tour.order.repo;

import hw6_tour.order.Order;

import static hw6_tour.storage.data.ListDataStorage.orders;

public class OrderListRepo implements OrderRepo {
    @Override
    public void add(Order order) {
        orders.add(order);
    }

    @Override
    public Order get(int index) {
        return orders.get(index);
    }

    @Override
    public Order findById(long id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                return order;
            }
        }
        return null;
    }

    @Override
    public Order findByName(String name) {
        return null;
    }

    @Override
    public int findByEntity(Order order) {
        return orders.indexOf(order);
    }

    @Override
    public boolean delete(Order order) {
        return orders.remove(order);
    }

    @Override
    public Order delete(int index) {
        Order order = orders.get(index);
        delete(order);
        return order;
    }
}
