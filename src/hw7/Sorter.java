package hw7;

import java.util.Collection;
import java.util.Random;

public class Sorter {
    private static Random random = new Random();

    private static void swop(String[] array, int i, int j) {
        String tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    private static boolean isC2MoreThenC1(char c1, char c2) {
        if (Character.toLowerCase(c2) == Character.toLowerCase(c1)) {
            if (Character.isLowerCase(c2) && !Character.isLowerCase(c1)) {
                return true;
            } else {
                return false;
            }
        }
        return (Character.toLowerCase(c1) < Character.toLowerCase(c2));
    }

    private static boolean isS2MoreThenS1(String s1, String s2) {
        int length = s1.length();
        if (s2.length() < length) {
            length = s2.length();
        }
        for (int i = 0; i < length; i++) {
            if (isC2MoreThenC1(s1.charAt(i), s2.charAt(i))) {
                return true;
            }
            if (isC2MoreThenC1(s2.charAt(i), s1.charAt(i))) {
                return false;
            }
        }
        return (s2.length() > s1.length());
    }

    private static void mergeSort(String[] array, int start, int end) {
        if (start == end) {
            return;
        }
        if (start + 1 == end) {
            if (!isS2MoreThenS1(array[start], array[end])) {
                swop(array, start, end);
            }
            return;
        }
        int middle = (start + end) / 2;
        mergeSort(array, start, middle);
        mergeSort(array, middle + 1, end);

        String[] mergedTemp = new String[end - start + 1];
        int cur = 0;
        int cur1 = start;
        int cur2 = middle + 1;
        while (cur1 <= middle && cur2 <= end) {
            if (!isS2MoreThenS1(array[cur1], array[cur2])) {
                mergedTemp[cur++] = array[cur2++];
            } else {
                mergedTemp[cur++] = array[cur1++];
            }
        }
        while (cur2 <= end) {
            mergedTemp[cur++] = array[cur2++];
        }
        while (cur1 <= middle) {
            mergedTemp[cur++] = array[cur1++];
        }

        for (int i = 0; i <= end - start; i++) {
            array[i + start] = mergedTemp[i];
        }
    }

    public static void sort(String[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    // =)
    public static void sort(Collection<String> array) {
        mergeSort((String[]) array.toArray(), 0, array.size() - 1);
    }


    public static void main(String[] args) {
        String[] input = {"zfd", "tdfg", "bfff", "dbf", "Bdd", "Acc", "cbcf", "acbfds", "acbfdsqq"};
        sort(input);

        System.out.println("Sorted array:");
        for (String str : input) {
            System.out.println(str);
        }
    }
}
