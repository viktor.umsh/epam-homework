package hw11_tour.order.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.order.domain.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
