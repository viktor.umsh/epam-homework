package hw11_tour.order.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.order.domain.Order;
import hw11_tour.order.repo.OrderIdGenerator;
import hw11_tour.order.repo.OrderRepo;

import static hw11_tour.storage.data.ArrayDataStore.orders;

public class OrderArrayRepo extends ArrayRepo<Order> implements OrderRepo {
    public OrderArrayRepo() {
        dataArray = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
