package hw11_tour.order.repo.impl;

import hw11_tour.base.repo.ListRepo;
import hw11_tour.order.domain.Order;
import hw11_tour.order.repo.OrderIdGenerator;
import hw11_tour.order.repo.OrderRepo;

import static hw11_tour.storage.data.ListDataStorage.orders;

public class OrderListRepo extends ListRepo<Order> implements OrderRepo {
    public OrderListRepo() {
        dataList = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
