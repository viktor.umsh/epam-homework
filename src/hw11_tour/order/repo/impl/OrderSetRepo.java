package hw11_tour.order.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.order.domain.Order;
import hw11_tour.order.repo.OrderIdGenerator;
import hw11_tour.order.repo.OrderRepo;
import hw11_tour.storage.data.SetDataStore;

public class OrderSetRepo extends SetRepo<Order> implements OrderRepo {
    public OrderSetRepo() {
        dataSet = SetDataStore.orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
