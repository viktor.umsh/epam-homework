package hw11_tour.order.service;

import hw11_tour.base.service.BaseService;
import hw11_tour.order.domain.Order;

public abstract class OrderService extends BaseService<Order> {
}
