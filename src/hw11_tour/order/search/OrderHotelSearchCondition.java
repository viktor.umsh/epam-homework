package hw11_tour.order.search;

import hw11_tour.hotel.domain.Hotel;
import hw11_tour.order.domain.Order;

public class OrderHotelSearchCondition implements OrderSearchCondition {
    private Hotel soughtHotel;

    public OrderHotelSearchCondition(Hotel soughtHotel) {
        this.soughtHotel = soughtHotel;
    }

    @Override
    public boolean isFit(Order order) {
        if (order.getHotel() == null) {
            return false;
        }
        return order.getHotel().equals(soughtHotel);
    }
}
