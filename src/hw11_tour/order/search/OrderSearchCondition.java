package hw11_tour.order.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.order.domain.Order;

public interface OrderSearchCondition extends BaseSearchCondition<Order> {
}
