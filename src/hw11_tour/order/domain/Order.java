package hw11_tour.order.domain;

import hw11_tour.base.domain.BaseDomain;
import hw11_tour.base.domain.Entities;
import hw11_tour.hotel.domain.Hotel;

import java.util.Objects;

public class Order extends BaseDomain {
    private long price;
    private Hotel hotel;

    public Order(long price, Hotel hotel) {
        this.price = price;
        this.hotel = hotel;
    }

    public long getPrice() {
        return price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public static Entities getEntityType() {
        return Entities.ORDER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return price == order.price &&
                Objects.equals(hotel, order.hotel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, hotel);
    }

    @Override
    public String toString() {
        return "Order{" +
                "price=" + price +
                ", hotel=" + hotel +
                ", id=" + id +
                '}';
    }

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof Order) || this == something) {
            return;
        }
        Order someOrder = (Order) something;
        if (someOrder.price != 0) {
            this.price = someOrder.price;
        }
        if (someOrder.hotel != null) {
            this.hotel = someOrder.hotel;
        }
    }
}
