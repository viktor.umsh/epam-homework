package hw11_tour.order.sort;

import hw11_tour.base.sort.BaseField;

public enum OrderSortedField implements BaseField {
    PRICE,
    HOTEL,
    CITY
}
