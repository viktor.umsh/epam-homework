package hw11_tour.country.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.country.domain.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
