package hw11_tour.country.repo.impl;

import hw11_tour.base.repo.ListRepo;
import hw11_tour.country.domain.Country;
import hw11_tour.country.repo.CountryIdGenerator;
import hw11_tour.country.repo.CountryRepo;

import static hw11_tour.storage.data.ListDataStorage.countries;

public class CountryListRepo extends ListRepo<Country> implements CountryRepo {
    public CountryListRepo() {
        dataList = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
