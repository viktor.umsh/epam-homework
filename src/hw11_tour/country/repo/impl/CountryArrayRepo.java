package hw11_tour.country.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.country.domain.Country;
import hw11_tour.country.repo.CountryIdGenerator;
import hw11_tour.country.repo.CountryRepo;

import static hw11_tour.storage.data.ArrayDataStore.countries;

public class CountryArrayRepo extends ArrayRepo<Country> implements CountryRepo {
    public CountryArrayRepo() {
        dataArray = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
