package hw11_tour.country.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.country.domain.Country;
import hw11_tour.country.repo.CountryIdGenerator;
import hw11_tour.country.repo.CountryRepo;
import hw11_tour.storage.data.SetDataStore;

public class CountrySetRepo extends SetRepo<Country> implements CountryRepo {
    public CountrySetRepo() {
        dataSet = SetDataStore.countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
