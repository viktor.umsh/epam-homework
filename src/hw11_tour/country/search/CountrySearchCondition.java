package hw11_tour.country.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.country.domain.Country;

public interface CountrySearchCondition extends BaseSearchCondition<Country> {
}
