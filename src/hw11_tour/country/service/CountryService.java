package hw11_tour.country.service;

import hw11_tour.base.service.BaseService;
import hw11_tour.country.domain.Country;

public abstract class CountryService extends BaseService<Country> {
}
