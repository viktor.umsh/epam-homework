package hw11_tour.country.service;

import hw11_tour.city.domain.City;
import hw11_tour.city.service.CityService;
import hw11_tour.country.domain.Country;
import hw11_tour.country.repo.CountryRepo;
import hw11_tour.storage.services.ServicesStorage;

public class CountryDefaultService extends CountryService {

    private CityService cityService;

    public CountryDefaultService(CountryRepo countryRepo) {
        repository = countryRepo;
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
    }

    @Override
    public void add(Country country) {
        if (basicAdd(country)) {
            for (City city : country.getCities()) {
                if (cityService.findByEntity(city) == null) {
                    cityService.add(city);
                }
            }
        }
    }

    @Override
    public boolean delete(Country country) {
        for (City city : country.getCities()) {
            cityService.delete(city);
        }
        return repository.delete(country);
    }

}
