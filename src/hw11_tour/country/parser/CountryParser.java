package hw11_tour.country.parser;

import hw11_tour.base.parser.BasicParser;
import hw11_tour.country.domain.Country;
import hw11_tour.country.service.CountryService;
import hw11_tour.storage.services.ServicesStorage;

public class CountryParser extends BasicParser {

    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 1) {
            countryMemoryService.add(new Country(args[0]));
        }
    }

}
