package hw11_tour.user.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.user.domain.AbstractUser;

public interface UserSearchCondition extends BaseSearchCondition<AbstractUser> {
}
