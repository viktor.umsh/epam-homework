package hw11_tour.user.search;

import hw11_tour.city.domain.City;
import hw11_tour.user.domain.AbstractUser;

public class UserCitySearchCondition implements UserSearchCondition {
    private City soughtCity;

    public UserCitySearchCondition(City soughtCity) {
        this.soughtCity = soughtCity;
    }

    @Override
    public boolean isFit(AbstractUser user) {
        if (user.getCity() == null) {
            return false;
        }
        return user.getCity().equals(soughtCity);
    }
}
