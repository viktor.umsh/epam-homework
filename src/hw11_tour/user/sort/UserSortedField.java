package hw11_tour.user.sort;

import hw11_tour.base.sort.BaseField;

public enum UserSortedField implements BaseField {
    NAME,
    CITY,
    COUNTRY
}
