package hw11_tour.user.service;

import hw11_tour.base.service.BaseService;
import hw11_tour.user.domain.AbstractUser;

public abstract class UserService extends BaseService<AbstractUser> {
}
