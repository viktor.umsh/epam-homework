package hw11_tour.user.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.user.domain.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
