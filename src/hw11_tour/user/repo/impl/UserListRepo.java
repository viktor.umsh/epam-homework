package hw11_tour.user.repo.impl;

import hw11_tour.base.repo.ListRepo;
import hw11_tour.user.domain.AbstractUser;
import hw11_tour.user.repo.UserIdGenerator;
import hw11_tour.user.repo.UserRepo;

import static hw11_tour.storage.data.ListDataStorage.users;

public class UserListRepo extends ListRepo<AbstractUser> implements UserRepo {
    public UserListRepo() {
        dataList = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
