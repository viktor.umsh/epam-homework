package hw11_tour.user.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.user.domain.AbstractUser;
import hw11_tour.user.repo.UserIdGenerator;
import hw11_tour.user.repo.UserRepo;

import static hw11_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo extends ArrayRepo<AbstractUser> implements UserRepo {
    public UserArrayRepo() {
        dataArray = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
