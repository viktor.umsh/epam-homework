package hw11_tour.user.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.storage.data.SetDataStore;
import hw11_tour.user.domain.AbstractUser;
import hw11_tour.user.repo.UserIdGenerator;
import hw11_tour.user.repo.UserRepo;

public class UserSetRepo extends SetRepo<AbstractUser> implements UserRepo {
    public UserSetRepo() {
        dataSet = SetDataStore.users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
