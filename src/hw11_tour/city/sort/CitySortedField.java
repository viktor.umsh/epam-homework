package hw11_tour.city.sort;

import hw11_tour.base.sort.BaseField;

public enum CitySortedField implements BaseField {
    NAME,
    COUNTRY,
    CLIMATE
}

