package hw11_tour.city.service;

import hw11_tour.city.domain.City;
import hw11_tour.city.repo.CityRepo;
import hw11_tour.country.service.CountryService;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.hotel.service.HotelService;
import hw11_tour.storage.services.ServicesStorage;

public class CityDefaultService extends CityService {
    private CountryService countryService;
    private HotelService hotelListService;

    public CityDefaultService(CityRepo cityRepo) {
        repository = cityRepo;
    }

    @Override
    public void initInsertedServices() {
        countryService = ServicesStorage.getCountryService();
        hotelListService = ServicesStorage.getHotelService();
    }

    @Override
    public void add(City city) {
        if (basicAdd(city)) {
            if (city.getCountry() != null) {
                if (countryService.findByEntity(city.getCountry()) == null) {
                    countryService.add(city.getCountry());
                }
                if (!city.getCountry().getCities().contains(city)) {
                    city.getCountry().addCity(city);
                }
            }
            for (Hotel hotel : city.getHotels()) {
                hotelListService.add(hotel);
            }
        }
    }

    @Override
    public boolean delete(City city) {
        for (Hotel hotel : city.getHotels()) {
            hotelListService.delete(hotel);
        }
        city.getCountry().deleteCity(city);
//        if (city.getCountry().getCities().size() == 0) {
//            countryService.delete(city.getCountry());
//        }
        return repository.delete(city);
    }
}
