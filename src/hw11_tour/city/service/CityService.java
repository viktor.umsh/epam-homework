package hw11_tour.city.service;

import hw11_tour.base.service.BaseService;
import hw11_tour.city.domain.City;

public abstract class CityService extends BaseService<City> {
}
