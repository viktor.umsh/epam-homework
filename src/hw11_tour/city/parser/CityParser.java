package hw11_tour.city.parser;

import hw11_tour.base.parser.BasicParser;
import hw11_tour.city.domain.City;
import hw11_tour.city.domain.Climate;
import hw11_tour.city.domain.Coordinate;
import hw11_tour.city.service.CityService;
import hw11_tour.country.domain.Country;
import hw11_tour.country.service.CountryService;
import hw11_tour.storage.services.ServicesStorage;

public class CityParser extends BasicParser {
    private CountryService countryMemoryService = ServicesStorage.getCountryService();
    private CityService cityMemoryService = ServicesStorage.getCityService();


    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length != 5) {
            return;
        }
        try {
            Coordinate coordinate = new Coordinate(Long.parseLong(args[1]), Long.parseLong(args[2]));
            Climate climate = null;
            try {
                climate = Climate.valueOf(args[4]);
            } catch (IllegalArgumentException e) {
                System.err.println("Illegal climate!");
                return;
            }
            Country country = countryMemoryService.findByName(args[3]);
            if (country == null) {
                country = new Country(args[3]);
                countryMemoryService.add(country);
            }
            cityMemoryService.add(new City(args[0], coordinate, country, climate));
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        }
    }
}
