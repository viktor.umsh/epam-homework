package hw11_tour.city.search;

import hw11_tour.city.domain.City;

public class CityNameSearchCondition implements CitySearchCondition {
    private String namePattern;

    public CityNameSearchCondition(String namePattern) {
        this.namePattern = namePattern;
    }

    @Override
    public boolean isFit(City city) {
        if (city.getName() == null) {
            return false;
        }
        return city.getName().contains(namePattern);
    }
}
