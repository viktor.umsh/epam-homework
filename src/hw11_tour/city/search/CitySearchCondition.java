package hw11_tour.city.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.city.domain.City;

public interface CitySearchCondition extends BaseSearchCondition<City> {
}
