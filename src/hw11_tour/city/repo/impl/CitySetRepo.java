package hw11_tour.city.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.city.domain.City;
import hw11_tour.city.repo.CityIdGenerator;
import hw11_tour.city.repo.CityRepo;
import hw11_tour.storage.data.SetDataStore;

public class CitySetRepo extends SetRepo<City> implements CityRepo {
    public CitySetRepo() {
        dataSet = SetDataStore.cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }

}
