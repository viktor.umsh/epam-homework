package hw11_tour.city.repo.impl;

import hw11_tour.base.repo.BaseIdGenerator;
import hw11_tour.base.repo.ListRepo;
import hw11_tour.city.domain.City;
import hw11_tour.city.repo.CityRepo;

import static hw11_tour.storage.data.ListDataStorage.cities;

public class CityListRepo extends ListRepo<City> implements CityRepo {
    public CityListRepo() {
        dataList = cities;
    }

    @Override
    protected long generateNextId() {
        return BaseIdGenerator.nextId();
    }

}
