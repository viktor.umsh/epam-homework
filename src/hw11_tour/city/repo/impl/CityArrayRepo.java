package hw11_tour.city.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.city.domain.City;
import hw11_tour.city.repo.CityIdGenerator;
import hw11_tour.city.repo.CityRepo;

import static hw11_tour.storage.data.ArrayDataStore.cities;

public class CityArrayRepo extends ArrayRepo<City> implements CityRepo {
    public CityArrayRepo() {
        dataArray = cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }
}
