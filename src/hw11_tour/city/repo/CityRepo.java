package hw11_tour.city.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.city.domain.City;

public interface CityRepo extends BaseRepo<City> {
}
