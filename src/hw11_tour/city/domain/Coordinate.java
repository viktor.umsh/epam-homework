package hw11_tour.city.domain;

import java.util.Objects;

public class Coordinate {

    private final long latitude;    // x
    private final long longitude;   // y

    public Coordinate(long latitude, long longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public long lengthTo(Coordinate destination) {
        return (long) ((destination.latitude - latitude) * (destination.latitude - latitude) +
                (destination.longitude - longitude) * (destination.longitude - longitude));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return latitude == that.latitude &&
                longitude == that.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
