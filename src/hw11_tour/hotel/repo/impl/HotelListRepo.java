package hw11_tour.hotel.repo.impl;

import hw11_tour.base.repo.ListRepo;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.hotel.repo.HotelIdGenerator;
import hw11_tour.hotel.repo.HotelRepo;

import static hw11_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo extends ListRepo<Hotel> implements HotelRepo {
    public HotelListRepo() {
        dataList = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
