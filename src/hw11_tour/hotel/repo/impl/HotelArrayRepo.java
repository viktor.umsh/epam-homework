package hw11_tour.hotel.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.hotel.repo.HotelIdGenerator;
import hw11_tour.hotel.repo.HotelRepo;

import static hw11_tour.storage.data.ArrayDataStore.hotels;

public class HotelArrayRepo extends ArrayRepo<Hotel> implements HotelRepo {
    public HotelArrayRepo() {
        dataArray = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
