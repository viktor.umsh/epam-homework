package hw11_tour.hotel.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.hotel.repo.HotelIdGenerator;
import hw11_tour.hotel.repo.HotelRepo;
import hw11_tour.storage.data.SetDataStore;

public class HotelSetRepo extends SetRepo<Hotel> implements HotelRepo {
    public HotelSetRepo() {
        dataSet = SetDataStore.hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
