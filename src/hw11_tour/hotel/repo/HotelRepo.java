package hw11_tour.hotel.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.hotel.domain.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
