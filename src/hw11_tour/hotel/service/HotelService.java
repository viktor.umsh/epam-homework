package hw11_tour.hotel.service;

import hw11_tour.base.service.BaseService;
import hw11_tour.hotel.domain.Hotel;

public abstract class HotelService extends BaseService<Hotel> {
}
