package hw11_tour.hotel.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.hotel.domain.Hotel;

public interface HotelSearchCondition extends BaseSearchCondition<Hotel> {
}
