package hw11_tour.hotel.parser;

import hw11_tour.base.parser.BasicParser;
import hw11_tour.city.domain.City;
import hw11_tour.city.service.CityService;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.hotel.service.HotelService;
import hw11_tour.storage.services.ServicesStorage;

public class HotelParser extends BasicParser {
    private HotelService hotelMemoryService = ServicesStorage.getHotelService();
    private CityService cityMemoryService = ServicesStorage.getCityService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length != 4) {
            return;
        }
        try {
            City city = cityMemoryService.findByName(args[3]);
            if (city == null) {
                city = new City(args[3], null, null, null);
//                return; // TODO: подумать что делать..
            }
            hotelMemoryService.add(new Hotel(args[0], Integer.parseInt(args[1]), Long.parseLong(args[2]), city));
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        }
    }
}
