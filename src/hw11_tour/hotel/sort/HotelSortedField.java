package hw11_tour.hotel.sort;

import hw11_tour.base.sort.BaseField;

public enum HotelSortedField implements BaseField {
    NAME,
    COUNTRY,
    CITY,
    PRICE
}
