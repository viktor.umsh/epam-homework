package hw11_tour.dataReader.xmlDataReader;

import hw11_tour.base.domain.BaseDomain;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public interface XMLElementParser<Type extends BaseDomain> {
    Type parse(XMLStreamReader reader) throws XMLStreamException;
}
