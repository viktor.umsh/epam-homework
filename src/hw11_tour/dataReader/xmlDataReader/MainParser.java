package hw11_tour.dataReader.xmlDataReader;

import hw11_tour.tourprovider.domain.TourProvider;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class MainParser {

    public static final String fileName = "inputdata.xml";

    public MainParser() {

    }

    public void start() {
        try(CustomStaxReader staxReader = CustomStaxReader.newInstance(fileName)) {
            XMLStreamReader streamReader = staxReader.getReader();
            readDocument(streamReader);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private void readDocument(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "system": {
                            System.out.println("System");
                            break;
                        }
                        case "TourProviders": {
                            XMLMultipleParser<TourProvider> countryXMLMultipleParser = new XMLMultipleParser<>(reader, TourProvider.class);
                            countryXMLMultipleParser.multipleParse();
                            break;
                        }
/*
                        case "countries": {
                            XMLMultipleParser<Country> countryXMLMultipleParser = new XMLMultipleParser<>(reader, Country.class);
                            countryXMLMultipleParser.multipleParse();
                            break;
                        }
                        case "cities": {
                            XMLMultipleParser<City> countryXMLMultipleParser = new XMLMultipleParser<>(reader, City.class);
                            countryXMLMultipleParser.multipleParse();
                            break;
                        }
                        case "users": {
                            XMLMultipleParser<AbstractUser> countryXMLMultipleParser = new XMLMultipleParser<>(reader, AbstractUser.class);
                            countryXMLMultipleParser.multipleParse();
                            break;
                        }
                        // */
                    }
                    break;
                }
                case XMLStreamConstants.END_DOCUMENT: {
                    System.out.println("Ok");
                    return ;
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }
}
