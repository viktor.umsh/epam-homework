package hw11_tour.dataReader.xmlDataReader;

import hw11_tour.base.domain.Entities;
import hw11_tour.city.domain.City;
import hw11_tour.country.domain.Country;
import hw11_tour.dataReader.xmlDataReader.elemeptParsers.*;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.order.domain.Order;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.user.domain.AbstractUser;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class TagStore {
    private static Map<Class, Entities> classMap = new HashMap<>();
    private static Map<Entities, String> tagMap = new EnumMap<>(Entities.class);
    private static Map<Entities, XMLElementParser> parsersMap = new EnumMap<>(Entities.class);

    static {
        classMap.put(City.class, Entities.CITY);
        classMap.put(Country.class, Entities.COUNTRY);
        classMap.put(Hotel.class, Entities.HOTEL);
        classMap.put(Order.class, Entities.ORDER);
        classMap.put(AbstractUser.class, Entities.USER);
        classMap.put(TourProvider.class, Entities.TOUR_PROVIDER);
    }
    static {
        tagMap.put(Entities.CITY, "city");
        tagMap.put(Entities.COUNTRY, "country");
        tagMap.put(Entities.HOTEL, "hotel");
        tagMap.put(Entities.ORDER, "order");
        tagMap.put(Entities.USER, "user");
        tagMap.put(Entities.TOUR_PROVIDER, "tourProvider");
    }
    static {
        parsersMap.put(Entities.USER, new UserParser());
        parsersMap.put(Entities.CITY, new CityParser());
        parsersMap.put(Entities.COUNTRY, new CountryParser());
        parsersMap.put(Entities.HOTEL, new HotelParser());
        parsersMap.put(Entities.TOUR_PROVIDER, new TourProviderParser());
        parsersMap.put(Entities.ORDER, new OrderParser());
    }

    public static Entities getEntity(Class some) {
        return classMap.get(some);
    }

    public static String getTagName(Entities element) {
        return tagMap.get(element);
    }

    public static XMLElementParser getParser(Entities element) {
        return parsersMap.get(element);
    }
}
