package hw11_tour.dataReader.xmlDataReader.elemeptParsers;

import hw11_tour.city.domain.Coordinate;
import hw11_tour.dataReader.xmlDataReader.CustomStaxReader;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class CoordinateParser {
    public static Coordinate parse(XMLStreamReader reader) throws XMLStreamException {
        Long x=null, y=null;
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "x": {
                            x = Long.parseLong(CustomStaxReader.readContent(reader));
                            break;
                        }
                        case "y": {
                            y = Long.parseLong(CustomStaxReader.readContent(reader));
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    if (x != null && y != null) {
                        return new Coordinate(x, y);
                    } else {
                        return null;
                    }
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }
}
