package hw11_tour.dataReader.xmlDataReader.elemeptParsers;

import hw11_tour.city.domain.City;
import hw11_tour.city.domain.Climate;
import hw11_tour.city.domain.Coordinate;
import hw11_tour.country.domain.Country;
import hw11_tour.country.service.CountryService;
import hw11_tour.dataReader.xmlDataReader.CustomStaxReader;
import hw11_tour.dataReader.xmlDataReader.XMLElementParser;
import hw11_tour.dataReader.xmlDataReader.XMLMultipleParser;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.storage.services.ServicesStorage;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class CityParser implements XMLElementParser<City> {

    @Override
    public City parse(XMLStreamReader reader) throws XMLStreamException {
        CountryService countryService = ServicesStorage.getCountryService();
//        long id=-1;
        String name = null;
        Country country = null;
        Climate climate = null;
        Coordinate coordinate = null;
        List<Hotel> hotels = null;
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "name": {
                            name = CustomStaxReader.readContent(reader);
                            break;
                        }
                        case "climate": {
                            climate = Climate.valueOf(CustomStaxReader.readContent(reader));
                            break;
                        }
                        case "coordinate": {
                            coordinate = CoordinateParser.parse(reader);
                            break;
                        }
                        case "hotels": {
                            XMLMultipleParser<Hotel> countryXMLMultipleParser = new XMLMultipleParser<>(reader, Hotel.class);
                            hotels = countryXMLMultipleParser.multipleParse();
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    if (name == null) {
                        throw new RuntimeException("Name of user not  initialized!");
                    }
                    City city = new City(name, coordinate, country, climate);
                    if (hotels != null) {
                        for (Hotel hotel : hotels) {
                            hotel.setCity(city);
                        }
                        city.addHotels(hotels);
                    }
                    return city;
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }
}
