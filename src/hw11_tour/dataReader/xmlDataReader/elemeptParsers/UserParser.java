package hw11_tour.dataReader.xmlDataReader.elemeptParsers;

import hw11_tour.city.domain.City;
import hw11_tour.city.service.CityService;
import hw11_tour.dataReader.xmlDataReader.CustomStaxReader;
import hw11_tour.dataReader.xmlDataReader.XMLElementParser;
import hw11_tour.dataReader.xmlDataReader.XMLMultipleParser;
import hw11_tour.order.domain.Order;
import hw11_tour.user.domain.AbstractUser;
import hw11_tour.user.domain.RandomMan;
import hw11_tour.storage.services.ServicesStorage;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class UserParser implements XMLElementParser<AbstractUser> {


    @Override
    public AbstractUser parse(XMLStreamReader reader) throws XMLStreamException {
        CityService cityService = ServicesStorage.getCityService();
        String name=null;
        City city=null;
        List<Order> orders = null;
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "name": {
                            name = CustomStaxReader.readContent(reader);
                            break;
                        }
                        case "city": {
                            long cityId = Long.parseLong(CustomStaxReader.readContent(reader));
                            city = cityService.findById(cityId);
                            break;
                        }
                        case "orders": {
                            XMLMultipleParser<Order> countryXMLMultipleParser = new XMLMultipleParser<>(reader, Order.class);
                            orders = countryXMLMultipleParser.multipleParse();
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    if (name == null) {
                        throw new RuntimeException("Name of user not  initialized!");
                    }
                    AbstractUser user = new RandomMan(name, city);
                    return user;
                }
            }
        }
        throw new RuntimeException("Have not close tag");

    }
}
