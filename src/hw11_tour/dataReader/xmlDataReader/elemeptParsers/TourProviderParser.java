package hw11_tour.dataReader.xmlDataReader.elemeptParsers;

import hw11_tour.country.domain.Country;
import hw11_tour.dataReader.xmlDataReader.CustomStaxReader;
import hw11_tour.dataReader.xmlDataReader.XMLElementParser;
import hw11_tour.dataReader.xmlDataReader.XMLMultipleParser;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.user.domain.AbstractUser;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class TourProviderParser implements XMLElementParser<TourProvider> {

    @Override
    public TourProvider parse(XMLStreamReader reader) throws XMLStreamException {
        String name = null;
        long distanceCost=0, serviceCost=0;
        List<Country> countries = null;
        List<AbstractUser> users = null;

        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "name": {
                            name = CustomStaxReader.readContent(reader);
                            break;
                        }
                        case "distanceCost":
                            distanceCost = Long.parseLong(CustomStaxReader.readContent(reader));
                            break;
                        case "serviceCost":
                            serviceCost = Long.parseLong(CustomStaxReader.readContent(reader));
                            break;
                        case "countries": {
                            XMLMultipleParser<Country> countryXMLMultipleParser = new XMLMultipleParser<>(reader, Country.class);
                            countries = countryXMLMultipleParser.multipleParse();
                            break;
                        }
                        case "users": {
                            XMLMultipleParser<AbstractUser> countryXMLMultipleParser = new XMLMultipleParser<>(reader, AbstractUser.class);
                            users = countryXMLMultipleParser.multipleParse();
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    TourProvider tourProvider = new TourProvider(name, distanceCost, serviceCost);
                    tourProvider.addCountries(countries);
                    return tourProvider;
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }

}
