package hw11_tour.dataReader.xmlDataReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CustomStaxReader implements AutoCloseable {
    private FileInputStream fileInputStream;
    private XMLStreamReader reader;

    private CustomStaxReader() {}

    public static CustomStaxReader newInstance(String fileName) throws FileNotFoundException, XMLStreamException {
        CustomStaxReader instance = new CustomStaxReader();
        instance.fileInputStream = new FileInputStream(fileName);
        instance.reader = XMLInputFactory.newInstance().createXMLStreamReader(instance.fileInputStream);
        return instance;
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    @Override
    public void close() throws XMLStreamException, IOException {
        if (fileInputStream != null) {
            fileInputStream.close();
        }
        if (reader != null) {
            reader.close();
        }
    }

    public static String readContent(XMLStreamReader reader) throws XMLStreamException {

        StringBuilder content = new StringBuilder();

        while (reader.hasNext()) {
            int eventType = reader.next();

            switch (eventType) {
                case XMLStreamConstants.CHARACTERS:
                case XMLStreamConstants.CDATA: {
                    content.append(reader.getText());
                    break;
                }

                case XMLStreamConstants.END_ELEMENT: {
                    return content.toString();
                }
            }
        }
        throw new RuntimeException("I didn't find suitable end tag");
    }
}
