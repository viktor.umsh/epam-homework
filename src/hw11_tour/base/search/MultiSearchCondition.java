package hw11_tour.base.search;

import java.util.ArrayList;
import java.util.List;

public class MultiSearchCondition<Type> implements BaseSearchCondition<Type> {
    private List<BaseSearchCondition<Type>> searchConditions = new ArrayList<>();

    @Override
    public boolean isFit(Type something) {
        for (BaseSearchCondition<Type> searchCondition : searchConditions) {
            if (!searchCondition.isFit(something)) {
                return false;
            }
        }
        return true;
    }

    public void addSearchCondition(BaseSearchCondition<Type> someCondiotion) {
        searchConditions.add(someCondiotion);
    }
}
