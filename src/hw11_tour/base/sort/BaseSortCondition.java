package hw11_tour.base.sort;

import java.util.*;

public abstract class BaseSortCondition<Type> {

    protected List<Integer> getSortedPriorities(Map<Integer, ?> sortedFields) {
        List<Integer> priorityList = new ArrayList<>(sortedFields.keySet());
        Collections.sort(priorityList);
        return priorityList;
    }

    public abstract Comparator<Type> getComparator();
}
