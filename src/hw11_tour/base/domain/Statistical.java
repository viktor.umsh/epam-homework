package hw11_tour.base.domain;

import hw11_tour.statistic.Statistic;

public abstract class Statistical {
    private Statistic statistic = new Statistic();

    public Statistic getStatistic() {
        return statistic;
    }

    public void updateStatistic(long newValue) {
        statistic.updateStatistic(newValue);
    }
}

