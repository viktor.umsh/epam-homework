package hw11_tour.storage.sort;

import hw11_tour.base.sort.BaseField;
import hw11_tour.city.sort.CitySortedField;
import hw11_tour.country.sort.CountrySortedField;
import hw11_tour.hotel.sort.HotelSortedField;
import hw11_tour.order.sort.OrderSortedField;
import hw11_tour.storage.EssenceType;
import hw11_tour.tourprovider.sort.TourProviderSortedField;
import hw11_tour.user.sort.UserSortedField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SortedFields {
    private static HashMap<EssenceType, List<BaseField>> sortedFields = generateMap();
    private static SortedFields instance = new SortedFields();

    public static SortedFields getInstance() {
        return instance;
    }

    static HashMap<EssenceType, List<BaseField>> generateMap() {
        HashMap<EssenceType, List<BaseField>> sortedFields = new HashMap<>();
        sortedFields.put(EssenceType.COUNTRY, new ArrayList<BaseField>(Arrays.asList(CountrySortedField.values())));
        sortedFields.put(EssenceType.CITY, new ArrayList<BaseField>(Arrays.asList(CitySortedField.values())));
        sortedFields.put(EssenceType.HOTEL, new ArrayList<BaseField>(Arrays.asList(HotelSortedField.values())));
        sortedFields.put(EssenceType.TOUR_PROVIDER, new ArrayList<BaseField>(Arrays.asList(TourProviderSortedField.values())));
        sortedFields.put(EssenceType.ORDER, new ArrayList<BaseField>(Arrays.asList(OrderSortedField.values())));
        sortedFields.put(EssenceType.USER, new ArrayList<BaseField>(Arrays.asList(UserSortedField.values())));
        return sortedFields;
    }

    public List<BaseField> getFields(EssenceType essenceType) {
        return sortedFields.get(essenceType);
    }
}
