package hw11_tour.storage.data;

import hw11_tour.city.domain.City;
import hw11_tour.country.domain.Country;
import hw11_tour.hotel.domain.Hotel;
import hw11_tour.order.domain.Order;
import hw11_tour.statistic.Statistic;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.user.domain.AbstractUser;

import java.util.HashSet;
import java.util.Set;

public class SetDataStore {
    public static Set<Country> countries;
    public static Set<City> cities;
    public static Set<Hotel> hotels;
    public static Set<TourProvider> tourProviders;
    public static Set<Order> orders;
    public static Set<AbstractUser> users;
    public static Set<Statistic> statistics;

    static void initStore() {
        countries = new HashSet<>();
        cities = new HashSet<>();
        hotels = new HashSet<>();
        tourProviders = new HashSet<>();
        orders = new HashSet<>();
        users = new HashSet<>();
        statistics = new HashSet<>();
        users = new HashSet<>();
    }

}
