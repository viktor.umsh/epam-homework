package hw11_tour.storage.data;

import hw11_tour.storage.StorageType;

public class DataStoreFactory {

    public static void init(StorageType storageType) {
        switch (storageType) {
            case LIST:
                ListDataStorage.initStore();
                break;
            case ARRAY:
                ArrayDataStore.initStore();
                break;
            case SET:
                SetDataStore.initStore();
                break;
        }
    }
}
