package hw11_tour.storage.services;

import hw11_tour.base.service.BaseService;
import hw11_tour.city.service.CityService;
import hw11_tour.country.service.CountryService;
import hw11_tour.hotel.service.HotelService;
import hw11_tour.order.service.OrderService;
import hw11_tour.storage.EssenceType;
import hw11_tour.storage.StorageType;
import hw11_tour.storage.data.DataStoreFactory;
import hw11_tour.tourprovider.service.TourProviderService;
import hw11_tour.user.service.UserService;


public class ServicesStorage {
    private static StorageType storageType = null;
    private static ServiceFactory serviceFactory;


    public static void setStorageType(StorageType storageType) {
        ServicesStorage.storageType = storageType;
        DataStoreFactory.init(storageType);
        initService();
        serviceFactory.setInsertedInitServices();
    }

    private static void initService() {
        switch (storageType) {
            case LIST:
                serviceFactory = new ListServiceFactory();
                break;
            case ARRAY:
                serviceFactory = new ArrayServiceFactory();
                break;
            case SET:
                serviceFactory = new SetServiceFactory();
                break;
        }
    }

    public static BaseService gerService(EssenceType something) {
        switch (something) {
            case CITY:
                return getCityService();
            case COUNTRY:
                return getCountryService();
            case HOTEL:
                return getHotelService();
            case TOUR_PROVIDER:
                return getTourProviderService();
            case ORDER:
                return getOrderService();
            case USER:
                return getUserService();
        }
        return null;
    }

    public static CountryService getCountryService() {
        return serviceFactory.getCountryService();
    }

    public static CityService getCityService() {
        return serviceFactory.getCityService();
    }

    public static HotelService getHotelService() {
        return serviceFactory.getHotelService();
    }

    public static TourProviderService getTourProviderService() {
        return serviceFactory.getTourProviderService();
    }

    public static OrderService getOrderService() {
        return serviceFactory.getOrderService();
    }

    public static UserService getUserService() {
        return serviceFactory.getUserService();
    }

    public static StorageType getStorageType() {
        return storageType;
    }
}

