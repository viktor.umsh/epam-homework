package hw11_tour.storage.services;

import hw11_tour.city.service.CityService;
import hw11_tour.country.service.CountryService;
import hw11_tour.hotel.service.HotelService;
import hw11_tour.order.service.OrderService;
import hw11_tour.storage.StorageType;
import hw11_tour.tourprovider.service.TourProviderService;
import hw11_tour.user.service.UserService;

public abstract class ServiceFactory {

    protected CountryService countryService;
    protected CityService cityService;
    protected HotelService hotelService;
    protected TourProviderService tourProviderService;
    protected OrderService orderService;
    protected UserService userService;
    protected StorageType storageType;

    protected void setInsertedInitServices() {
        countryService.initInsertedServices();
        cityService.initInsertedServices();
        hotelService.initInsertedServices();
        tourProviderService.initInsertedServices();
        orderService.initInsertedServices();
        userService.initInsertedServices();
    }

    public CountryService getCountryService() {
        return countryService;
    }

    public CityService getCityService() {
        return cityService;
    }

    public HotelService getHotelService() {
        return hotelService;
    }

    public TourProviderService getTourProviderService() {
        return tourProviderService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public UserService getUserService() {
        return userService;
    }

    public StorageType getStorageType() {
        return storageType;
    }
}
