package hw11_tour.storage.services;

import hw11_tour.city.repo.CityRepo;
import hw11_tour.city.repo.impl.CityListRepo;
import hw11_tour.city.service.CityDefaultService;
import hw11_tour.country.repo.CountryRepo;
import hw11_tour.country.repo.impl.CountryListRepo;
import hw11_tour.country.service.CountryDefaultService;
import hw11_tour.hotel.repo.HotelRepo;
import hw11_tour.hotel.repo.impl.HotelListRepo;
import hw11_tour.hotel.service.HotelDefaultService;
import hw11_tour.order.repo.OrderRepo;
import hw11_tour.order.repo.impl.OrderListRepo;
import hw11_tour.order.service.OrderDefaultService;
import hw11_tour.tourprovider.repo.TourProviderRepo;
import hw11_tour.tourprovider.repo.impl.TourProviderListRepo;
import hw11_tour.tourprovider.service.TourProviderDefaultService;
import hw11_tour.user.repo.UserRepo;
import hw11_tour.user.repo.impl.UserListRepo;
import hw11_tour.user.service.UserDefaultService;


public class ListServiceFactory extends ServiceFactory {
    public ListServiceFactory() {
        UserRepo userRepo = new UserListRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityListRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryListRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelListRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderListRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderListRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
