package hw11_tour.storage.services;

import hw11_tour.city.repo.CityRepo;
import hw11_tour.city.repo.impl.CitySetRepo;
import hw11_tour.city.service.CityDefaultService;
import hw11_tour.country.repo.CountryRepo;
import hw11_tour.country.repo.impl.CountrySetRepo;
import hw11_tour.country.service.CountryDefaultService;
import hw11_tour.hotel.repo.HotelRepo;
import hw11_tour.hotel.repo.impl.HotelSetRepo;
import hw11_tour.hotel.service.HotelDefaultService;
import hw11_tour.order.repo.OrderRepo;
import hw11_tour.order.repo.impl.OrderSetRepo;
import hw11_tour.order.service.OrderDefaultService;
import hw11_tour.tourprovider.repo.TourProviderRepo;
import hw11_tour.tourprovider.repo.impl.TourProviderSetRepo;
import hw11_tour.tourprovider.service.TourProviderDefaultService;
import hw11_tour.user.repo.UserRepo;
import hw11_tour.user.repo.impl.UserSetRepo;
import hw11_tour.user.service.UserDefaultService;


public class SetServiceFactory extends ServiceFactory {
    public SetServiceFactory() {
        UserRepo userRepo = new UserSetRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CitySetRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountrySetRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelSetRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderSetRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderSetRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
