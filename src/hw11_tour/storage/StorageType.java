package hw11_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
