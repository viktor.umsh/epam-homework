package hw11_tour.tourprovider.sort;

import hw11_tour.base.sort.BaseSortCondition;
import hw11_tour.tourprovider.domain.TourProvider;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class TourProviderSortCondition extends BaseSortCondition<TourProvider> {

    private HashMap<Integer, TourProviderSortedField> sortedFields = new HashMap<>();

    public void addField(int priority, TourProviderSortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<TourProvider> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<TourProvider>() {
            @Override
            public int compare(TourProvider o1, TourProvider o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    TourProviderSortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case NAME:
                            compareResult = o1.getName().compareTo(o2.getName());
                            break;

                        case SERVICE_COST:
                            compareResult = Long.compare(o1.getSERVICE_COST(), o2.getSERVICE_COST());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }
}
