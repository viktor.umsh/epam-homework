package hw11_tour.tourprovider.sort;

import hw11_tour.base.sort.BaseField;

public enum TourProviderSortedField implements BaseField {
    NAME,
    SERVICE_COST
}
