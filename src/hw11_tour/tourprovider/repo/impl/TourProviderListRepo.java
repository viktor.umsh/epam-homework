package hw11_tour.tourprovider.repo.impl;

import hw11_tour.base.repo.ListRepo;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.tourprovider.repo.TourProviderIdGenerator;
import hw11_tour.tourprovider.repo.TourProviderRepo;

import static hw11_tour.storage.data.ListDataStorage.tourProviders;

public class TourProviderListRepo extends ListRepo<TourProvider> implements TourProviderRepo {
    public TourProviderListRepo() {
        dataList = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
