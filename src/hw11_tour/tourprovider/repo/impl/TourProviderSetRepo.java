package hw11_tour.tourprovider.repo.impl;

import hw11_tour.base.repo.SetRepo;
import hw11_tour.storage.data.SetDataStore;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.tourprovider.repo.TourProviderIdGenerator;
import hw11_tour.tourprovider.repo.TourProviderRepo;

public class TourProviderSetRepo extends SetRepo<TourProvider> implements TourProviderRepo {
    public TourProviderSetRepo() {
        dataSet = SetDataStore.tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
