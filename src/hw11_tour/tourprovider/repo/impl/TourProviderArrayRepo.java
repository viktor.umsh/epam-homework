package hw11_tour.tourprovider.repo.impl;

import hw11_tour.base.repo.ArrayRepo;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.tourprovider.repo.TourProviderIdGenerator;
import hw11_tour.tourprovider.repo.TourProviderRepo;

import static hw11_tour.storage.data.ArrayDataStore.tourProviders;


public class TourProviderArrayRepo extends ArrayRepo<TourProvider> implements TourProviderRepo {
    public TourProviderArrayRepo() {
        dataArray = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
