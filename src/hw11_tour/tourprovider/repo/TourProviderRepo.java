package hw11_tour.tourprovider.repo;

import hw11_tour.base.repo.BaseRepo;
import hw11_tour.tourprovider.domain.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
