package hw11_tour.tourprovider.parser;

import hw11_tour.base.parser.BasicParser;
import hw11_tour.country.domain.Country;
import hw11_tour.country.service.CountryService;
import hw11_tour.storage.services.ServicesStorage;
import hw11_tour.tourprovider.domain.TourProvider;
import hw11_tour.tourprovider.service.TourProviderService;

public class TourProviderParser extends BasicParser {

    private TourProviderService tourProviderMemoryService = ServicesStorage.getTourProviderService();
    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length <= 2) {
            return;
        }
        try {
            TourProvider tourProvider = new TourProvider(args[0], Long.parseLong(args[1]), Long.parseLong(args[2]));
            for (int i = 3; i < args.length; i++) {
                Country country = countryMemoryService.findByName(args[i]);
                if (country == null) {
                    country = new Country(args[i]);
                }
                tourProvider.addCountry(country);
            }
            tourProviderMemoryService.add(tourProvider);
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        }
    }
}
