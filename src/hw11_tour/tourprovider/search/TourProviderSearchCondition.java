package hw11_tour.tourprovider.search;

import hw11_tour.base.search.BaseSearchCondition;
import hw11_tour.tourprovider.domain.TourProvider;

public interface TourProviderSearchCondition extends BaseSearchCondition<TourProvider> {

}
