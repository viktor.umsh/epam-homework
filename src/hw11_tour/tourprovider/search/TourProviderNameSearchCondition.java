package hw11_tour.tourprovider.search;

import hw11_tour.tourprovider.domain.TourProvider;

public class TourProviderNameSearchCondition implements TourProviderSearchCondition {
    private String namePattern;

    public TourProviderNameSearchCondition(String namePattern) {
        this.namePattern = namePattern;
    }

    @Override
    public boolean isFit(TourProvider tourProvider) {
        if (tourProvider.getName() == null) {
            return false;
        }
        return tourProvider.getName().contains(namePattern);
    }
}
