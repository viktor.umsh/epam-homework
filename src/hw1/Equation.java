package hw1;

public class Equation {
    private double a = 0;
    private double b = 0;
    private double c = 0;
    private double x1 = 0;
    private double x2 = 0;

    private boolean isRoots;
    private boolean isX1;
    private boolean isX2;
    private int countRoots;

    public Equation(double a, double b, double c) {
        setNewValues(a, b, c);
    }

    public void setNewValues(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        calcRoots();
    }

    private void setRootData(boolean isRoots, boolean isX1, boolean isX2, int countRoots) {
        this.isRoots = isRoots;
        this.isX1 = isX1;
        this.isX2 = isX2;
        this.countRoots = countRoots;
    }

    private void calcRoots() {
        if (a == 0) {
            if (b == 0) {
                if (c == 0) {
                    setRootData(true, false, false, Integer.MAX_VALUE);
                } else {
                    setRootData(false, false, false, 0);
                }
            } else {
                x1 = -c / b;
                setRootData(true, true, false, 1);
            }
        } else {
            double d = b * b - 4 * a * c;
            if (d < 0) {
                setRootData(false, false, false, 0);
            } else {
                if (d == 0) {
                    x1 = -b / (2 * a);
                    setRootData(true, true, false, 1);
                } else {
                    x1 = (-b - Math.sqrt(d)) / (2 * a);
                    x2 = (-b + Math.sqrt(d)) / (2 * a);
                    setRootData(true, true, true, 2);
                }
            }
        }
    }

    public String equationToString() {
        return "" + a + "*x^2 + " + b + "*x + " + c + " = 0";
    }

    public String rootsToString() {
        if (isX1 && isX2) {
            return "Equation has two roots: x1 = " + x1 + " x2 = " + x2 + ".";
        }
        if (isX1 && !isX2) {
            return "Equation has only one root: x1 = " + x1 + ".";
        }
        if (!isX1 && !isX2 && !isRoots) {
            return "Equation has not any roots.";
        }
        if (!isX1 && !isX2 && isRoots) {
            return "Equation has continuum roots: any value will do.";
        }
        return "";
    }

    public static void main(String[] args) {
        Equation equation = new Equation(1, 2, 3);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        equation.setNewValues(0, 0, 0);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        equation.setNewValues(0, 0, 3);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        equation.setNewValues(0, 3, 6);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        equation.setNewValues(1, 0, 0);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        try {
            System.out.print(equation.equationToString() + " : Try to take x2: ");
            System.out.println(equation.getX2());
        } catch (HaveNotThisRootException e) {
            System.out.println(e.getMessage());
        }
        equation.setNewValues(1, -4, 4);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
        equation.setNewValues(1, -4, 2);
        System.out.println(equation.equationToString() + " : " + equation.rootsToString());
    }


    public double getX1() throws HaveNotThisRootException {
        if (!isX1) {
            throw new HaveNotThisRootException("This equation haven't x1 root.");
        }
        return x1;
    }

    public double getX2() throws HaveNotThisRootException {
        if (!isX2) {
            throw new HaveNotThisRootException("This equation haven't x2 root.");
        }
        return x2;
    }

    public boolean isX1() {
        return isX1;
    }

    public boolean isX2() {
        return isX2;
    }

    public boolean isRoots() {
        return isRoots;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
}

