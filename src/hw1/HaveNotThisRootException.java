package hw1;

public class HaveNotThisRootException extends Exception {
    public HaveNotThisRootException(String message) {
        super(message);
    }
}
