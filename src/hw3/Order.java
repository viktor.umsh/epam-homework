package hw3;

public class Order {
    private long price;
    private Hotel hotel;

    public Order(long price, Hotel hotel) {
        this.price = price;
        this.hotel = hotel;
    }

    public long getPrice() {
        return price;
    }

    public Hotel getHotel() {
        return hotel;
    }
}
