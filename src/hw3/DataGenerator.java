package hw3;

public class DataGenerator {
    private TourProvider tourProvider;
    private User vasiliy;

    public DataGenerator() {
    }

    public void test1() {
        Country italy = new Country("Italy");
        City melan = new City("Melan", new Coordinate(101, 2), italy);
        melan.addHotel(new Hotel("Reddison", 5, 97999, melan));
        melan.addHotel(new Hotel("SanPlegrin", 4, 49999, melan));

        City neapol = new City("Neapol", new Coordinate(98, 4), italy);
        neapol.addHotel(new Hotel("ReddisonNeapol", 5, 87999, neapol));
        neapol.addHotel(new Hotel("AllStars", 3, 17999, neapol));
        neapol.addHotel(new Hotel("PizzaHouse", 2, 8999, neapol));

        italy.addCity(melan);
        italy.addCity(neapol);
        tourProvider = new TourProvider(99, 2999);
        tourProvider.addCountry(italy);

        Country russia = new Country("russia");
        City Spb = new City("St-Peterburg", new Coordinate(0, 0), russia);
        vasiliy = new RandomMan("Vasiliy", Spb);
    }

    public TourProvider getTourProvider() {
        return tourProvider;
    }

    public User getVasiliy() {
        return vasiliy;
    }

    public static void main(String[] args) {
        DataGenerator generator = new DataGenerator();
        generator.test1();
        Order result = Controller.done(generator.getTourProvider(), generator.getVasiliy());
        System.out.println("User " + generator.getVasiliy().getName() + " choose order to " + result.getHotel().getName() + " hotel. Cost: " + result.getPrice());
    }

}
