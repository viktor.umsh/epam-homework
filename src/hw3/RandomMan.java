package hw3;

import java.util.ArrayList;
import java.util.Random;

public class RandomMan extends User {
    private Random random;

    public RandomMan(String name, City city) {
        super(name, city);
        random = new Random();
    }

    @Override
    public ArrayList<Country> chooseCounrties(ArrayList<Country> countries) {
        ArrayList<Country> choice = new ArrayList<Country>();
        choice.add(countries.get(random.nextInt(countries.size())));
        return choice;
    }

    @Override
    public ArrayList<City> chooseCities(ArrayList<City> cities) {
        ArrayList<City> choice = new ArrayList<City>();
        choice.add(cities.get(random.nextInt(cities.size())));
        return choice;
    }

    @Override
    public ArrayList<Hotel> chooseHotels(ArrayList<Hotel> hotels) {
        ArrayList<Hotel> choice = new ArrayList<Hotel>();
        choice.add(hotels.get(random.nextInt(hotels.size())));
        return choice;
    }

    @Override
    public Order chooseTour(ArrayList<Order> orders) {
        return orders.get(random.nextInt(orders.size()));
    }
}
