package hw3;

public class Coordinate {
    private final long latitude;    // x
    private final long longitude;   // y

    public Coordinate(long latitude, long longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public long lengthTo(Coordinate destination) {
        return (long) ((destination.latitude - latitude) * (destination.latitude - latitude) +
                (destination.longitude - longitude) * (destination.longitude - longitude));
    }
}
