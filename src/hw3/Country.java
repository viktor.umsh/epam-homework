package hw3;

import java.util.ArrayList;

public class Country {
    private String name;
    private ArrayList<City> cities;

    public Country(String name) {
        this.name = name;
        cities = new ArrayList<City>();
    }

    public void addCity(City city) {
        cities.add(city);
    }

    public String getName() {
        return name;
    }

    public ArrayList<City> getCities() {
        return cities;
    }
}
