package hw3;

import java.util.ArrayList;
import java.util.Objects;

public class City {
    private final String name;
    private final Coordinate coordinate;
    private final Country country;
    private ArrayList<Hotel> hotels;

    public City(String name, Coordinate coordinate, Country country) {
        this.name = name;
        this.coordinate = coordinate;
        this.country = country;
        hotels = new ArrayList<Hotel>();
    }

    public void addHotel(Hotel hotel) {
        this.hotels.add(hotel);
    }

    public ArrayList<Hotel> getHotels() {
        return hotels;
    }

    public String getName() {
        return name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name) &&
                Objects.equals(coordinate, city.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coordinate);
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", coordinate=" + coordinate +
                ", country=" + country +
                '}';
    }
}
