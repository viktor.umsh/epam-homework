package hw3;

import java.util.ArrayList;

public class RichMan extends User {

    public RichMan(String name, City city) {
        super(name, city);
    }

    @Override
    public ArrayList<Country> chooseCounrties(ArrayList<Country> countries) {
        return null;
    }

    @Override
    public ArrayList<City> chooseCities(ArrayList<City> cities) {
        return null;
    }

    @Override
    public ArrayList<Hotel> chooseHotels(ArrayList<Hotel> hotels) {
        return null;
    }

    @Override
    public Order chooseTour(ArrayList<Order> orders) {
        return null;
    }
}
