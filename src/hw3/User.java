package hw3;

import java.util.ArrayList;

abstract public class User {
    private String name;
    private City city;

    public User(String name, City city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    abstract public ArrayList<Country> chooseCounrties(ArrayList<Country> countries);

    abstract public ArrayList<City> chooseCities(ArrayList<City> cities);

    abstract public ArrayList<Hotel> chooseHotels(ArrayList<Hotel> hotels);

    abstract public Order chooseTour(ArrayList<Order> orders);
}
