package hw12_tour.order.sort;

import hw12_tour.base.sort.BaseField;

public enum OrderSortedField implements BaseField {
    PRICE,
    HOTEL,
    CITY
}
