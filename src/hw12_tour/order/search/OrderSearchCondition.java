package hw12_tour.order.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.order.domain.Order;

public interface OrderSearchCondition extends BaseSearchCondition<Order> {
}
