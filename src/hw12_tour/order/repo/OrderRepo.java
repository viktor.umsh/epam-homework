package hw12_tour.order.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.order.domain.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
