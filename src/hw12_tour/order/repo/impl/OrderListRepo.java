package hw12_tour.order.repo.impl;

import hw12_tour.base.repo.ListRepo;
import hw12_tour.order.domain.Order;
import hw12_tour.order.repo.OrderIdGenerator;
import hw12_tour.order.repo.OrderRepo;

import static hw12_tour.storage.data.ListDataStorage.orders;

public class OrderListRepo extends ListRepo<Order> implements OrderRepo {
    public OrderListRepo() {
        dataList = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
