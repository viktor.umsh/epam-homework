package hw12_tour.order.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.order.domain.Order;
import hw12_tour.order.repo.OrderIdGenerator;
import hw12_tour.order.repo.OrderRepo;
import hw12_tour.storage.data.SetDataStore;

public class OrderSetRepo extends SetRepo<Order> implements OrderRepo {
    public OrderSetRepo() {
        dataSet = SetDataStore.orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
