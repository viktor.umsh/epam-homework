package hw12_tour.order.service;

import hw12_tour.base.service.BaseService;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.order.domain.Order;

public abstract class OrderService extends BaseService<Order> {
    public int countByHotel(Hotel hotel) {
        int result=0;
        for (Order order : repository) {
            if (order.getHotel() == hotel) {
                result++;
            }
        }
        return result;
    }
}
