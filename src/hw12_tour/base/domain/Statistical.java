package hw12_tour.base.domain;

import hw12_tour.statistic.Statistic;

public abstract class Statistical {
    private Statistic statistic = new Statistic();

    public Statistic getStatistic() {
        return statistic;
    }

    public void updateStatistic(long newValue) {
        statistic.updateStatistic(newValue);
    }
}

