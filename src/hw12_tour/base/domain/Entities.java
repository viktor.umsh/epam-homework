package hw12_tour.base.domain;

public enum Entities {
    USER,
    COUNTRY,
    CITY,
    HOTEL,
    TOUR_PROVIDER,
    ORDER
}
