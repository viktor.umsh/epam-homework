package hw12_tour.base.service;

import hw12_tour.base.domain.BaseDomain;
import hw12_tour.base.repo.BaseRepo;
import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.base.sort.BaseSortCondition;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BaseService<Type extends BaseDomain> {
    protected BaseRepo<Type> repository;

    protected boolean checkNotNull(Type something) {
        return (something == null);
    }

    protected boolean basicAdd(Type something) {
        if (checkNotNull(something)) {
            return false;
        }
        Type somethingContained = findByEntity(something);
        if (somethingContained == null) {
            repository.add(something);
            return true;
        } else {
            somethingContained.update(something);
            return false;
        }
    }

    public abstract void initInsertedServices();

    public void add(Type something) {
        repository.add(something);
    }

    public Type get(int index) {
        return repository.get(index);
    }

    public Type findById(long id) {
        return repository.findById(id);
    }

    public Type findByName(String name) {
        return repository.findByName(name);
    }

    /**
     * @param something than finding
     * @return index of something in storage or -1 if not exist
     */
    public Type findByEntity(Type something) {
        return repository.findByEntity(something);
    }

    public void update(Type something) {
        repository.update(something);
    }

    public boolean delete(Type something) {
        return repository.delete(something);
    }

    public Type delete(int index) {
        Type deleted = repository.get(index);
        delete(deleted);
        return deleted;
    }

    public void printAll(PrintStream out) {
        for (Type elem : repository) {
            out.println(elem);
        }
        out.println();
    }

    public List<Type> search(BaseSearchCondition<Type> searchCondition) {
        List<Type> result = new ArrayList<>();
        for (Type something : repository) {
            if (searchCondition.isFit(something)) {
                result.add(something);
            }
        }
        return result;
    }

    public List<Type> repoToList() {
        List<Type> repoList = new ArrayList<>();
        for (Type elem : repository) {
            repoList.add(elem);
        }
        return repoList;
    }


    public List<Type> sort(BaseSortCondition<Type> sortCondition) {
        List<Type> repoList = repoToList();
        Collections.sort(repoList, sortCondition.getComparator());
        return repoList;
    }
}
