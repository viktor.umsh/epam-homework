package hw12_tour.base.exceptions;

public class BaseHotelUncheckedException extends RuntimeException {
    protected int code;

    public BaseHotelUncheckedException(int code, String message) {
        super(message);
        this.code = code;
    }

    public BaseHotelUncheckedException(int code, String message, Exception cause) {
        super(message);
        this.code = code;
        initCause(cause);
    }
}
