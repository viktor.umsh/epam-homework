package hw12_tour.base.exceptions;

public class BaseHotelCheckedException extends Exception {
    protected int code;

    public BaseHotelCheckedException(int code, String message) {
        super(message);
        this.code = code;
    }

    public BaseHotelCheckedException(int code, String message, Exception cause) {
        super(message);
        this.code = code;
        initCause(cause);
    }
}
