package hw12_tour.country.service;

import hw12_tour.base.service.BaseService;
import hw12_tour.country.domain.Country;

public abstract class CountryService extends BaseService<Country> {
}
