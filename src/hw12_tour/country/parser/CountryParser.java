package hw12_tour.country.parser;

import hw12_tour.base.parser.BasicParser;
import hw12_tour.country.domain.Country;
import hw12_tour.country.service.CountryService;
import hw12_tour.storage.services.ServicesStorage;

public class CountryParser extends BasicParser {

    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 1) {
            countryMemoryService.add(new Country(args[0]));
        }
    }

}
