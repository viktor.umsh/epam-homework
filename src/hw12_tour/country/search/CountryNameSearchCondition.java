package hw12_tour.country.search;

import hw12_tour.country.domain.Country;

public class CountryNameSearchCondition implements CountrySearchCondition {
    private String namePattern;

    public CountryNameSearchCondition(String namePattern) {
        this.namePattern = namePattern;
    }

    @Override
    public boolean isFit(Country country) {
        if (country.getName() == null) {
            return false;
        }
        return country.getName().contains(namePattern);
    }
}
