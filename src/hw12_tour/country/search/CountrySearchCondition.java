package hw12_tour.country.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.country.domain.Country;

public interface CountrySearchCondition extends BaseSearchCondition<Country> {
}
