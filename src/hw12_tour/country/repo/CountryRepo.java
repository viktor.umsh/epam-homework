package hw12_tour.country.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.country.domain.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
