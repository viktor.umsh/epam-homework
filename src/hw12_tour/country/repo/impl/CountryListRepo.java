package hw12_tour.country.repo.impl;

import hw12_tour.base.repo.ListRepo;
import hw12_tour.country.domain.Country;
import hw12_tour.country.repo.CountryIdGenerator;
import hw12_tour.country.repo.CountryRepo;

import static hw12_tour.storage.data.ListDataStorage.countries;

public class CountryListRepo extends ListRepo<Country> implements CountryRepo {
    public CountryListRepo() {
        dataList = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
