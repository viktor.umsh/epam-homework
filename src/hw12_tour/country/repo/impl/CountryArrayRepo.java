package hw12_tour.country.repo.impl;

import hw12_tour.base.repo.ArrayRepo;
import hw12_tour.country.domain.Country;
import hw12_tour.country.repo.CountryIdGenerator;
import hw12_tour.country.repo.CountryRepo;

import static hw12_tour.storage.data.ArrayDataStore.countries;

public class CountryArrayRepo extends ArrayRepo<Country> implements CountryRepo {
    public CountryArrayRepo() {
        dataArray = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
