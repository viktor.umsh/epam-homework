package hw12_tour.country.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.country.domain.Country;
import hw12_tour.country.repo.CountryIdGenerator;
import hw12_tour.country.repo.CountryRepo;
import hw12_tour.storage.data.SetDataStore;

public class CountrySetRepo extends SetRepo<Country> implements CountryRepo {
    public CountrySetRepo() {
        dataSet = SetDataStore.countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
