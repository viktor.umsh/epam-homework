package hw12_tour.city.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.city.domain.City;
import hw12_tour.city.repo.CityIdGenerator;
import hw12_tour.city.repo.CityRepo;
import hw12_tour.storage.data.SetDataStore;

public class CitySetRepo extends SetRepo<City> implements CityRepo {
    public CitySetRepo() {
        dataSet = SetDataStore.cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }

}
