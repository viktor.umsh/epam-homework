package hw12_tour.city.repo.impl;

import hw12_tour.base.repo.BaseIdGenerator;
import hw12_tour.base.repo.ListRepo;
import hw12_tour.city.domain.City;
import hw12_tour.city.repo.CityRepo;

import static hw12_tour.storage.data.ListDataStorage.cities;

public class CityListRepo extends ListRepo<City> implements CityRepo {
    public CityListRepo() {
        dataList = cities;
    }

    @Override
    protected long generateNextId() {
        return BaseIdGenerator.nextId();
    }

}
