package hw12_tour.city.repo.impl;

import hw12_tour.base.repo.ArrayRepo;
import hw12_tour.city.domain.City;
import hw12_tour.city.repo.CityIdGenerator;
import hw12_tour.city.repo.CityRepo;

import static hw12_tour.storage.data.ArrayDataStore.cities;

public class CityArrayRepo extends ArrayRepo<City> implements CityRepo {
    public CityArrayRepo() {
        dataArray = cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }
}
