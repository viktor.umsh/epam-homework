package hw12_tour.city.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.city.domain.City;

public interface CityRepo extends BaseRepo<City> {
}
