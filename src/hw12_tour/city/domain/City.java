package hw12_tour.city.domain;

import hw12_tour.base.domain.BaseDomain;
import hw12_tour.country.domain.Country;
import hw12_tour.hotel.domain.Hotel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class City extends BaseDomain { // implements Statistical
    private String name;
    private Coordinate coordinate;
    private Country country;
    private ArrayList<Hotel> hotels;
    private Climate climate;
//    private Statistic statistic;

    public City(String name, Coordinate coordinate, Country country, Climate climate) {
        this.name = name;
        this.coordinate = coordinate;
        this.country = country;
        this.climate = climate;
        this.hotels = new ArrayList<>();
        //    this.statistic = new Statistic();
    }

    public void addHotel(Hotel hotel) {
        if (!hotels.contains(hotel)) {
            hotels.add(hotel);
        }
    }

    public void addHotels(Collection<Hotel> hotels) {
        hotels.addAll(hotels);
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public boolean deleteHotel(Hotel hotel) {
        return this.hotels.remove(hotel);
    }

    public Collection<Hotel> getHotels() {
        return hotels;
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof City) || this == something) {
            return;
        }
        City someCity = (City) something;
        if (someCity.climate != null) {
            this.climate = someCity.climate;
        }
        if (someCity.country != null) {
            this.country = someCity.country;
        }
        if (someCity.coordinate != null) {
            this.coordinate = someCity.coordinate;
        }
        if (someCity.name != null) {
            this.name = someCity.name;
        }
        someCity.getHotels().removeAll(hotels);
        hotels.addAll(someCity.hotels);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Country getCountry() {
        return country;
    }

    public Climate getClimate() {
        return climate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name); // && Objects.equals(coordinate, city.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coordinate);
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", coordinate=" + coordinate +
                ", country=" + country +
                ", climate=" + climate +
                ", id=" + id +
                '}';
    }
//    @Override
//    public Statistic getStatistic() {
//        return statistic;
//    }
//
//    @Override
//    public void updateStatistic(long newValue) {
//        statistic.updateStatistic(newValue);
//    }
}
