package hw12_tour.city.sort;

import hw12_tour.base.sort.BaseField;

public enum CitySortedField implements BaseField {
    NAME,
    COUNTRY,
    CLIMATE
}

