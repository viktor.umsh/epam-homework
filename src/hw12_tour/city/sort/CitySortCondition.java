package hw12_tour.city.sort;

import hw12_tour.base.sort.BaseSortCondition;
import hw12_tour.city.domain.City;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class CitySortCondition extends BaseSortCondition<City> {

    private HashMap<Integer, CitySortedField> sortedFields = new HashMap<>();

    public void addField(int priority, CitySortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<City> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    CitySortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case NAME:
                            compareResult = o1.getName().compareTo(o2.getName());
                            break;

                        case CLIMATE:
                            compareResult = o1.getClimate().compareTo(o2.getClimate());
                            break;

                        case COUNTRY:
                            compareResult = o1.getCountry().getName().compareTo(o2.getCountry().getName());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }
}
