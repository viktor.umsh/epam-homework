package hw12_tour.city.search;

import hw12_tour.city.domain.City;
import hw12_tour.city.domain.Climate;

public class CityClimateSearchCondition implements CitySearchCondition {

    private Climate soughtClimate;

    public CityClimateSearchCondition(Climate soughtClimate) {
        this.soughtClimate = soughtClimate;
    }

    @Override
    public boolean isFit(City city) {
        if (city.getClimate() == null) {
            return false;
        }
        return (city.getClimate().equals(soughtClimate));
    }
}
