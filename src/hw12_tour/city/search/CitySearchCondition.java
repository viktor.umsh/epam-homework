package hw12_tour.city.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.city.domain.City;

public interface CitySearchCondition extends BaseSearchCondition<City> {
}
