package hw12_tour.city.service;

import hw12_tour.base.service.BaseService;
import hw12_tour.city.domain.City;

public abstract class CityService extends BaseService<City> {
}
