package hw12_tour.tourprovider.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.storage.data.SetDataStore;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.tourprovider.repo.TourProviderIdGenerator;
import hw12_tour.tourprovider.repo.TourProviderRepo;

public class TourProviderSetRepo extends SetRepo<TourProvider> implements TourProviderRepo {
    public TourProviderSetRepo() {
        dataSet = SetDataStore.tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
