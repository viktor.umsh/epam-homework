package hw12_tour.tourprovider.repo.impl;

import hw12_tour.base.repo.ArrayRepo;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.tourprovider.repo.TourProviderIdGenerator;
import hw12_tour.tourprovider.repo.TourProviderRepo;

import static hw12_tour.storage.data.ArrayDataStore.tourProviders;


public class TourProviderArrayRepo extends ArrayRepo<TourProvider> implements TourProviderRepo {
    public TourProviderArrayRepo() {
        dataArray = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
