package hw12_tour.tourprovider.repo.impl;

import hw12_tour.base.repo.ListRepo;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.tourprovider.repo.TourProviderIdGenerator;
import hw12_tour.tourprovider.repo.TourProviderRepo;

import static hw12_tour.storage.data.ListDataStorage.tourProviders;

public class TourProviderListRepo extends ListRepo<TourProvider> implements TourProviderRepo {
    public TourProviderListRepo() {
        dataList = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
