package hw12_tour.tourprovider.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.tourprovider.domain.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
