package hw12_tour.tourprovider.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.tourprovider.domain.TourProvider;

public interface TourProviderSearchCondition extends BaseSearchCondition<TourProvider> {

}
