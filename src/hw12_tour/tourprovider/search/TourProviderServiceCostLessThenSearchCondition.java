package hw12_tour.tourprovider.search;

import hw12_tour.tourprovider.domain.TourProvider;

public class TourProviderServiceCostLessThenSearchCondition implements TourProviderSearchCondition {
    private long serviceCostLessThen;

    public TourProviderServiceCostLessThenSearchCondition(long serviceCostLessThen) {
        this.serviceCostLessThen = serviceCostLessThen;
    }

    @Override
    public boolean isFit(TourProvider tourProvider) {
        return (tourProvider.getSERVICE_COST() < serviceCostLessThen);
    }
}
