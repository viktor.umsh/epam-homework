package hw12_tour.tourprovider.service;

import hw12_tour.country.domain.Country;
import hw12_tour.country.service.CountryService;
import hw12_tour.storage.services.ServicesStorage;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.tourprovider.repo.TourProviderRepo;


public class TourProviderDefaultService extends TourProviderService {
    private CountryService countryService;

    public TourProviderDefaultService(TourProviderRepo tourProviderRepo) {
        repository = tourProviderRepo;
    }

    @Override
    public void add(TourProvider tourProvider) {
        if (basicAdd(tourProvider)) {
            for (Country country : tourProvider.getCountries()) {
                countryService.add(country);
            }
        }
    }

    @Override
    public void initInsertedServices() {
        countryService = ServicesStorage.getCountryService();
    }

    @Override
    public boolean delete(TourProvider tourProvider) {
        for (Country country : tourProvider.getCountries()) {
            countryService.delete(country);
        }
        return tourProviderRepo.delete(tourProvider);
    }
}
