package hw12_tour.tourprovider.sort;

import hw12_tour.base.sort.BaseField;

public enum TourProviderSortedField implements BaseField {
    NAME,
    SERVICE_COST
}
