package hw12_tour.user.domain;

import hw12_tour.city.domain.City;
import hw12_tour.country.domain.Country;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.order.domain.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomMan extends AbstractUser {
    private Random random;

    public RandomMan(String name, City city) {
        super(name, city);
        random = new Random();
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        ArrayList<Country> choice = new ArrayList<>();
        choice.add(countries.get(random.nextInt(countries.size())));
        return choice;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        ArrayList<City> choice = new ArrayList<>();
        choice.add(cities.get(random.nextInt(cities.size())));
        return choice;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        ArrayList<Hotel> choice = new ArrayList<>();
        choice.add(hotels.get(random.nextInt(hotels.size())));
        return choice;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return orders.get(random.nextInt(orders.size()));
    }
}
