package hw12_tour.user.domain;

import hw12_tour.city.domain.City;
import hw12_tour.country.domain.Country;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.order.domain.Order;

import java.util.List;

public class RichMan extends AbstractUser {

    public RichMan(String name, City city) {
        super(name, city);
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        return null;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        return null;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        return null;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return null;
    }
}
