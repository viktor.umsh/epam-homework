package hw12_tour.user.service;

import hw12_tour.base.service.BaseService;
import hw12_tour.user.domain.AbstractUser;

public abstract class UserService extends BaseService<AbstractUser> {
}
