package hw12_tour.user.repo.impl;

import hw12_tour.base.repo.ArrayRepo;
import hw12_tour.user.domain.AbstractUser;
import hw12_tour.user.repo.UserIdGenerator;
import hw12_tour.user.repo.UserRepo;

import static hw12_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo extends ArrayRepo<AbstractUser> implements UserRepo {
    public UserArrayRepo() {
        dataArray = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
