package hw12_tour.user.repo.impl;

import hw12_tour.base.repo.ListRepo;
import hw12_tour.user.domain.AbstractUser;
import hw12_tour.user.repo.UserIdGenerator;
import hw12_tour.user.repo.UserRepo;

import static hw12_tour.storage.data.ListDataStorage.users;

public class UserListRepo extends ListRepo<AbstractUser> implements UserRepo {
    public UserListRepo() {
        dataList = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
