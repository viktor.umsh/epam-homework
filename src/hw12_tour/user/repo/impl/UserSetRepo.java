package hw12_tour.user.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.storage.data.SetDataStore;
import hw12_tour.user.domain.AbstractUser;
import hw12_tour.user.repo.UserIdGenerator;
import hw12_tour.user.repo.UserRepo;

public class UserSetRepo extends SetRepo<AbstractUser> implements UserRepo {
    public UserSetRepo() {
        dataSet = SetDataStore.users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
