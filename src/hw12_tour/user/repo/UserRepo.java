package hw12_tour.user.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.user.domain.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
