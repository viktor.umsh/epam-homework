package hw12_tour.user.sort;

import hw12_tour.base.sort.BaseField;

public enum UserSortedField implements BaseField {
    NAME,
    CITY,
    COUNTRY
}
