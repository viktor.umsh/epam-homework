package hw12_tour.user.sort;

import hw12_tour.base.sort.BaseSortCondition;
import hw12_tour.user.domain.AbstractUser;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class UserSortCondition extends BaseSortCondition<AbstractUser> {

    private HashMap<Integer, UserSortedField> sortedFields = new HashMap<>();

    public void addField(int priority, UserSortedField sortedField) {
        sortedFields.put(priority, sortedField);
    }

    @Override
    public Comparator<AbstractUser> getComparator() {
        List<Integer> priorityList = getSortedPriorities(sortedFields);
        return new Comparator<AbstractUser>() {
            @Override
            public int compare(AbstractUser o1, AbstractUser o2) {
                int compareResult = 0;
                for (int cur : priorityList) {
                    UserSortedField currentField = sortedFields.get(cur);
                    switch (currentField) {
                        case NAME:
                            compareResult = o1.getName().compareTo(o2.getName());
                            break;

                        case CITY:
                            compareResult = o1.getCity().getName().compareTo(o2.getCity().getName());
                            break;

                        case COUNTRY:
                            compareResult = o1.getCity().getCountry().getName().compareTo(o2.getCity().getCountry().getName());
                            break;
                    }
                    if (compareResult != 0) {
                        return compareResult;
                    }
                }
                return compareResult;
            }
        };
    }

}
