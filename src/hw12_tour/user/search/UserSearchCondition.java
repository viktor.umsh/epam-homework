package hw12_tour.user.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.user.domain.AbstractUser;

public interface UserSearchCondition extends BaseSearchCondition<AbstractUser> {
}
