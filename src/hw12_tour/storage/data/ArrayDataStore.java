package hw12_tour.storage.data;

import hw12_tour.city.domain.City;
import hw12_tour.country.domain.Country;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.order.domain.Order;
import hw12_tour.statistic.Statistic;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.user.domain.AbstractUser;
import hw12_tour.util.Array;

public class ArrayDataStore {
    public static Array<Country> countries;
    public static Array<City> cities;
    public static Array<Hotel> hotels;
    public static Array<TourProvider> tourProviders;
    public static Array<Order> orders;
    public static Array<AbstractUser> users;
    public static Array<Statistic> statistics;


    static void initStore() {
        countries = new Array<>();
        cities = new Array<>();
        hotels = new Array<>();
        tourProviders = new Array<>();
        orders = new Array<>();
        users = new Array<>();
        statistics = new Array<>();
    }
}
