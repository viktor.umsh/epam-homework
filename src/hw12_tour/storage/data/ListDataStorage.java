package hw12_tour.storage.data;

import hw12_tour.city.domain.City;
import hw12_tour.country.domain.Country;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.order.domain.Order;
import hw12_tour.statistic.Statistic;
import hw12_tour.tourprovider.domain.TourProvider;
import hw12_tour.user.domain.AbstractUser;

import java.util.ArrayList;
import java.util.List;

public class ListDataStorage {
    public static List<Country> countries;
    public static List<City> cities;
    public static List<Hotel> hotels;
    public static List<TourProvider> tourProviders;
    public static List<Order> orders;
    public static List<AbstractUser> users;
    public static List<Statistic> statistics;

    static void initStore() {
        countries = new ArrayList<>();
        cities = new ArrayList<>();
        hotels = new ArrayList<>();
        tourProviders = new ArrayList<>();
        orders = new ArrayList<>();
        users = new ArrayList<>();
        statistics = new ArrayList<>();
        users = new ArrayList<>();
    }

}
