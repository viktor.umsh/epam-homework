package hw12_tour.storage.services;

import hw12_tour.city.repo.CityRepo;
import hw12_tour.city.repo.impl.CitySetRepo;
import hw12_tour.city.service.CityDefaultService;
import hw12_tour.country.repo.CountryRepo;
import hw12_tour.country.repo.impl.CountrySetRepo;
import hw12_tour.country.service.CountryDefaultService;
import hw12_tour.hotel.repo.HotelRepo;
import hw12_tour.hotel.repo.impl.HotelSetRepo;
import hw12_tour.hotel.service.HotelDefaultService;
import hw12_tour.order.repo.OrderRepo;
import hw12_tour.order.repo.impl.OrderSetRepo;
import hw12_tour.order.service.OrderDefaultService;
import hw12_tour.tourprovider.repo.TourProviderRepo;
import hw12_tour.tourprovider.repo.impl.TourProviderSetRepo;
import hw12_tour.tourprovider.service.TourProviderDefaultService;
import hw12_tour.user.repo.UserRepo;
import hw12_tour.user.repo.impl.UserSetRepo;
import hw12_tour.user.service.UserDefaultService;


public class SetServiceFactory extends ServiceFactory {
    public SetServiceFactory() {
        UserRepo userRepo = new UserSetRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CitySetRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountrySetRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelSetRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderSetRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderSetRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
