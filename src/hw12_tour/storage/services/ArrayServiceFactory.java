package hw12_tour.storage.services;

import hw12_tour.city.repo.CityRepo;
import hw12_tour.city.repo.impl.CityArrayRepo;
import hw12_tour.city.service.CityDefaultService;
import hw12_tour.country.repo.CountryRepo;
import hw12_tour.country.repo.impl.CountryArrayRepo;
import hw12_tour.country.service.CountryDefaultService;
import hw12_tour.hotel.repo.HotelRepo;
import hw12_tour.hotel.repo.impl.HotelArrayRepo;
import hw12_tour.hotel.service.HotelDefaultService;
import hw12_tour.order.repo.OrderRepo;
import hw12_tour.order.repo.impl.OrderArrayRepo;
import hw12_tour.order.service.OrderDefaultService;
import hw12_tour.tourprovider.repo.TourProviderRepo;
import hw12_tour.tourprovider.repo.impl.TourProviderArrayRepo;
import hw12_tour.tourprovider.service.TourProviderDefaultService;
import hw12_tour.user.repo.UserRepo;
import hw12_tour.user.repo.impl.UserArrayRepo;
import hw12_tour.user.service.UserDefaultService;


public class ArrayServiceFactory extends ServiceFactory {

    public ArrayServiceFactory() {
        UserRepo userRepo = new UserArrayRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityArrayRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryArrayRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelArrayRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderArrayRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderArrayRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
