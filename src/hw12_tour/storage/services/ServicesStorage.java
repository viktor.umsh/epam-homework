package hw12_tour.storage.services;

import hw12_tour.base.service.BaseService;
import hw12_tour.city.service.CityService;
import hw12_tour.country.service.CountryService;
import hw12_tour.hotel.service.HotelService;
import hw12_tour.order.service.OrderService;
import hw12_tour.storage.EssenceType;
import hw12_tour.storage.StorageType;
import hw12_tour.storage.data.DataStoreFactory;
import hw12_tour.tourprovider.service.TourProviderService;
import hw12_tour.user.service.UserService;


public class ServicesStorage {
    private static StorageType storageType = null;
    private static ServiceFactory serviceFactory;


    public static void setStorageType(StorageType storageType) {
        ServicesStorage.storageType = storageType;
        DataStoreFactory.init(storageType);
        initService();
        serviceFactory.setInsertedInitServices();
    }

    private static void initService() {
        switch (storageType) {
            case LIST:
                serviceFactory = new ListServiceFactory();
                break;
            case ARRAY:
                serviceFactory = new ArrayServiceFactory();
                break;
            case SET:
                serviceFactory = new SetServiceFactory();
                break;
        }
    }

    public static BaseService gerService(EssenceType something) {
        switch (something) {
            case CITY:
                return getCityService();
            case COUNTRY:
                return getCountryService();
            case HOTEL:
                return getHotelService();
            case TOUR_PROVIDER:
                return getTourProviderService();
            case ORDER:
                return getOrderService();
            case USER:
                return getUserService();
        }
        return null;
    }

    public static CountryService getCountryService() {
        return serviceFactory.getCountryService();
    }

    public static CityService getCityService() {
        return serviceFactory.getCityService();
    }

    public static HotelService getHotelService() {
        return serviceFactory.getHotelService();
    }

    public static TourProviderService getTourProviderService() {
        return serviceFactory.getTourProviderService();
    }

    public static OrderService getOrderService() {
        return serviceFactory.getOrderService();
    }

    public static UserService getUserService() {
        return serviceFactory.getUserService();
    }

    public static StorageType getStorageType() {
        return storageType;
    }
}

