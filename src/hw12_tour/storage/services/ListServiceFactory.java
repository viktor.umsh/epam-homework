package hw12_tour.storage.services;

import hw12_tour.city.repo.CityRepo;
import hw12_tour.city.repo.impl.CityListRepo;
import hw12_tour.city.service.CityDefaultService;
import hw12_tour.country.repo.CountryRepo;
import hw12_tour.country.repo.impl.CountryListRepo;
import hw12_tour.country.service.CountryDefaultService;
import hw12_tour.hotel.repo.HotelRepo;
import hw12_tour.hotel.repo.impl.HotelListRepo;
import hw12_tour.hotel.service.HotelDefaultService;
import hw12_tour.order.repo.OrderRepo;
import hw12_tour.order.repo.impl.OrderListRepo;
import hw12_tour.order.service.OrderDefaultService;
import hw12_tour.tourprovider.repo.TourProviderRepo;
import hw12_tour.tourprovider.repo.impl.TourProviderListRepo;
import hw12_tour.tourprovider.service.TourProviderDefaultService;
import hw12_tour.user.repo.UserRepo;
import hw12_tour.user.repo.impl.UserListRepo;
import hw12_tour.user.service.UserDefaultService;


public class ListServiceFactory extends ServiceFactory {
    public ListServiceFactory() {
        UserRepo userRepo = new UserListRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityListRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryListRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelListRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderListRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderListRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
