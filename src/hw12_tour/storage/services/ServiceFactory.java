package hw12_tour.storage.services;

import hw12_tour.city.service.CityService;
import hw12_tour.country.service.CountryService;
import hw12_tour.hotel.service.HotelService;
import hw12_tour.order.service.OrderService;
import hw12_tour.storage.StorageType;
import hw12_tour.tourprovider.service.TourProviderService;
import hw12_tour.user.service.UserService;

public abstract class ServiceFactory {

    protected CountryService countryService;
    protected CityService cityService;
    protected HotelService hotelService;
    protected TourProviderService tourProviderService;
    protected OrderService orderService;
    protected UserService userService;
    protected StorageType storageType;

    protected void setInsertedInitServices() {
        countryService.initInsertedServices();
        cityService.initInsertedServices();
        hotelService.initInsertedServices();
        tourProviderService.initInsertedServices();
        orderService.initInsertedServices();
        userService.initInsertedServices();
    }

    public CountryService getCountryService() {
        return countryService;
    }

    public CityService getCityService() {
        return cityService;
    }

    public HotelService getHotelService() {
        return hotelService;
    }

    public TourProviderService getTourProviderService() {
        return tourProviderService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public UserService getUserService() {
        return userService;
    }

    public StorageType getStorageType() {
        return storageType;
    }
}
