package hw12_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
