package hw12_tour.dataReader.xmlDataReader.elemeptParsers;

import hw12_tour.city.domain.City;
import hw12_tour.country.domain.Country;
import hw12_tour.dataReader.xmlDataReader.CustomStaxReader;
import hw12_tour.dataReader.xmlDataReader.XMLElementParser;
import hw12_tour.dataReader.xmlDataReader.XMLMultipleParser;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class CountryParser implements XMLElementParser<Country> {

    @Override
    public Country parse(XMLStreamReader reader) throws XMLStreamException {
        String name = null;
        List<City> cities = null;
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "name": {
                            name = CustomStaxReader.readContent(reader);
                            break;
                        }
                        case "cities": {
                            XMLMultipleParser<City> countryXMLMultipleParser = new XMLMultipleParser<>(reader, City.class);
                            cities = countryXMLMultipleParser.multipleParse();
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    if (name == null) {
                        throw new RuntimeException("Name of user not  initialized!");
                    }
                    Country country = new Country(name);
                    if (cities != null) {
                        for (City city : cities){
                            city.setCountry(country);
                        }
                    }
                    country.addCities(cities);
                    return country;
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }
}
