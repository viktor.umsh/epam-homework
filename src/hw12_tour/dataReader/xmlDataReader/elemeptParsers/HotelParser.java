package hw12_tour.dataReader.xmlDataReader.elemeptParsers;

import hw12_tour.dataReader.xmlDataReader.CustomStaxReader;
import hw12_tour.dataReader.xmlDataReader.XMLElementParser;
import hw12_tour.hotel.domain.Hotel;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class HotelParser implements XMLElementParser<Hotel> {

    @Override
    public Hotel parse(XMLStreamReader reader) throws XMLStreamException {
        String name = null;
        long price=0;
        int stars=0;
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String currentElement = reader.getLocalName();
                    if (currentElement == null) {
                        break;
                    }
                    switch (currentElement) {
                        case "name": {
                            name = CustomStaxReader.readContent(reader);
                            break;
                        }
                        case "price": {
                            price = Long.parseLong(CustomStaxReader.readContent(reader));
                            break;
                        }
                        case "stars": {
                            stars = Integer.parseInt(CustomStaxReader.readContent(reader));
                            break;
                        }
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    if (name == null) {
                        throw new RuntimeException("Name of user not  initialized!");
                    }
                    Hotel hotel = new Hotel(name, stars, price, null);
                    return hotel;
                }
            }
        }
        throw new RuntimeException("Have not close tag");
    }
}
