package hw12_tour.dataReader.xmlDataReader;

import hw12_tour.base.domain.BaseDomain;
import hw12_tour.base.domain.Entities;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayList;
import java.util.List;

public class XMLMultipleParser<Type extends BaseDomain> {
    private List<Type> elements;
    private XMLElementParser<Type> elementParser;
    private XMLStreamReader reader;
    private Entities parsingEntity;

    public XMLMultipleParser(XMLStreamReader reader, Class parsingClass) {
        elements = new ArrayList<>();
        parsingEntity = TagStore.getEntity(parsingClass);
        this.elementParser = TagStore.getParser(parsingEntity);
        this.reader = reader;
    }

    public List<Type> multipleParse() throws XMLStreamException {
        if (parsingEntity == null) {
            throw new RuntimeException("Have no parsing entity!");
        }
        String readTeg = TagStore.getTagName(parsingEntity);
        while (reader.hasNext()) {
            int currentElementType = reader.next();
            switch (currentElementType) {
                case XMLStreamReader.START_ELEMENT: {
                    String elementName = reader.getLocalName();
                    if (readTeg.equals(elementName)) {
                        elements.add((Type) TagStore.getParser(parsingEntity).parse(reader));
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    return elements;
                }
            }
        }
        throw new RuntimeException("I didn't find suitable end tag");
    }

}
