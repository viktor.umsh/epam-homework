package hw12_tour.hotel.exceptions;

import hw12_tour.base.exceptions.BaseHotelCheckedException;

public class DeleteHotelException extends BaseHotelCheckedException {
    public DeleteHotelException(int code, String message) {
        super(code, message);
    }

    public DeleteHotelException(int code, String message, Exception cause) {
        super(code, message, cause);
    }
}
