package hw12_tour.hotel.exceptions;

public enum HotelExceptionMeta {
    DELETE_HOTEL_EXCEPTION(1, "Can not remove hotel, because there is an orders in this hotel!");

    private int code;
    private String description;

    HotelExceptionMeta(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
