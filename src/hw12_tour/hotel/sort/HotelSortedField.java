package hw12_tour.hotel.sort;

import hw12_tour.base.sort.BaseField;

public enum HotelSortedField implements BaseField {
    NAME,
    COUNTRY,
    CITY,
    PRICE
}
