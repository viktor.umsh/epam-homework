package hw12_tour.hotel.repo;

import hw12_tour.base.repo.BaseRepo;
import hw12_tour.hotel.domain.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
