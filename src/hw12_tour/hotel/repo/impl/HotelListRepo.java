package hw12_tour.hotel.repo.impl;

import hw12_tour.base.repo.ListRepo;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.hotel.repo.HotelIdGenerator;
import hw12_tour.hotel.repo.HotelRepo;

import static hw12_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo extends ListRepo<Hotel> implements HotelRepo {
    public HotelListRepo() {
        dataList = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
