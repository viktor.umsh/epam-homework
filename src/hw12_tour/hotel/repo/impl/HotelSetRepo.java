package hw12_tour.hotel.repo.impl;

import hw12_tour.base.repo.SetRepo;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.hotel.repo.HotelIdGenerator;
import hw12_tour.hotel.repo.HotelRepo;
import hw12_tour.storage.data.SetDataStore;

public class HotelSetRepo extends SetRepo<Hotel> implements HotelRepo {
    public HotelSetRepo() {
        dataSet = SetDataStore.hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
