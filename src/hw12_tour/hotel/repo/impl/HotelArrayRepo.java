package hw12_tour.hotel.repo.impl;

import hw12_tour.base.repo.ArrayRepo;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.hotel.repo.HotelIdGenerator;
import hw12_tour.hotel.repo.HotelRepo;

import static hw12_tour.storage.data.ArrayDataStore.hotels;

public class HotelArrayRepo extends ArrayRepo<Hotel> implements HotelRepo {
    public HotelArrayRepo() {
        dataArray = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
