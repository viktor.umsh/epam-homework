package hw12_tour.hotel.search;

import hw12_tour.hotel.domain.Hotel;

public class HotelPriceLessThenSearchCondition implements HotelSearchCondition {
    private long priceLessThen;

    public HotelPriceLessThenSearchCondition(long priceLessThen) {
        this.priceLessThen = priceLessThen;
    }

    @Override
    public boolean isFit(Hotel something) {
        return (something.getPrice() < priceLessThen);
    }
}
