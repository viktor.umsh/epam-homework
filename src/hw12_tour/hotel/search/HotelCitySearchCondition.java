package hw12_tour.hotel.search;

import hw12_tour.city.domain.City;
import hw12_tour.hotel.domain.Hotel;

public class HotelCitySearchCondition implements HotelSearchCondition {
    private City soughtCity;

    public HotelCitySearchCondition(City soughtCity) {
        this.soughtCity = soughtCity;
    }

    @Override
    public boolean isFit(Hotel hotel) {
        if (hotel.getCity() == null) {
            return false;
        }
        return hotel.getCity().equals(soughtCity);
    }
}
