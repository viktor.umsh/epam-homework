package hw12_tour.hotel.search;

import hw12_tour.base.search.BaseSearchCondition;
import hw12_tour.hotel.domain.Hotel;

public interface HotelSearchCondition extends BaseSearchCondition<Hotel> {
}
