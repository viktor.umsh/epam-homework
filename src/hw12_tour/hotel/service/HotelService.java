package hw12_tour.hotel.service;

import hw12_tour.base.service.BaseService;
import hw12_tour.hotel.domain.Hotel;

public abstract class HotelService extends BaseService<Hotel> {

}
