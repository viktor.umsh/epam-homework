package hw12_tour.hotel.service;

import hw12_tour.city.service.CityService;
import hw12_tour.hotel.domain.Hotel;
import hw12_tour.hotel.repo.HotelRepo;
import hw12_tour.order.service.OrderService;
import hw12_tour.storage.services.ServicesStorage;

public class HotelDefaultService extends HotelService {

    private CityService cityService;
    private OrderService orderService;

    public HotelDefaultService(HotelRepo hotelRepo) {
        repository = hotelRepo;
    }

    @Override
    public void add(Hotel hotel) {
        if (basicAdd(hotel)) {
            if (hotel.getCity() != null) {
                if (cityService.findByEntity(hotel.getCity()) == null) {
                    cityService.add(hotel.getCity());
                }
                if (!hotel.getCity().getHotels().contains(hotel)) {
                    hotel.getCity().addHotel(hotel);
                }
            }
        }
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
        orderService = ServicesStorage.getOrderService();
    }


    @Override
    public boolean delete(Hotel hotel) {
        boolean isHotelHaveOrders = (orderService.countByHotel(hotel) != 0);
        if (isHotelHaveOrders) {

        }
        hotel.getCity().deleteHotel(hotel);
//        if (hotel.getCity().getHotels().size() == 0) {
//            cityService.delete(hotel.getCity());
//        }
        return repository.delete(hotel);
    }
}
