package hw4_tour.Order.service;

import hw4_tour.Order.Order;
import hw4_tour.Order.repo.OrderMemoryRepo;

public class OrderMemoryService {
    private OrderMemoryRepo orderMemoryRepo = new OrderMemoryRepo();

    public void add(Order order) {
        if (order == null) {
            return;
        }
        if (orderMemoryRepo.findByEntity(order) != -1) {
            return;
        }
        orderMemoryRepo.add(order);
    }

    public Order get(int index) {
        return orderMemoryRepo.get(index);
    }

    public Order findById(long id) {
        return orderMemoryRepo.findById(id);
    }

    public Order findByName(String name) {
        return orderMemoryRepo.findByName(name);
    }

    public int findByEntity(Order order) {
        return orderMemoryRepo.findByEntity(order);
    }

    public boolean delete(Order order) {
        return orderMemoryRepo.delete(order);
    }

    public Order delete(int index) {
        return orderMemoryRepo.delete(index);
    }
}
