package hw4_tour.Order;

import hw4_tour.Hotel.Hotel;

public class Order {
    private long price;
    private Hotel hotel;
    private static long lastFreeId = 0;
    private final long id;

    public Order(long price, Hotel hotel) {
        this.price = price;
        this.hotel = hotel;
        this.id = Order.lastFreeId;
        Order.lastFreeId++;
    }

    public long getPrice() {
        return price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public long getId() {
        return id;
    }
}
