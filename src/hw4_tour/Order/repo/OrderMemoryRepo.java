package hw4_tour.Order.repo;

import hw4_tour.Order.Order;

import static hw4_tour.storage.Storage.orders;

public class OrderMemoryRepo {
    public void add(Order order) {
        orders.add(order);
    }

    public Order get(int index) {
        return orders.get(index);
    }

    public Order findById(long id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                return order;
            }
        }
        return null;
    }

    public Order findByName(String name) {
        return null;
    }

    public int findByEntity(Order order) {
        return orders.indexOf(order);
    }

    public boolean delete(Order order) {
        return orders.remove(order);
    }

    public Order delete(int index) {
        Order order = orders.get(index);
        delete(order);
        return order;
    }
}
