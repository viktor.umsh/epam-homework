package hw4_tour;

import hw4_tour.City.City;
import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Order.Order;
import hw4_tour.TourProvider.TourProvider;
import hw4_tour.User.User;

import java.util.List;

public class Controller {
    public static Order done(TourProvider tourProvider, User user) {
        List<Country> chosenCountries = user.chooseCountries(tourProvider.getCountries());
        List<City> chosenCities = user.chooseCities(tourProvider.getCities(chosenCountries));
        List<Hotel> chosenHotels = user.chooseHotels(tourProvider.getHotels(chosenCities));
        return user.chooseTour(tourProvider.generateOrder(user, chosenHotels));
    }
}
