package hw4_tour.Hotel;

import hw4_tour.City.City;

public class Hotel {
    private static long lastId = 0;
    private final long id;
    private final String name;
    private final int stars;
    private final long price;
    private final City city;

    public Hotel(String name, int stars, long price, City city) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.city = city;
        this.id = Hotel.lastId;
        Hotel.lastId++;
    }

    public String getName() {
        return name;
    }

    public int getStars() {
        return stars;
    }

    public long getPrice() {
        return price;
    }

    public City getCity() {
        return city;
    }

    public long getId() {
        return id;
    }
}
