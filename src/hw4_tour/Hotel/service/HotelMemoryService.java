package hw4_tour.Hotel.service;

import hw4_tour.Hotel.Hotel;
import hw4_tour.Hotel.repo.HotelMemoryRepo;

public class HotelMemoryService {
    private HotelMemoryRepo hotelMemoryRepo = new HotelMemoryRepo();

    public void addHotel(Hotel hotel) {
        hotel.getCity().addHotel(hotel);
        hotelMemoryRepo.addHotel(hotel);
    }

    public Hotel getHotel(int index) {
        return hotelMemoryRepo.getHotel(index);
    }

    public Hotel findHotelById(long id) {
        return hotelMemoryRepo.findHotelById(id);
    }

    public Hotel findHotelByName(String name) {
        return hotelMemoryRepo.findHotelByName(name);
    }

    public int findHotelByEntity(Hotel hotel) {
        return hotelMemoryRepo.findHotelByEntity(hotel);
    }

    public boolean deleteHotel(Hotel hotel) {
        hotel.getCity().deleteHotel(hotel);
        return hotelMemoryRepo.deleteHotel(hotel);
    }

    public Hotel deleteHotel(int index) {
        Hotel hotel = hotelMemoryRepo.getHotel(index);
        deleteHotel(hotel);
        return hotel;
    }
}
