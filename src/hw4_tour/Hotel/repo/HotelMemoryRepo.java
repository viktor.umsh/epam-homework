package hw4_tour.Hotel.repo;

import hw4_tour.Hotel.Hotel;

import static hw4_tour.storage.Storage.hotels;

public class HotelMemoryRepo {


    public void addHotel(Hotel hotel) {
        hotels.add(hotel);
    }

    public Hotel getHotel(int index) {
        return hotels.get(index);
    }

    public Hotel findHotelById(long id) {
        for (Hotel hotel : hotels) {
            if (hotel.getId() == id) {
                return hotel;
            }
        }
        return null;
    }

    public Hotel findHotelByName(String name) {
        for (Hotel hotel : hotels) {
            if (hotel.getName().equals(name)) {
                return hotel;
            }
        }
        return null;
    }

    public int findHotelByEntity(Hotel hotel) {
        return hotels.indexOf(hotel);
    }

    public boolean deleteHotel(Hotel hotel) {
        return hotels.remove(hotel);
    }

    public Hotel deleteHotel(int index) {
        return hotels.remove(index);
    }


}
