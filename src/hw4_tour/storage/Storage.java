package hw4_tour.storage;

import hw4_tour.City.City;
import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Order.Order;
import hw4_tour.TourProvider.TourProvider;
import hw4_tour.User.User;

import java.util.ArrayList;

public class Storage {
    public static ArrayList<Country> countries;
    public static ArrayList<City> cities;
    public static ArrayList<Hotel> hotels;
    public static ArrayList<TourProvider> tourProviders;
    public static ArrayList<Order> orders;
    public static ArrayList<User> users;

    static {
        countries = new ArrayList<Country>();
        cities = new ArrayList<City>();
        hotels = new ArrayList<Hotel>();
        tourProviders = new ArrayList<TourProvider>();
        orders = new ArrayList<Order>();
        users = new ArrayList<User>();
    }
}
