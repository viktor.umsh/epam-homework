package hw4_tour.Country;

import hw4_tour.City.City;

import java.util.ArrayList;
import java.util.Collection;

public class Country {
    private static long lastFreeId = 0;
    private final long id;
    private final String name;
    private ArrayList<City> cities;

    public Country(String name) {
        this.name = name;
        cities = new ArrayList<City>();
        this.id = Country.lastFreeId;
        Country.lastFreeId++;
    }

    public void addCity(City city) {
        cities.add(city);
    }

    public boolean deleteCity(City city) {
        return cities.remove(city);
    }

    public String getName() {
        return name;
    }

    public Collection<City> getCities() {
        return cities;
    }

    public long getId() {
        return id;
    }
}
