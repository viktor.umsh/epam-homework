package hw4_tour.Country.repo;

import hw4_tour.Country.Country;

import static hw4_tour.storage.Storage.countries;

public class CountryMemoryRepo {

    public void addCountry(Country country) {
        countries.add(country);
    }

    public Country getCountry(int index) {
        return countries.get(index);
    }

    public Country findCountryById(long id) {
        for (Country country : countries) {
            if (country.getId() == id) {
                return country;
            }
        }
        return null;
    }

    public Country findCountryByName(String name) {
        for (Country country : countries) {
            if (country.getName().equals(name)) {
                return country;
            }
        }
        return null;
    }

    public int findCountryByEntity(Country country) {
        return countries.indexOf(country);
    }

    public boolean deleteCountry(Country country) {
        return countries.remove(country);
    }

    public Country deleteCountry(int index) {
        return countries.remove(index);
    }

}
