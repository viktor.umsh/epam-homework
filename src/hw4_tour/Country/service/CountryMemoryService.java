package hw4_tour.Country.service;

import hw4_tour.City.City;
import hw4_tour.City.repo.CityMemoryRepo;
import hw4_tour.City.service.CityMemoryService;
import hw4_tour.Country.Country;
import hw4_tour.Country.repo.CountryMemoryRepo;


public class CountryMemoryService {

    private CountryMemoryRepo countryMemoryRepo = new CountryMemoryRepo();
    private CityMemoryRepo cityMemoryRepo = new CityMemoryRepo();
    private CityMemoryService cityMemoryService = new CityMemoryService();

    public void addCountry(Country country) {
        countryMemoryRepo.addCountry(country);
        for (City city : country.getCities()) {
            cityMemoryService.addCity(city);
        }
    }

    public Country getCountry(int index) {
        return countryMemoryRepo.getCountry(index);
    }

    public Country findCountryById(long id) {
        return countryMemoryRepo.findCountryById(id);
    }

    public Country findCountryByName(String name) {
        return countryMemoryRepo.findCountryByName(name);
    }

    public int findCountryByEntity(Country country) {
        return countryMemoryRepo.findCountryByEntity(country);
    }

    public boolean deleteCountry(Country country) {
        for (City city : country.getCities()) {
            cityMemoryService.deleteCity(city);
        }
        return countryMemoryRepo.deleteCountry(country);
    }

    public Country deleteCountry(int index) {
        Country country = countryMemoryRepo.getCountry(index);
        deleteCountry(country);
        return country;
    }
}
