package hw4_tour.User.service;

import hw4_tour.City.service.CityMemoryService;
import hw4_tour.User.User;
import hw4_tour.User.repo.UserMemoryRepo;


public class UserMemoryService {
    private UserMemoryRepo userMemoryRepo = new UserMemoryRepo();
    private CityMemoryService cityMemoryService = new CityMemoryService();

    public void add(User user) {
        if (user == null) {
            return;
        }
        if (userMemoryRepo.findByEntity(user) != -1) {
            return;
        }
        userMemoryRepo.add(user);
        if (cityMemoryService.findCityByEntity(user.getCity()) == -1) {
            cityMemoryService.addCity(user.getCity());
        }
    }

    public User get(int index) {
        return userMemoryRepo.get(index);
    }

    public User findById(long id) {
        return userMemoryRepo.findById(id);
    }

    public User findByName(String name) {
        return userMemoryRepo.findByName(name);
    }

    public int findByEntity(User user) {
        return userMemoryRepo.findByEntity(user);
    }

    public boolean delete(User user) {
        return userMemoryRepo.delete(user);
    }

    public User delete(int index) {
        return userMemoryRepo.delete(index);
    }
}
