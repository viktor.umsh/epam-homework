package hw4_tour.User;

import hw4_tour.City.City;
import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Order.Order;

import java.util.List;

public class RichMan extends User {

    public RichMan(String name, City city) {
        super(name, city);
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        return null;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        return null;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        return null;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return null;
    }
}
