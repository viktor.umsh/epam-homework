package hw4_tour.User.repo;


import hw4_tour.User.User;

import static hw4_tour.storage.Storage.users;

public class UserMemoryRepo {
    public void add(User user) {
        users.add(user);
    }

    public User get(int index) {
        return users.get(index);
    }

    public User findById(long id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public User findByName(String name) {
        for (User user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    public int findByEntity(User user) {
        return users.indexOf(user);
    }

    public boolean delete(User user) {
        return users.remove(user);
    }

    public User delete(int index) {
        return users.remove(index);
    }
}
