package hw4_tour.User;

import hw4_tour.City.City;
import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Order.Order;

import java.util.List;

abstract public class User {
    private String name;
    private City city;
    private static long lastFreeId = 0;
    private final long id;

    public User(String name, City city) {
        this.name = name;
        this.city = city;
        this.id = User.lastFreeId;
        User.lastFreeId++;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    abstract public List<Country> chooseCountries(List<Country> countries);

    abstract public List<City> chooseCities(List<City> cities);

    abstract public List<Hotel> chooseHotels(List<Hotel> hotels);

    abstract public Order chooseTour(List<Order> orders);

    public long getId() {
        return id;
    }
}
