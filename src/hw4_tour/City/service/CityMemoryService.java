package hw4_tour.City.service;

import hw4_tour.City.City;
import hw4_tour.City.repo.CityMemoryRepo;
import hw4_tour.Country.repo.CountryMemoryRepo;
import hw4_tour.Country.service.CountryMemoryService;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Hotel.repo.HotelMemoryRepo;

public class CityMemoryService {
    CityMemoryRepo cityMemoryRepo = new CityMemoryRepo();
    CountryMemoryRepo countryMemoryRepo = new CountryMemoryRepo();
    CountryMemoryService countryMemoryService = new CountryMemoryService();
    HotelMemoryRepo hotelMemoryRepo = new HotelMemoryRepo();

    public void addCity(City city) {
//        city.getCity().addHotel(city);
        if (countryMemoryRepo.findCountryByEntity(city.getCountry()) == -1) {
            countryMemoryService.addCountry(city.getCountry());
        } else {
            cityMemoryRepo.addCity(city);
            for (Hotel hotel : city.getHotels()) {
                hotelMemoryRepo.addHotel(hotel);
            }
        }
    }

    public City getCity(int index) {
        return cityMemoryRepo.getCity(index);
    }

    public City findCityById(long id) {
        return cityMemoryRepo.findCityById(id);
    }

    public City findCityByName(String name) {
        return cityMemoryRepo.findCityByName(name);
    }

    public int findCityByEntity(City city) {
        return cityMemoryRepo.findCityByEntity(city);
    }

    public boolean deleteCity(City city) {
        for (Hotel hotel : city.getHotels()) {
            hotelMemoryRepo.deleteHotel(hotel);
        }
        city.getCountry().deleteCity(city);
        return cityMemoryRepo.deleteCity(city);
    }

    public City deleteCity(int index) {
        City city = cityMemoryRepo.getCity(index);
        deleteCity(city);
        return city;
    }
}
