package hw4_tour.City;

import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;

import java.util.ArrayList;
import java.util.Collection;

public class City {
    private static long lastFreeId = 0;
    private final long id;
    private final String name;
    private final Coordinate coordinate;
    private final Country country;
    private ArrayList<Hotel> hotels;

    public City(String name, Coordinate coordinate, Country country) {
        this.name = name;
        this.coordinate = coordinate;
        this.country = country;
        hotels = new ArrayList<Hotel>();
        this.id = City.lastFreeId;
        City.lastFreeId++;
    }

    public void addHotel(Hotel hotel) {
        this.hotels.add(hotel);
    }

    public boolean deleteHotel(Hotel hotel) {
        return this.hotels.remove(hotel);
    }

    public Collection<Hotel> getHotels() {
        return hotels;
    }

    public String getName() {
        return name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Country getCountry() {
        return country;
    }

    public long getId() {
        return id;
    }
}
