package hw4_tour.City.repo;

import hw4_tour.City.City;

import static hw4_tour.storage.Storage.cities;

public class CityMemoryRepo {

    public void addCity(City city) {
        cities.add(city);
    }

    public City getCity(int index) {
        return cities.get(index);
    }

    public City findCityById(long id) {
        for (City city : cities) {
            if (city.getId() == id) {
                return city;
            }
        }
        return null;
    }

    public City findCityByName(String name) {
        for (City city : cities) {
            if (city.getName().equals(name)) {
                return city;
            }
        }
        return null;
    }

    public int findCityByEntity(City city) {
        return cities.indexOf(city);
    }

    public boolean deleteCity(City city) {
        return cities.remove(city);
    }

    public City deleteCity(int index) {
        return cities.remove(index);
    }

}
