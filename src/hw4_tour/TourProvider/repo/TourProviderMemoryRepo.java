package hw4_tour.TourProvider.repo;

import hw4_tour.TourProvider.TourProvider;

import static hw4_tour.storage.Storage.tourProviders;

public class TourProviderMemoryRepo {
    public void add(TourProvider tourProvider) {
        tourProviders.add(tourProvider);
    }

    public TourProvider get(int index) {
        return tourProviders.get(index);
    }

    public TourProvider findById(long id) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getId() == id) {
                return tourProvider;
            }
        }
        return null;
    }

    public TourProvider findByName(String name) {
        for (TourProvider tourProvider : tourProviders) {
            if (tourProvider.getName().equals(name)) {
                return tourProvider;
            }
        }
        return null;
    }

    public int findByEntity(TourProvider tourProvider) {
        return tourProviders.indexOf(tourProvider);
    }

    public boolean delete(TourProvider tourProvider) {
        return tourProviders.remove(tourProvider);
    }

    public TourProvider delete(int index) {
        return tourProviders.remove(index);
    }
}
