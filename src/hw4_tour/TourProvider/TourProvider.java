package hw4_tour.TourProvider;

import hw4_tour.City.City;
import hw4_tour.Country.Country;
import hw4_tour.Hotel.Hotel;
import hw4_tour.Order.Order;
import hw4_tour.User.User;

import java.util.ArrayList;
import java.util.List;

public class TourProvider {
    private String name;
    private final long DISTANCE_COST;
    private final long SERVICE_COST;
    private static long lastFreeId = 0;
    private final long id;

    private ArrayList<Country> countries;

    public TourProvider(String name, long DISTANCE_COST, long SERVICE_COST) {
        this.name = name;
        this.DISTANCE_COST = DISTANCE_COST;
        this.SERVICE_COST = SERVICE_COST;
        countries = new ArrayList<Country>();
        this.id = TourProvider.lastFreeId;
        TourProvider.lastFreeId++;
    }

    public List<City> getCities(List<Country> requiredCountries) {
        ArrayList<City> requiredCities = new ArrayList<City>();
        for (Country country : requiredCountries) {
            requiredCities.addAll(country.getCities());
        }
        return requiredCities;
    }

    public List<Hotel> getHotels(List<City> requiredCity) {
        ArrayList<Hotel> requiredHotels = new ArrayList<Hotel>();
        for (City city : requiredCity) {
            requiredHotels.addAll(city.getHotels());
        }
        return requiredHotels;
    }

    public List<Order> generateOrder(User user, List<Hotel> hotels) {
        ArrayList<Order> orders = new ArrayList<Order>();
        for (Hotel hotel : hotels) {
            orders.add(new Order(calcOrder(user, hotel), hotel));
        }
        return orders;
    }

    private long calcOrder(User user, Hotel hotel) {
        return SERVICE_COST + hotel.getPrice() +
                DISTANCE_COST * user.getCity().getCoordinate().lengthTo(hotel.getCity().getCoordinate());
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
