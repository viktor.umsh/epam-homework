package hw4_tour.TourProvider.service;

import hw4_tour.Country.Country;
import hw4_tour.Country.service.CountryMemoryService;
import hw4_tour.TourProvider.TourProvider;
import hw4_tour.TourProvider.repo.TourProviderMemoryRepo;

public class TourProviderService {
    private TourProviderMemoryRepo tourProviderMemoryRepo = new TourProviderMemoryRepo();
    private CountryMemoryService countryMemoryService = new CountryMemoryService();

    public void add(TourProvider tourProvider) {
        if (tourProvider == null) {
            return;
        }
        if (tourProviderMemoryRepo.findByEntity(tourProvider) != -1) {
            return;
        }
        tourProviderMemoryRepo.add(tourProvider);
        for (Country country : tourProvider.getCountries()) {
            countryMemoryService.addCountry(country);
        }
    }

    public TourProvider get(int index) {
        return tourProviderMemoryRepo.get(index);
    }

    public TourProvider findById(long id) {
        return tourProviderMemoryRepo.findById(id);
    }

    public TourProvider findByName(String name) {
        return tourProviderMemoryRepo.findByName(name);
    }

    public int findByEntity(TourProvider tourProvider) {
        return tourProviderMemoryRepo.findByEntity(tourProvider);
    }

    public boolean delete(TourProvider tourProvider) {
        for (Country country : tourProvider.getCountries()) {
            countryMemoryService.deleteCountry(country);
        }
        return tourProviderMemoryRepo.delete(tourProvider);
    }

    public TourProvider delete(int index) {
        TourProvider tourProvider = tourProviderMemoryRepo.get(index);
        delete(tourProvider);
        return tourProvider;
    }
}
