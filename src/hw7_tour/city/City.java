package hw7_tour.city;

import hw7_tour.base.IdentifiableAndNamed;
import hw7_tour.country.Country;
import hw7_tour.hotel.Hotel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class City extends IdentifiableAndNamed { // implements Statistical
    private static long lastFreeId = 0;
    private final String name;
    private final Coordinate coordinate;
    private final Country country;
    private ArrayList<Hotel> hotels;
    private Climate climate;
//    private Statistic statistic;

    public City(String name, Coordinate coordinate, Country country, Climate climate) {
        this.name = name;
        this.coordinate = coordinate;
        this.country = country;
        this.climate = climate;
        this.hotels = new ArrayList<Hotel>();
        createId();
//        this.statistic = new Statistic();
    }


    @Override
    protected void createId() {
        this.id = City.lastFreeId;
        City.lastFreeId++;
    }

    public void addHotel(Hotel hotel) {
        this.hotels.add(hotel);
    }

    public boolean deleteHotel(Hotel hotel) {
        return this.hotels.remove(hotel);
    }

    public Collection<Hotel> getHotels() {
        return hotels;
    }

    public String getName() {
        return name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Country getCountry() {
        return country;
    }

    public Climate getClimate() {
        return climate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name) &&
                Objects.equals(coordinate, city.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coordinate);
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", coordinate=" + coordinate +
                ", country=" + country +
                ", climate=" + climate +
                ", id=" + id +
                '}';
    }
//    @Override
//    public Statistic getStatistic() {
//        return statistic;
//    }
//
//    @Override
//    public void updateStatistic(long newValue) {
//        statistic.updateStatistic(newValue);
//    }
}
