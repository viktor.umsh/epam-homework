package hw7_tour.city.service;

import hw7_tour.base.BaseService;
import hw7_tour.city.City;

public abstract class CityService extends BaseService<City> {
}
