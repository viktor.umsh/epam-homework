package hw7_tour.city.service;

import hw7_tour.city.City;
import hw7_tour.city.repo.CityRepo;
import hw7_tour.country.service.CountryService;
import hw7_tour.hotel.Hotel;
import hw7_tour.hotel.service.HotelService;
import hw7_tour.storage.services.ServicesStorage;

public class CityDefaultService extends CityService {
    private CountryService countryService;
    private HotelService hotelListService;

    public CityDefaultService(CityRepo cityRepo) {
        repository = cityRepo;
    }

    @Override
    public void initInsertedServices() {
        countryService = ServicesStorage.getCountryService();
        hotelListService = ServicesStorage.getHotelService();
    }

    @Override
    public void add(City city) {
        if (basicAdd(city)) {
            if (!city.getCountry().getCities().contains(city)) {
                city.getCountry().addCity(city);
            }
            if (countryService.findByEntity(city.getCountry()) == -1) {
                countryService.add(city.getCountry());
            }
            for (Hotel hotel : city.getHotels()) {
                hotelListService.add(hotel);
            }
        }
    }

    @Override
    public boolean delete(City city) {
        for (Hotel hotel : city.getHotels()) {
            hotelListService.delete(hotel);
        }
        city.getCountry().deleteCity(city);
        return repository.delete(city);
    }
}
