package hw7_tour.city.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.city.City;

public interface CityRepo extends BaseRepo<City> {
}
