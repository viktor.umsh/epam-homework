package hw7_tour.city.repo.impl;

import hw7_tour.base.ArrayRepo;
import hw7_tour.city.City;
import hw7_tour.city.repo.CityRepo;

import static hw7_tour.storage.data.ArrayDataStore.cities;

public class CityArrayRepo extends ArrayRepo<City> implements CityRepo {
    public CityArrayRepo() {
        dataArray = cities;
    }
}
