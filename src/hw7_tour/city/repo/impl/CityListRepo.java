package hw7_tour.city.repo.impl;

import hw7_tour.city.City;
import hw7_tour.city.repo.CityRepo;

import static hw7_tour.storage.data.ListDataStorage.cities;

public class CityListRepo implements CityRepo {

    @Override
    public void add(City city) {
        cities.add(city);
    }

    @Override
    public City get(int index) {
        return cities.get(index);
    }

    @Override
    public City findById(long id) {
        for (City city : cities) {
            if (city.getId() == id) {
                return city;
            }
        }
        return null;
    }

    @Override
    public City findByName(String name) {
        for (City city : cities) {
            if (city.getName().equals(name)) {
                return city;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(City city) {
        return cities.indexOf(city);
    }

    @Override
    public boolean delete(City city) {
        return cities.remove(city);
    }

    @Override
    public City delete(int index) {
        return cities.remove(index);
    }
}
