package hw7_tour;

import hw7_tour.city.City;
import hw7_tour.country.Country;
import hw7_tour.dataReader.DataGenerator;
import hw7_tour.hotel.Hotel;
import hw7_tour.order.Order;
import hw7_tour.storage.StorageType;
import hw7_tour.storage.services.ServicesStorage;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.tourProvider.service.TourProviderService;
import hw7_tour.user.AbstractUser;
import hw7_tour.user.service.UserService;

import java.util.List;

public class Application {
    private UserService userService;
    private TourProviderService tourProviderService;
//    private CountryService countryService;
//    private CityService cityService;
//    private HotelService hotelService;
//    private OrderService orderService;

    public Order UserTourProviderInteraction(TourProvider tourProvider, AbstractUser user) {
        List<Country> chosenCountries = user.chooseCountries(tourProvider.getCountries());
        List<City> chosenCities = user.chooseCities(tourProvider.getCities(chosenCountries));
        List<Hotel> chosenHotels = user.chooseHotels(tourProvider.getHotels(chosenCities));
        return user.chooseTour(tourProvider.generateOrder(user, chosenHotels));
    }


    public Application(StorageType storageType) {
        ServicesStorage.setStorageType(storageType);
        initServices();
        DataGenerator generator = new DataGenerator();
        generator.readFile();
    }

    private void initServices() {
        userService = ServicesStorage.getUserService();
        tourProviderService = ServicesStorage.getTourProviderService();
//        countryService = ServicesStorage.getCountryService();
//        cityService = ServicesStorage.getCityService();
//        hotelService = ServicesStorage.getHotelService();
//        orderService = ServicesStorage.getOrderService();
    }

    public void runApplication() {
        AbstractUser vasiliy = userService.findByName("Vasiliy");
        TourProvider grandTours = tourProviderService.findByName("GrandTours");
        Order order = UserTourProviderInteraction(grandTours, vasiliy);
        System.out.println("System create order:" + order);
    }

    public static void main(String[] args) {
        System.out.println("--------------------LIST:------------------------");
        Application application = new Application(StorageType.LIST);
        application.runApplication();
        System.out.println();

        System.out.println("-------------------ARRAY:------------------------");
        application = new Application(StorageType.ARRAY);
        application.runApplication();
    }
}
