package hw7_tour.base;

public interface BaseRepo<Type> { // extends Iterable<Type>

    public void add(Type something);

    public Type get(int index);

    public Type findById(long id);

    public Type findByName(String name);

    public int findByEntity(Type something);

    public boolean delete(Type something);

    public Type delete(int index);

}
