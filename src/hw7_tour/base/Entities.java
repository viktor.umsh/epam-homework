package hw7_tour.base;

public enum Entities {
    USER,
    COUNTRY,
    CITY,
    HOTEL,
    TOUR_PROVIDER,
    ORDER
}
