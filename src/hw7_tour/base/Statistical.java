package hw7_tour.base;

import hw7_tour.statistic.Statistic;

public abstract class Statistical {
    Statistic statistic = new Statistic();

    public Statistic getStatistic() {
        return statistic;
    }

    public void updateStatistic(long newValue) {
        statistic.updateStatistic(newValue);
    }
}

