package hw7_tour.base;

public abstract class BaseService<Type> {
    protected BaseRepo<Type> repository;

    protected boolean basicAdd(Type something) {
        if (something == null) {
            return false;
        }
        if (repository.findByEntity(something) != -1) {
            return true;
        } else {
            repository.add(something);
            return true;
        }
    }

    public abstract void initInsertedServices();

    public void add(Type something) {
        repository.add(something);
    }

    ;

    public Type get(int index) {
        return repository.get(index);
    }

    public Type findById(long id) {
        return repository.findById(id);
    }

    public Type findByName(String name) {
        return repository.findByName(name);
    }

    /**
     * @param something
     * @return index of something in storage or -1 if not exist
     */
    public int findByEntity(Type something) {
        return repository.findByEntity(something);
    }

    public boolean delete(Type something) {
        return repository.delete(something);
    }

    public Type delete(int index) {
        Type deleted = repository.get(index);
        delete(deleted);
        return deleted;
    }

//    public void printAll() {
//        for (Type elem : repository) { ... }
//    }
}
