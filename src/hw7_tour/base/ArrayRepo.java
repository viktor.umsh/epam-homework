package hw7_tour.base;

import hw7_tour.utils.Array;

public abstract class ArrayRepo<Type extends IdentifiableAndNamed> {

    protected Array<Type> dataArray;

    public void add(Type country) {
        dataArray.add(country);
    }

    public Type get(int index) {
        return dataArray.get(index);
    }

    public Type findById(long id) {
        for (Type country : dataArray) {
            if (country.getId() == id) {
                return country;
            }
        }
        return null;
    }

    public Type findByName(String name) {
        for (Type country : dataArray) {
            if (country.getName().equals(name)) {
                return country;
            }
        }
        return null;
    }

    public int findByEntity(Type country) {
        for (int i = 0; i < dataArray.size(); i++) {
            if (dataArray.get(i).equals(country)) {
                return i;
            }
        }
        return -1;
    }

    public boolean delete(Type city) {
        int index = findByEntity(city);
        if (index != -1) {
            delete(index);
            return true;
        }
        return false;
    }

    public Type delete(int index) {
        return dataArray.remove(index);
    }

}
