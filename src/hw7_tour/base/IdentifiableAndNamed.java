package hw7_tour.base;

public abstract class IdentifiableAndNamed {
    protected long id;
    protected String name;

    protected abstract void createId();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
