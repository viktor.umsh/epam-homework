package hw7_tour.tourProvider.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.tourProvider.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
