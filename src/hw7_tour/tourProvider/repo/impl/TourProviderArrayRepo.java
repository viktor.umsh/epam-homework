package hw7_tour.tourProvider.repo.impl;

import hw7_tour.base.ArrayRepo;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.tourProvider.repo.TourProviderRepo;

import static hw7_tour.storage.data.ArrayDataStore.tourProviders;


public class TourProviderArrayRepo extends ArrayRepo<TourProvider> implements TourProviderRepo {
    public TourProviderArrayRepo() {
        dataArray = tourProviders;
    }
}
