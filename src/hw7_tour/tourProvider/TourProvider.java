package hw7_tour.tourProvider;

import hw7_tour.base.IdentifiableAndNamed;
import hw7_tour.city.City;
import hw7_tour.country.Country;
import hw7_tour.hotel.Hotel;
import hw7_tour.order.Order;
import hw7_tour.user.AbstractUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TourProvider extends IdentifiableAndNamed {
    private static long lastId = 0;
    private final long DISTANCE_COST;
    private final long SERVICE_COST;
    private String name;

    private ArrayList<Country> countries;

    public TourProvider(String name, long DISTANCE_COST, long SERVICE_COST) {
        this.name = name;
        this.DISTANCE_COST = DISTANCE_COST;
        this.SERVICE_COST = SERVICE_COST;
        countries = new ArrayList<Country>();
        createId();
    }

    @Override
    protected void createId() {
        this.id = TourProvider.lastId;
        TourProvider.lastId++;
    }

    public List<City> getCities(List<Country> requiredCountries) {
        ArrayList<City> requiredCities = new ArrayList<City>();
        for (Country country : requiredCountries) {
            requiredCities.addAll(country.getCities());
        }
        return requiredCities;
    }

    public List<Hotel> getHotels(List<City> requiredCity) {
        ArrayList<Hotel> requiredHotels = new ArrayList<Hotel>();
        for (City city : requiredCity) {
            requiredHotels.addAll(city.getHotels());
        }
        return requiredHotels;
    }

    public List<Order> generateOrder(AbstractUser user, List<Hotel> hotels) {
        ArrayList<Order> orders = new ArrayList<Order>();
        for (Hotel hotel : hotels) {
            orders.add(new Order(calcOrder(user, hotel), hotel));
        }
        return orders;
    }

    private long calcOrder(AbstractUser user, Hotel hotel) {
        return SERVICE_COST + hotel.getPrice() +
                DISTANCE_COST * user.getCity().getCoordinate().lengthTo(hotel.getCity().getCoordinate());
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TourProvider that = (TourProvider) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TourProvider{" +
                "DISTANCE_COST=" + DISTANCE_COST +
                ", SERVICE_COST=" + SERVICE_COST +
                ", name='" + name + '\'' +
                ", countries=" + countries +
                ", id=" + id +
                '}';
    }
}
