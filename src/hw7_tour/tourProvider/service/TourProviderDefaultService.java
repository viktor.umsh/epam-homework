package hw7_tour.tourProvider.service;

import hw7_tour.country.Country;
import hw7_tour.country.service.CountryService;
import hw7_tour.storage.services.ServicesStorage;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.tourProvider.repo.TourProviderRepo;


public class TourProviderDefaultService extends TourProviderService {
    private CountryService countryService;

    public TourProviderDefaultService(TourProviderRepo tourProviderRepo) {
        repository = tourProviderRepo;
    }

    @Override
    public void add(TourProvider tourProvider) {
        if (basicAdd(tourProvider)) {
            for (Country country : tourProvider.getCountries()) {
                countryService.add(country);
            }
        }
    }

    @Override
    public void initInsertedServices() {
        countryService = ServicesStorage.getCountryService();
    }

    @Override
    public boolean delete(TourProvider tourProvider) {
        for (Country country : tourProvider.getCountries()) {
            countryService.delete(country);
        }
        return tourProviderRepo.delete(tourProvider);
    }
}
