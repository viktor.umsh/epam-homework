package hw7_tour.dataReader.Parsers;

import hw7_tour.city.City;
import hw7_tour.city.service.CityService;
import hw7_tour.storage.services.ServicesStorage;
import hw7_tour.user.RandomMan;
import hw7_tour.user.service.UserService;

public class UserParser extends BasicParser {
    private CityService cityMemoryService = ServicesStorage.getCityService();
    private UserService userMemoryService = ServicesStorage.getUserService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 2) {
            City city = cityMemoryService.findByName(args[1]);
            userMemoryService.add(new RandomMan(args[0], city));
        }
    }
}
