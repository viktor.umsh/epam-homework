package hw7_tour.dataReader.Parsers;

import hw7_tour.city.City;
import hw7_tour.city.service.CityService;
import hw7_tour.hotel.Hotel;
import hw7_tour.hotel.service.HotelService;
import hw7_tour.storage.services.ServicesStorage;

public class HotelParser extends BasicParser {
    private HotelService hotelMemoryService = ServicesStorage.getHotelService();
    private CityService cityMemoryService = ServicesStorage.getCityService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length != 4) {
            return;
        }
        try {
            City city = cityMemoryService.findByName(args[3]);
            if (city == null) {
                return; // TODO: подумать что делать..
            }
            hotelMemoryService.add(new Hotel(args[0], Integer.parseInt(args[1]), Long.parseLong(args[2]), city));
        } catch (NumberFormatException e) {
        }
    }
}
