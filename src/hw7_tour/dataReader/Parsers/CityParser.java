package hw7_tour.dataReader.Parsers;

import hw7_tour.city.City;
import hw7_tour.city.Climate;
import hw7_tour.city.Coordinate;
import hw7_tour.city.service.CityService;
import hw7_tour.country.Country;
import hw7_tour.country.service.CountryService;
import hw7_tour.storage.services.ServicesStorage;

public class CityParser extends BasicParser {
    private CountryService countryMemoryService = ServicesStorage.getCountryService();
    private CityService cityMemoryService = ServicesStorage.getCityService();


    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length != 5) {
            return;
        }
        try {
            Coordinate coordinate = new Coordinate(Long.parseLong(args[1]), Long.parseLong(args[2]));
            Climate climate = Climate.valueOf(args[4]);
            Country country = countryMemoryService.findByName(args[3]);
            if (coordinate == null) {
                country = new Country(args[3]);
                countryMemoryService.add(country);
            }
            cityMemoryService.add(new City(args[0], coordinate, country, climate));
        } catch (NumberFormatException e) {
        }
    }
}
