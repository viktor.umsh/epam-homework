package hw7_tour.dataReader.Parsers;

import hw7_tour.country.Country;
import hw7_tour.country.service.CountryService;
import hw7_tour.storage.services.ServicesStorage;

public class CountryParser extends BasicParser {

    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 1) {
            countryMemoryService.add(new Country(args[0]));
        }
    }

}
