package hw7_tour.dataReader.Parsers;

import hw7_tour.country.Country;
import hw7_tour.country.service.CountryService;
import hw7_tour.storage.services.ServicesStorage;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.tourProvider.service.TourProviderService;

public class TourProviderParser extends BasicParser {

    private TourProviderService tourProviderMemoryService = ServicesStorage.getTourProviderService();
    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length <= 2) {
            return;
        }
        try {
            TourProvider tourProvider = new TourProvider(args[0], Long.parseLong(args[1]), Long.parseLong(args[2]));
            for (int i = 3; i < args.length; i++) {
                Country country = countryMemoryService.findByName(args[i]);
                if (country == null) {
                    country = new Country(args[i]);
                }
                tourProvider.addCountry(country);
            }
            tourProviderMemoryService.add(tourProvider);
        } catch (NumberFormatException e) {
        }
    }
}
