package hw7_tour.dataReader;

import hw7_tour.dataReader.Parsers.*;

public class Parser {
    private CountryParser countryParser = new CountryParser();
    private CityParser cityParser = new CityParser();
    private HotelParser hotelParser = new HotelParser();
    private UserParser userParser = new UserParser();
    private TourProviderParser tourProviderParser = new TourProviderParser();
    private OrderParser orderParser = new OrderParser();

    public void parse(String whatRead, String line) {
        switch (whatRead) {
            case "Users:":
                userParser.parseString(line);
                break;
            case "Countries:":
                countryParser.parseString(line);
                break;
            case "Cities:":
                cityParser.parseString(line);
                break;
            case "Hotels:":
                hotelParser.parseString(line);
                break;
            case "TourProviders:":
                tourProviderParser.parseString(line);
                break;
            case "Orders:":
                orderParser.parseString(line);
                break;
        }
    }
}
