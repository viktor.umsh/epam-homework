package hw7_tour.hotel.repo.impl;

import hw7_tour.hotel.Hotel;
import hw7_tour.hotel.repo.HotelRepo;

import static hw7_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo implements HotelRepo {

    @Override
    public void add(Hotel hotel) {
        hotels.add(hotel);
    }

    @Override
    public Hotel get(int index) {
        return hotels.get(index);
    }

    @Override
    public Hotel findById(long id) {
        for (Hotel hotel : hotels) {
            if (hotel.getId() == id) {
                return hotel;
            }
        }
        return null;
    }

    @Override
    public Hotel findByName(String name) {
        for (Hotel hotel : hotels) {
            if (hotel.getName().equals(name)) {
                return hotel;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(Hotel hotel) {
        return hotels.indexOf(hotel);
    }

    @Override
    public boolean delete(Hotel hotel) {
        return hotels.remove(hotel);
    }

    @Override
    public Hotel delete(int index) {
        return hotels.remove(index);
    }
}
