package hw7_tour.hotel.repo.impl;

import hw7_tour.base.ArrayRepo;
import hw7_tour.hotel.Hotel;
import hw7_tour.hotel.repo.HotelRepo;

import static hw7_tour.storage.data.ArrayDataStore.hotels;

public class HotelArrayRepo extends ArrayRepo<Hotel> implements HotelRepo {
    public HotelArrayRepo() {
        dataArray = hotels;
    }
}
