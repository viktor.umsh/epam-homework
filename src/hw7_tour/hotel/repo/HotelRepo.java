package hw7_tour.hotel.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.hotel.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
