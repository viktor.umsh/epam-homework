package hw7_tour.hotel.service;

import hw7_tour.hotel.Hotel;
import hw7_tour.hotel.repo.HotelRepo;

public class HotelDefaultService extends HotelService {

    public HotelDefaultService(HotelRepo hotelRepo) {
        repository = hotelRepo;
    }

    @Override
    public void add(Hotel hotel) {
        if (basicAdd(hotel)) {
            hotel.getCity().addHotel(hotel);
        }
    }

    @Override
    public void initInsertedServices() {
    }


    @Override
    public boolean delete(Hotel hotel) {
        hotel.getCity().deleteHotel(hotel);
        return repository.delete(hotel);
    }
}
