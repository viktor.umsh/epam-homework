package hw7_tour.statistic;

public class Statistic {
    private long id;
    private static long lastId;
    private long minimumOffer;
    private long maximumOffer;
    private long mediumOffer;
    private long numberIfIffers;

    public Statistic() {
        this.minimumOffer = Long.MAX_VALUE;
        this.maximumOffer = Long.MIN_VALUE;
        this.numberIfIffers = 0;
        this.mediumOffer = 0;
        this.id = Statistic.lastId;
        Statistic.lastId++;
    }

    public void updateStatistic(long newValue) {
        if (newValue > this.maximumOffer) {
            this.maximumOffer = newValue;
        }
        if (newValue < this.minimumOffer) {
            this.minimumOffer = newValue;
        }
        this.mediumOffer = (this.mediumOffer * this.numberIfIffers + newValue) / (this.minimumOffer + 1);
        this.numberIfIffers++;
    }

    public long getId() {
        return id;
    }
}
