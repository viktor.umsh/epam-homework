package hw7_tour.country.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.country.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
