package hw7_tour.country.repo.impl;

import hw7_tour.country.Country;
import hw7_tour.country.repo.CountryRepo;

import static hw7_tour.storage.data.ListDataStorage.countries;

public class CountryListRepo implements CountryRepo {

    @Override
    public void add(Country country) {
        countries.add(country);
    }

    @Override
    public Country get(int index) {
        return countries.get(index);
    }

    @Override
    public Country findById(long id) {
        for (Country country : countries) {
            if (country.getId() == id) {
                return country;
            }
        }
        return null;
    }

    @Override
    public Country findByName(String name) {
        for (Country country : countries) {
            if (country.getName().equals(name)) {
                return country;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(Country country) {
        return countries.indexOf(country);
    }

    @Override
    public boolean delete(Country country) {
        return countries.remove(country);
    }

    @Override
    public Country delete(int index) {
        return countries.remove(index);
    }
}
