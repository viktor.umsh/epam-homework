package hw7_tour.country.repo.impl;

import hw7_tour.base.ArrayRepo;
import hw7_tour.country.Country;
import hw7_tour.country.repo.CountryRepo;

import static hw7_tour.storage.data.ArrayDataStore.countries;

public class CountryArrayRepo extends ArrayRepo<Country> implements CountryRepo {
    public CountryArrayRepo() {
        dataArray = countries;
    }
}
