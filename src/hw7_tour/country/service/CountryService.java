package hw7_tour.country.service;

import hw7_tour.base.BaseService;
import hw7_tour.country.Country;

public abstract class CountryService extends BaseService<Country> {
}
