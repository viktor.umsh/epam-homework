package hw7_tour.country.service;

import hw7_tour.city.City;
import hw7_tour.city.service.CityService;
import hw7_tour.country.Country;
import hw7_tour.country.repo.CountryRepo;
import hw7_tour.storage.services.ServicesStorage;

public class CountryDefaultService extends CountryService {

    private CityService cityService;

    public CountryDefaultService(CountryRepo countryRepo) {
        repository = countryRepo;
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
    }

    @Override
    public void add(Country country) {
        if (basicAdd(country)) {
            for (City city : country.getCities()) {
                if (cityService.findByEntity(city) == -1) {
                    cityService.add(city);
                }
            }
        }
    }

    @Override
    public boolean delete(Country country) {
        for (City city : country.getCities()) {
            cityService.delete(city);
        }
        return repository.delete(country);
    }

}
