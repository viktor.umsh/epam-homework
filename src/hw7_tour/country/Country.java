package hw7_tour.country;

import hw7_tour.base.IdentifiableAndNamed;
import hw7_tour.city.City;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Country extends IdentifiableAndNamed {
    private static long lastFreeId = 0;
    private final String name;
    private ArrayList<City> cities;

    public Country(String name) {
        this.name = name;
        cities = new ArrayList<City>();
        createId();
    }

    @Override
    protected void createId() {
        this.id = Country.lastFreeId;
        Country.lastFreeId++;
    }

    public void addCity(City city) {
        cities.add(city);
    }

    public boolean deleteCity(City city) {
        return cities.remove(city);
    }

    public String getName() {
        return name;
    }

    public Collection<City> getCities() {
        return cities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
