package hw7_tour.user.service;

import hw7_tour.city.service.CityService;
import hw7_tour.storage.services.ServicesStorage;
import hw7_tour.user.AbstractUser;
import hw7_tour.user.repo.UserRepo;

public class UserDefaultService extends UserService {
    private CityService cityService;

    public UserDefaultService(UserRepo userRepo) {
        repository = userRepo;
    }

    @Override
    public void add(AbstractUser user) {
        if (basicAdd(user)) {
            if (cityService.findByEntity(user.getCity()) == -1) {
                cityService.add(user.getCity());
            }
        }
    }

    @Override
    public void initInsertedServices() {
        cityService = ServicesStorage.getCityService();
    }

}
