package hw7_tour.user;

import hw7_tour.base.IdentifiableAndNamed;
import hw7_tour.city.City;
import hw7_tour.country.Country;
import hw7_tour.hotel.Hotel;
import hw7_tour.order.Order;

import java.util.List;
import java.util.Objects;

abstract public class AbstractUser extends IdentifiableAndNamed {
    private static long lastId = 0;
    private String name;
    private City city;

    public AbstractUser(String name, City city) {
        this.name = name;
        this.city = city;
        createId();
    }

    @Override
    protected void createId() {
        this.id = AbstractUser.lastId;
        AbstractUser.lastId++;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    abstract public List<Country> chooseCountries(List<Country> countries);

    abstract public List<City> chooseCities(List<City> cities);

    abstract public List<Hotel> chooseHotels(List<Hotel> hotels);

    abstract public Order chooseTour(List<Order> orders);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractUser that = (AbstractUser) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }

    @Override
    public String toString() {
        return "AbstractUser{" +
                "name='" + name + '\'' +
                ", city=" + city +
                ", id=" + id +
                '}';
    }
}
