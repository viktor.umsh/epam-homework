package hw7_tour.user.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.user.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
