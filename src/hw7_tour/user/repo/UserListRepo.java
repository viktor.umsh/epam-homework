package hw7_tour.user.repo;

import hw7_tour.user.AbstractUser;

import static hw7_tour.storage.data.ListDataStorage.users;

public class UserListRepo implements UserRepo {
    @Override
    public void add(AbstractUser user) {
        users.add(user);
    }

    @Override
    public AbstractUser get(int index) {
        return users.get(index);
    }

    @Override
    public AbstractUser findById(long id) {
        for (AbstractUser user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public AbstractUser findByName(String name) {
        for (AbstractUser user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public int findByEntity(AbstractUser user) {
        return users.indexOf(user);
    }

    @Override
    public boolean delete(AbstractUser user) {
        return users.remove(user);
    }

    @Override
    public AbstractUser delete(int index) {
        return users.remove(index);
    }
}
