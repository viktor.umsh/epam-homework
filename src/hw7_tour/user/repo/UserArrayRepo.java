package hw7_tour.user.repo;

import hw7_tour.base.ArrayRepo;
import hw7_tour.user.AbstractUser;

import static hw7_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo extends ArrayRepo<AbstractUser> implements UserRepo {
    public UserArrayRepo() {
        dataArray = users;
    }
}
