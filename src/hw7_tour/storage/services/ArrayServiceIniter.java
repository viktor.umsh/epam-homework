package hw7_tour.storage.services;

import hw7_tour.base.BaseService;
import hw7_tour.base.Entities;
import hw7_tour.city.repo.CityRepo;
import hw7_tour.city.repo.impl.CityArrayRepo;
import hw7_tour.city.service.CityDefaultService;
import hw7_tour.country.repo.CountryRepo;
import hw7_tour.country.repo.impl.CountryArrayRepo;
import hw7_tour.country.service.CountryDefaultService;
import hw7_tour.hotel.repo.HotelRepo;
import hw7_tour.hotel.repo.impl.HotelArrayRepo;
import hw7_tour.hotel.service.HotelDefaultService;
import hw7_tour.order.repo.OrderRepo;
import hw7_tour.order.repo.impl.OrderArrayRepo;
import hw7_tour.order.service.OrderDefaultService;
import hw7_tour.tourProvider.repo.TourProviderRepo;
import hw7_tour.tourProvider.repo.impl.TourProviderArrayRepo;
import hw7_tour.tourProvider.service.TourProviderDefaultService;
import hw7_tour.user.repo.UserArrayRepo;
import hw7_tour.user.repo.UserRepo;
import hw7_tour.user.service.UserDefaultService;

import java.util.HashMap;

public class ArrayServiceIniter {
    public static HashMap<Entities, BaseService> initArrayServices() {
        HashMap<Entities, BaseService> servicesMap = new HashMap<>();
        for (Entities entity : Entities.values()) {
            switch (entity) {
                case USER:
                    UserRepo userRepo = new UserArrayRepo();
                    servicesMap.put(entity, new UserDefaultService(userRepo));
                    break;

                case CITY:
                    CityRepo cityRepo = new CityArrayRepo();
                    servicesMap.put(entity, new CityDefaultService(cityRepo));
                    break;

                case COUNTRY:
                    CountryRepo countryRepo = new CountryArrayRepo();
                    servicesMap.put(entity, new CountryDefaultService(countryRepo));
                    break;

                case HOTEL:
                    HotelRepo hotelRepo = new HotelArrayRepo();
                    servicesMap.put(entity, new HotelDefaultService(hotelRepo));
                    break;

                case ORDER:
                    OrderRepo orderRepo = new OrderArrayRepo();
                    servicesMap.put(entity, new OrderDefaultService(orderRepo));
                    break;

                case TOUR_PROVIDER:
                    TourProviderRepo tourProviderRepo = new TourProviderArrayRepo();
                    servicesMap.put(entity, new TourProviderDefaultService(tourProviderRepo));
                    break;
            }
        }
        return servicesMap;
    }

}
