package hw7_tour.storage.services;

import hw7_tour.base.BaseService;
import hw7_tour.base.Entities;
import hw7_tour.city.service.CityService;
import hw7_tour.country.service.CountryService;
import hw7_tour.hotel.service.HotelService;
import hw7_tour.order.service.OrderService;
import hw7_tour.storage.StorageType;
import hw7_tour.storage.data.DataStoreIniter;
import hw7_tour.tourProvider.service.TourProviderService;
import hw7_tour.user.service.UserService;

import java.util.HashMap;

public class ServicesStorage {
    private static StorageType storageType = null;
    private static HashMap<Entities, BaseService> servicesMap = new HashMap<>();

    public static void setStorageType(StorageType storageType) {
        ServicesStorage.storageType = storageType;
        DataStoreIniter.init(storageType);
        servicesMap = ListServiceIniter.initListServices();
        initService();
        setInsertedInitServices();
    }

    private static void setInsertedInitServices() {
        for (BaseService baseService : servicesMap.values()) {
            baseService.initInsertedServices();
        }
    }

    private static void initService() {
        switch (storageType) {
            case LIST:
                servicesMap = ListServiceIniter.initListServices();
                break;
            case ARRAY:
                servicesMap = ArrayServiceIniter.initArrayServices();
                break;
        }
    }

    public static BaseService getService(Entities entity) {
        return servicesMap.get(entity);
    }

    public static CountryService getCountryService() {
        return (CountryService) getService(Entities.COUNTRY);
    }

    public static CityService getCityService() {
        return (CityService) getService(Entities.CITY);
    }

    public static HotelService getHotelService() {
        return (HotelService) getService(Entities.HOTEL);
    }

    public static TourProviderService getTourProviderService() {
        return (TourProviderService) getService(Entities.TOUR_PROVIDER);
    }

    public static OrderService getOrderService() {
        return (OrderService) getService(Entities.ORDER);
    }

    public static UserService getUserService() {
        return (UserService) getService(Entities.USER);
    }

    public static StorageType getStorageType() {
        return storageType;
    }
}

