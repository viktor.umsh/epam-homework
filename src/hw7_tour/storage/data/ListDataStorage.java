package hw7_tour.storage.data;

import hw7_tour.city.City;
import hw7_tour.country.Country;
import hw7_tour.hotel.Hotel;
import hw7_tour.order.Order;
import hw7_tour.statistic.Statistic;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.user.AbstractUser;

import java.util.ArrayList;
import java.util.List;

public class ListDataStorage {
    public static List<Country> countries;
    public static List<City> cities;
    public static List<Hotel> hotels;
    public static List<TourProvider> tourProviders;
    public static List<Order> orders;
    public static List<AbstractUser> users;
    public static List<Statistic> statistics;

    static void initStore() {
        countries = new ArrayList<Country>();
        cities = new ArrayList<City>();
        hotels = new ArrayList<Hotel>();
        tourProviders = new ArrayList<TourProvider>();
        orders = new ArrayList<Order>();
        users = new ArrayList<AbstractUser>();
        statistics = new ArrayList<Statistic>();
        users = new ArrayList<AbstractUser>();
    }

}
