package hw7_tour.storage.data;

import hw7_tour.city.City;
import hw7_tour.country.Country;
import hw7_tour.hotel.Hotel;
import hw7_tour.order.Order;
import hw7_tour.statistic.Statistic;
import hw7_tour.tourProvider.TourProvider;
import hw7_tour.user.AbstractUser;
import hw7_tour.utils.Array;

public class ArrayDataStore {
    public static Array<Country> countries;
    public static Array<City> cities;
    public static Array<Hotel> hotels;
    public static Array<TourProvider> tourProviders;
    public static Array<Order> orders;
    public static Array<AbstractUser> users;
    public static Array<Statistic> statistics;


    static void initStore() {
        countries = new Array<Country>();
        cities = new Array<City>();
        hotels = new Array<Hotel>();
        tourProviders = new Array<TourProvider>();
        orders = new Array<Order>();
        users = new Array<AbstractUser>();
        statistics = new Array<Statistic>();
    }
}
