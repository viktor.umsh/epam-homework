package hw7_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
