package hw7_tour.order.repo.impl;

import hw7_tour.base.ArrayRepo;
import hw7_tour.order.Order;
import hw7_tour.order.repo.OrderRepo;

import static hw7_tour.storage.data.ArrayDataStore.orders;

public class OrderArrayRepo extends ArrayRepo<Order> implements OrderRepo {
    public OrderArrayRepo() {
        dataArray = orders;
    }
}
