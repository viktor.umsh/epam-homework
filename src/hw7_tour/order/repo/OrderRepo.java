package hw7_tour.order.repo;

import hw7_tour.base.BaseRepo;
import hw7_tour.order.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
