package hw7_tour.order;

import hw7_tour.base.IdentifiableAndNamed;
import hw7_tour.hotel.Hotel;

import java.util.Objects;

public class Order extends IdentifiableAndNamed {
    private static long lastId = 0;

    private long price;
    private Hotel hotel;

    public Order(long price, Hotel hotel) {
        this.price = price;
        this.hotel = hotel;
        createId();
    }

    @Override
    protected void createId() {
        this.id = lastId;
        Order.lastId++;
    }

    public long getPrice() {
        return price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return price == order.price &&
                Objects.equals(hotel, order.hotel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, hotel);
    }

    @Override
    public String toString() {
        return "Order{" +
                "price=" + price +
                ", hotel=" + hotel +
                ", id=" + id +
                '}';
    }
}
