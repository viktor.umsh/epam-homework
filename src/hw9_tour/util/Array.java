package hw9_tour.util;

import java.util.Iterator;

public class Array<Type extends Object> implements Iterable<Type> {
    private final int START_SIZE = 10;
    private final double upsize = 1.8;
    private Object[] array;
    private int currentMaxSize;
    private int lastNumber = 0;

    public Array() {
        array = new Object[START_SIZE];
        currentMaxSize = START_SIZE;
    }

    public void add(Type data) {
        array[lastNumber] = data;
        lastNumber++;
        if (lastNumber == currentMaxSize) {
            upsize();
        }
    }

    public Type get(int index) {
        if (index < lastNumber) {
            return (Type) array[index];
        } else {
            return null;
        }
    }

    public Type remove(int index) {
        if (index < lastNumber) {
            Type removed = (Type) array[index];
            for (int i = index; i < lastNumber; i++) {
                array[i] = array[i + 1];
            }
            lastNumber--;
            return removed;
        } else {
            return null;
        }
    }

    public boolean remove(Type something) {
        if (something == null) {
            return false;
        }
        for (int i = 0; i < lastNumber; i++) {
            if (something.equals(array[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return lastNumber;
    }

    private void upsize() {
        Object[] newArray = new Object[(int) (upsize * currentMaxSize)];
        for (int i = 0; i < lastNumber; i++) {
            newArray[i] = array[i];
        }
        currentMaxSize *= upsize;
        array = newArray;
    }

    @Override
    public Iterator<Type> iterator() {
        return new Iterator<Type>() {
            private int current = 0;

            @Override
            public boolean hasNext() {
                return (current < lastNumber);
            }

            @Override
            public Type next() {
                return (Type) array[current++];
            }
        };
    }
}
