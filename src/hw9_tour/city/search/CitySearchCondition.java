package hw9_tour.city.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.city.domain.City;

public interface CitySearchCondition extends BaseSearchCondition<City> {
}
