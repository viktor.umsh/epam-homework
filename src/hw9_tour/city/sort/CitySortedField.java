package hw9_tour.city.sort;

import hw9_tour.base.sort.BaseField;

public enum CitySortedField implements BaseField {
    NAME,
    COUNTRY,
    CLIMATE
}

