package hw9_tour.city.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.city.domain.City;

public interface CityRepo extends BaseRepo<City> {
}
