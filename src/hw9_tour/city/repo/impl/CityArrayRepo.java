package hw9_tour.city.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.city.domain.City;
import hw9_tour.city.repo.CityIdGenerator;
import hw9_tour.city.repo.CityRepo;

import static hw9_tour.storage.data.ArrayDataStore.cities;

public class CityArrayRepo extends ArrayRepo<City> implements CityRepo {
    public CityArrayRepo() {
        dataArray = cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }
}
