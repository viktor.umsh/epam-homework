package hw9_tour.city.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.city.domain.City;
import hw9_tour.city.repo.CityIdGenerator;
import hw9_tour.city.repo.CityRepo;
import hw9_tour.storage.data.SetDataStore;

public class CitySetRepo extends SetRepo<City> implements CityRepo {
    public CitySetRepo() {
        dataSet = SetDataStore.cities;
    }

    @Override
    protected long generateNextId() {
        return CityIdGenerator.nextId();
    }

}
