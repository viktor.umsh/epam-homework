package hw9_tour.city.repo.impl;

import hw9_tour.base.repo.BaseIdGenerator;
import hw9_tour.base.repo.ListRepo;
import hw9_tour.city.domain.City;
import hw9_tour.city.repo.CityRepo;

import static hw9_tour.storage.data.ListDataStorage.cities;

public class CityListRepo extends ListRepo<City> implements CityRepo {
    public CityListRepo() {
        dataList = cities;
    }

    @Override
    protected long generateNextId() {
        return BaseIdGenerator.nextId();
    }

}
