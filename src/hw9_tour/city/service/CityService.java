package hw9_tour.city.service;

import hw9_tour.base.service.BaseService;
import hw9_tour.city.domain.City;

public abstract class CityService extends BaseService<City> {
}
