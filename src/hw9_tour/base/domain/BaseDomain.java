package hw9_tour.base.domain;

public abstract class BaseDomain {
    protected long id;
    protected String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public abstract void update(BaseDomain something);
}
