package hw9_tour.base.search;

public interface BaseSearchCondition<Type> {
    boolean isFit(Type something);
}
