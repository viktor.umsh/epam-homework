package hw9_tour.base.repo;

public abstract class BaseIdGenerator {
    private static long lastId = 0;

    public static long nextId() {
        lastId++;
        return lastId;
    }
}
