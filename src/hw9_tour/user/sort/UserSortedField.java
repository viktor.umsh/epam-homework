package hw9_tour.user.sort;

import hw9_tour.base.sort.BaseField;

public enum UserSortedField implements BaseField {
    NAME,
    CITY,
    COUNTRY
}
