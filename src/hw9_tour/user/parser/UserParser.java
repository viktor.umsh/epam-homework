package hw9_tour.user.parser;

import hw9_tour.base.parser.BasicParser;
import hw9_tour.city.domain.City;
import hw9_tour.city.service.CityService;
import hw9_tour.storage.services.ServicesStorage;
import hw9_tour.user.domain.RandomMan;
import hw9_tour.user.service.UserService;

public class UserParser extends BasicParser {
    private CityService cityMemoryService = ServicesStorage.getCityService();
    private UserService userMemoryService = ServicesStorage.getUserService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 2) {
            City city = cityMemoryService.findByName(args[1]);
            userMemoryService.add(new RandomMan(args[0], city));
        }
    }
}
