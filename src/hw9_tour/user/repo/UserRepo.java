package hw9_tour.user.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.user.domain.AbstractUser;

public interface UserRepo extends BaseRepo<AbstractUser> {
}
