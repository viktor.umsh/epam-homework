package hw9_tour.user.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.user.domain.AbstractUser;
import hw9_tour.user.repo.UserIdGenerator;
import hw9_tour.user.repo.UserRepo;

import static hw9_tour.storage.data.ArrayDataStore.users;

public class UserArrayRepo extends ArrayRepo<AbstractUser> implements UserRepo {
    public UserArrayRepo() {
        dataArray = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
