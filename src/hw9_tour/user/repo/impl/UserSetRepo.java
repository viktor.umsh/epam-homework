package hw9_tour.user.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.storage.data.SetDataStore;
import hw9_tour.user.domain.AbstractUser;
import hw9_tour.user.repo.UserIdGenerator;
import hw9_tour.user.repo.UserRepo;

public class UserSetRepo extends SetRepo<AbstractUser> implements UserRepo {
    public UserSetRepo() {
        dataSet = SetDataStore.users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
