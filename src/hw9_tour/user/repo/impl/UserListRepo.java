package hw9_tour.user.repo.impl;

import hw9_tour.base.repo.ListRepo;
import hw9_tour.user.domain.AbstractUser;
import hw9_tour.user.repo.UserIdGenerator;
import hw9_tour.user.repo.UserRepo;

import static hw9_tour.storage.data.ListDataStorage.users;

public class UserListRepo extends ListRepo<AbstractUser> implements UserRepo {
    public UserListRepo() {
        dataList = users;
    }

    @Override
    protected long generateNextId() {
        return UserIdGenerator.nextId();
    }
}
