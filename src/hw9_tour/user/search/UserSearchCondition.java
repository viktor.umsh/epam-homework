package hw9_tour.user.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.user.domain.AbstractUser;

public interface UserSearchCondition extends BaseSearchCondition<AbstractUser> {
}
