package hw9_tour.user.domain;

import hw9_tour.city.domain.City;
import hw9_tour.country.domain.Country;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.order.domain.Order;

import java.util.List;

public class RichMan extends AbstractUser {

    public RichMan(String name, City city) {
        super(name, city);
    }

    @Override
    public List<Country> chooseCountries(List<Country> countries) {
        return null;
    }

    @Override
    public List<City> chooseCities(List<City> cities) {
        return null;
    }

    @Override
    public List<Hotel> chooseHotels(List<Hotel> hotels) {
        return null;
    }

    @Override
    public Order chooseTour(List<Order> orders) {
        return null;
    }
}
