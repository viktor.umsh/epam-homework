package hw9_tour.user.service;

import hw9_tour.base.service.BaseService;
import hw9_tour.user.domain.AbstractUser;

public abstract class UserService extends BaseService<AbstractUser> {
}
