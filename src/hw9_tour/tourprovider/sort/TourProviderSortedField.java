package hw9_tour.tourprovider.sort;

import hw9_tour.base.sort.BaseField;

public enum TourProviderSortedField implements BaseField {
    NAME,
    SERVICE_COST
}
