package hw9_tour.tourprovider.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.tourprovider.domain.TourProvider;

public interface TourProviderRepo extends BaseRepo<TourProvider> {
}
