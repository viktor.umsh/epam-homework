package hw9_tour.tourprovider.repo.impl;

import hw9_tour.base.repo.ListRepo;
import hw9_tour.tourprovider.domain.TourProvider;
import hw9_tour.tourprovider.repo.TourProviderIdGenerator;
import hw9_tour.tourprovider.repo.TourProviderRepo;

import static hw9_tour.storage.data.ListDataStorage.tourProviders;

public class TourProviderListRepo extends ListRepo<TourProvider> implements TourProviderRepo {
    public TourProviderListRepo() {
        dataList = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
