package hw9_tour.tourprovider.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.storage.data.SetDataStore;
import hw9_tour.tourprovider.domain.TourProvider;
import hw9_tour.tourprovider.repo.TourProviderIdGenerator;
import hw9_tour.tourprovider.repo.TourProviderRepo;

public class TourProviderSetRepo extends SetRepo<TourProvider> implements TourProviderRepo {
    public TourProviderSetRepo() {
        dataSet = SetDataStore.tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
