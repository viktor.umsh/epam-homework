package hw9_tour.tourprovider.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.tourprovider.domain.TourProvider;
import hw9_tour.tourprovider.repo.TourProviderIdGenerator;
import hw9_tour.tourprovider.repo.TourProviderRepo;

import static hw9_tour.storage.data.ArrayDataStore.tourProviders;


public class TourProviderArrayRepo extends ArrayRepo<TourProvider> implements TourProviderRepo {
    public TourProviderArrayRepo() {
        dataArray = tourProviders;
    }

    @Override
    protected long generateNextId() {
        return TourProviderIdGenerator.nextId();
    }
}
