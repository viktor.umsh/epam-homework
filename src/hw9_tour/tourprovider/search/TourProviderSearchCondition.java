package hw9_tour.tourprovider.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.tourprovider.domain.TourProvider;

public interface TourProviderSearchCondition extends BaseSearchCondition<TourProvider> {

}
