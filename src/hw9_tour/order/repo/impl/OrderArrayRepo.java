package hw9_tour.order.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.order.domain.Order;
import hw9_tour.order.repo.OrderIdGenerator;
import hw9_tour.order.repo.OrderRepo;

import static hw9_tour.storage.data.ArrayDataStore.orders;

public class OrderArrayRepo extends ArrayRepo<Order> implements OrderRepo {
    public OrderArrayRepo() {
        dataArray = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
