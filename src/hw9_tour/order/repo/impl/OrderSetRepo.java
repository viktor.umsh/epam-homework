package hw9_tour.order.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.order.domain.Order;
import hw9_tour.order.repo.OrderIdGenerator;
import hw9_tour.order.repo.OrderRepo;
import hw9_tour.storage.data.SetDataStore;

public class OrderSetRepo extends SetRepo<Order> implements OrderRepo {
    public OrderSetRepo() {
        dataSet = SetDataStore.orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
