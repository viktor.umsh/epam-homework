package hw9_tour.order.repo.impl;

import hw9_tour.base.repo.ListRepo;
import hw9_tour.order.domain.Order;
import hw9_tour.order.repo.OrderIdGenerator;
import hw9_tour.order.repo.OrderRepo;

import static hw9_tour.storage.data.ListDataStorage.orders;

public class OrderListRepo extends ListRepo<Order> implements OrderRepo {
    public OrderListRepo() {
        dataList = orders;
    }

    @Override
    protected long generateNextId() {
        return OrderIdGenerator.nextId();
    }
}
