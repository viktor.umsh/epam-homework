package hw9_tour.order.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.order.domain.Order;

public interface OrderRepo extends BaseRepo<Order> {
}
