package hw9_tour.order.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.order.domain.Order;

public interface OrderSearchCondition extends BaseSearchCondition<Order> {
}
