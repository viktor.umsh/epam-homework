package hw9_tour.order.sort;

import hw9_tour.base.sort.BaseField;

public enum OrderSortedField implements BaseField {
    PRICE,
    HOTEL,
    CITY
}
