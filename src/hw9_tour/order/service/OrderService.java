package hw9_tour.order.service;

import hw9_tour.base.service.BaseService;
import hw9_tour.order.domain.Order;

public abstract class OrderService extends BaseService<Order> {
}
