package hw9_tour.hotel.domain;

import hw9_tour.base.domain.BaseDomain;
import hw9_tour.city.domain.City;

import java.util.Objects;

public class Hotel extends BaseDomain {
    private final String name;
    private int stars;
    private long price;
    private City city;

    public Hotel(String name, int stars, long price, City city) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(BaseDomain something) {
        if (!(something instanceof Hotel) || this == something || this.equals(something)) {
            return;
        }
        Hotel someHotel = (Hotel) something;
        if (someHotel.stars != 0) {
            this.stars = someHotel.stars;
        }
        if (someHotel.price != 0) {
            this.price = someHotel.price;
        }
        if (someHotel.city != null) {
            this.city = someHotel.city;
        }
    }

    public int getStars() {
        return stars;
    }

    public long getPrice() {
        return price;
    }

    public City getCity() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return stars == hotel.stars &&
                price == hotel.price &&
                Objects.equals(name, hotel.name) &&
                Objects.equals(city, hotel.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, stars, price, city);
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "name='" + name + '\'' +
                ", stars=" + stars +
                ", price=" + price +
                ", city=" + city +
                ", id=" + id +
                '}';
    }
}
