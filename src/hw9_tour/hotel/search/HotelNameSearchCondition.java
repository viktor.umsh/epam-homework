package hw9_tour.hotel.search;

import hw9_tour.hotel.domain.Hotel;

public class HotelNameSearchCondition implements HotelSearchCondition {
    private String namePattern;

    public HotelNameSearchCondition(String namePattern) {
        this.namePattern = namePattern;
    }

    @Override
    public boolean isFit(Hotel hotel) {
        if (hotel.getName() == null) {
            return false;
        }
        return hotel.getName().contains(namePattern);
    }
}
