package hw9_tour.hotel.search;

import hw9_tour.country.domain.Country;
import hw9_tour.hotel.domain.Hotel;

public class HotelCountryHotelSearchCondition implements HotelSearchCondition {
    private Country soughtCountry;

    public HotelCountryHotelSearchCondition(Country soughtCountry) {
        this.soughtCountry = soughtCountry;
    }

    @Override
    public boolean isFit(Hotel hotel) {
        if (hotel.getCity() == null) {
            return false;
        }
        if (hotel.getCity().getCountry() == null) {
            return false;
        }
        return hotel.getCity().getCountry().equals(soughtCountry);
    }
}
