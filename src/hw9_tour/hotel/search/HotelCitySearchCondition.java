package hw9_tour.hotel.search;

import hw9_tour.city.domain.City;
import hw9_tour.hotel.domain.Hotel;

public class HotelCitySearchCondition implements HotelSearchCondition {
    private City soughtCity;

    public HotelCitySearchCondition(City soughtCity) {
        this.soughtCity = soughtCity;
    }

    @Override
    public boolean isFit(Hotel hotel) {
        if (hotel.getCity() == null) {
            return false;
        }
        return hotel.getCity().equals(soughtCity);
    }
}
