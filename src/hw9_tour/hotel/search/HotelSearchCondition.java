package hw9_tour.hotel.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.hotel.domain.Hotel;

public interface HotelSearchCondition extends BaseSearchCondition<Hotel> {
}
