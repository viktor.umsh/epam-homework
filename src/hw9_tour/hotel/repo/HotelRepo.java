package hw9_tour.hotel.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.hotel.domain.Hotel;

public interface HotelRepo extends BaseRepo<Hotel> {
}
