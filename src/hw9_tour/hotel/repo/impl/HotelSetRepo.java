package hw9_tour.hotel.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.hotel.repo.HotelIdGenerator;
import hw9_tour.hotel.repo.HotelRepo;
import hw9_tour.storage.data.SetDataStore;

public class HotelSetRepo extends SetRepo<Hotel> implements HotelRepo {
    public HotelSetRepo() {
        dataSet = SetDataStore.hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
