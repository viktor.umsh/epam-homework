package hw9_tour.hotel.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.hotel.repo.HotelIdGenerator;
import hw9_tour.hotel.repo.HotelRepo;

import static hw9_tour.storage.data.ArrayDataStore.hotels;

public class HotelArrayRepo extends ArrayRepo<Hotel> implements HotelRepo {
    public HotelArrayRepo() {
        dataArray = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
