package hw9_tour.hotel.repo.impl;

import hw9_tour.base.repo.ListRepo;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.hotel.repo.HotelIdGenerator;
import hw9_tour.hotel.repo.HotelRepo;

import static hw9_tour.storage.data.ListDataStorage.hotels;

public class HotelListRepo extends ListRepo<Hotel> implements HotelRepo {
    public HotelListRepo() {
        dataList = hotels;
    }

    @Override
    protected long generateNextId() {
        return HotelIdGenerator.nextId();
    }
}
