package hw9_tour.hotel.service;

import hw9_tour.base.service.BaseService;
import hw9_tour.hotel.domain.Hotel;

public abstract class HotelService extends BaseService<Hotel> {
}
