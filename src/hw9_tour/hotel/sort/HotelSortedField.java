package hw9_tour.hotel.sort;

import hw9_tour.base.sort.BaseField;

public enum HotelSortedField implements BaseField {
    NAME,
    COUNTRY,
    CITY,
    PRICE
}
