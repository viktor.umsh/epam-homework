package hw9_tour;

import hw9_tour.base.search.MultiSearchCondition;
import hw9_tour.base.sort.BaseField;
import hw9_tour.city.domain.City;
import hw9_tour.city.domain.Climate;
import hw9_tour.city.search.CityClimateSearchCondition;
import hw9_tour.city.search.CityCountrySearchCondition;
import hw9_tour.city.service.CityService;
import hw9_tour.city.sort.CitySortCondition;
import hw9_tour.city.sort.CitySortedField;
import hw9_tour.country.domain.Country;
import hw9_tour.country.search.CountryNameSearchCondition;
import hw9_tour.country.service.CountryService;
import hw9_tour.dataReader.ConsoleSpeacker;
import hw9_tour.dataReader.DataGenerator;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.hotel.service.HotelService;
import hw9_tour.hotel.sort.HotelSortedField;
import hw9_tour.order.domain.Order;
import hw9_tour.order.service.OrderService;
import hw9_tour.storage.EssenceType;
import hw9_tour.storage.StorageType;
import hw9_tour.storage.services.ServicesStorage;
import hw9_tour.storage.sort.SortedFields;
import hw9_tour.tourprovider.domain.TourProvider;
import hw9_tour.tourprovider.service.TourProviderService;
import hw9_tour.user.domain.AbstractUser;
import hw9_tour.user.service.UserService;

import java.io.PrintStream;
import java.util.List;

public class Application {
    private UserService userService;
    private TourProviderService tourProviderService;
    private CountryService countryService;
    private CityService cityService;
    private HotelService hotelService;
    private OrderService orderService;

    public Order UserTourProviderInteraction(TourProvider tourProvider, AbstractUser user) {
        List<Country> chosenCountries = user.chooseCountries(tourProvider.getCountries());
        List<City> chosenCities = user.chooseCities(tourProvider.getCities(chosenCountries));
        List<Hotel> chosenHotels = user.chooseHotels(tourProvider.getHotels(chosenCities));
        return user.chooseTour(tourProvider.generateOrder(user, chosenHotels));
    }

    public void trySearchCity() {
        List<Country> countryResult = countryService.search(new CountryNameSearchCondition("Russ"));
        System.out.println("Search country by string pattern: Rus");
        for (Country country : countryResult) {
            System.out.println(country);
        }
        Country searchCountry = countryResult.get(0);

        MultiSearchCondition<City> multiSearchCondition = new MultiSearchCondition<>();
        multiSearchCondition.addSearchCondition(new CityCountrySearchCondition(searchCountry));
        multiSearchCondition.addSearchCondition(new CityClimateSearchCondition(Climate.ARCTIC));
        List<City> cityResult = cityService.search(multiSearchCondition);
        System.out.println("Search city by country: Russia and climate: ARCTIC");
        for (City city : cityResult) {
            System.out.println(city);
        }
        System.out.println();
    }

    public Application(StorageType storageType) {
        ServicesStorage.setStorageType(storageType);
        initServices();
    }

    public void readDataFromFile() {
        DataGenerator generator = new DataGenerator();
        generator.readFile();
    }

    private void initServices() {
        userService = ServicesStorage.getUserService();
        tourProviderService = ServicesStorage.getTourProviderService();
        countryService = ServicesStorage.getCountryService();
        cityService = ServicesStorage.getCityService();
        hotelService = ServicesStorage.getHotelService();
        orderService = ServicesStorage.getOrderService();
    }

    public void runApplication() {
        AbstractUser vasiliy = userService.findByName("Vasiliy");
        TourProvider grandTours = tourProviderService.findByName("GrandTours");
        Order order = UserTourProviderInteraction(grandTours, vasiliy);
        System.out.println("System create order:" + order);
        System.out.println();
    }

    public void trySort() {
        CitySortCondition sortCondition = new CitySortCondition();
//        sortCondition.addField(1, CitySortedField.CLIMATE);
        sortCondition.addField(1, CitySortedField.COUNTRY);
        sortCondition.addField(2, CitySortedField.NAME);
        List<City> result = cityService.sort(sortCondition);
        for (City city : result) {
            System.out.println(city);
        }

        System.out.println("--------------------CITY SORT FIELDS BY ENUM:------------------------");
        for (BaseField some : EssenceType.CITY.getSortedFields()) {
            System.out.println(some);
        }

        System.out.println("CITY.getSortedFields contains CitySortedField.COUNTRY: " + EssenceType.CITY.getSortedFields().contains(CitySortedField.COUNTRY));
        System.out.println("CITY.getSortedFields contains HotelSortedField.COUNTRY: " + EssenceType.CITY.getSortedFields().contains(HotelSortedField.COUNTRY));

        System.out.println("--------------------CITY SORT FIELDS BY MAP:------------------------");
        for (BaseField some : SortedFields.getInstance().getFields(EssenceType.CITY)) {
            System.out.println(some);
        }
    }

    public void outAll(PrintStream out) {
        System.out.println("Application out all domain data:");
        tourProviderService.printAll(out);
        countryService.printAll(out);
        cityService.printAll(out);
        hotelService.printAll(out);
        userService.printAll(out);
        orderService.printAll(out);
    }

    public static void main(String[] args) {
        Application application = new Application(StorageType.LIST);
        ConsoleSpeacker consoleSpeacker = new ConsoleSpeacker();
        consoleSpeacker.runSpeacker();

//        System.out.println("--------------------LIST:------------------------");
//        Application application = new Application(StorageType.LIST);
//        application.readDataFromFile();
//        application.outAll(System.out);
//        application.runApplication();
//        System.out.println();
////        System.out.println("--------------------SEARCH:------------------------");
////        application.trySearchCity();
//        System.out.println("--------------------SORT:------------------------");
//        application.trySort();
//
////        System.out.println("-------------------ARRAY:------------------------");
////        application = new Application(StorageType.ARRAY);
////        application.readDataFromFile();
////        application.runApplication();
////
////        System.out.println("--------------------SET:-------------------------");
////        application = new Application(StorageType.SET);
////        application.readDataFromFile();
////        application.runApplication();

    }
}
