package hw9_tour.storage.data;

import hw9_tour.city.domain.City;
import hw9_tour.country.domain.Country;
import hw9_tour.hotel.domain.Hotel;
import hw9_tour.order.domain.Order;
import hw9_tour.statistic.Statistic;
import hw9_tour.tourprovider.domain.TourProvider;
import hw9_tour.user.domain.AbstractUser;
import hw9_tour.util.Array;

public class ArrayDataStore {
    public static Array<Country> countries;
    public static Array<City> cities;
    public static Array<Hotel> hotels;
    public static Array<TourProvider> tourProviders;
    public static Array<Order> orders;
    public static Array<AbstractUser> users;
    public static Array<Statistic> statistics;


    static void initStore() {
        countries = new Array<>();
        cities = new Array<>();
        hotels = new Array<>();
        tourProviders = new Array<>();
        orders = new Array<>();
        users = new Array<>();
        statistics = new Array<>();
    }
}
