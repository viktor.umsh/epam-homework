package hw9_tour.storage;

public enum StorageType {
    ARRAY,
    LIST,
    SET
}
