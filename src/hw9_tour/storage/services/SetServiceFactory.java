package hw9_tour.storage.services;

import hw9_tour.city.repo.CityRepo;
import hw9_tour.city.repo.impl.CitySetRepo;
import hw9_tour.city.service.CityDefaultService;
import hw9_tour.country.repo.CountryRepo;
import hw9_tour.country.repo.impl.CountrySetRepo;
import hw9_tour.country.service.CountryDefaultService;
import hw9_tour.hotel.repo.HotelRepo;
import hw9_tour.hotel.repo.impl.HotelSetRepo;
import hw9_tour.hotel.service.HotelDefaultService;
import hw9_tour.order.repo.OrderRepo;
import hw9_tour.order.repo.impl.OrderSetRepo;
import hw9_tour.order.service.OrderDefaultService;
import hw9_tour.tourprovider.repo.TourProviderRepo;
import hw9_tour.tourprovider.repo.impl.TourProviderSetRepo;
import hw9_tour.tourprovider.service.TourProviderDefaultService;
import hw9_tour.user.repo.UserRepo;
import hw9_tour.user.repo.impl.UserSetRepo;
import hw9_tour.user.service.UserDefaultService;


public class SetServiceFactory extends ServiceFactory {
    public SetServiceFactory() {
        UserRepo userRepo = new UserSetRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CitySetRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountrySetRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelSetRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderSetRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderSetRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
