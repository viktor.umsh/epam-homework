package hw9_tour.storage.services;

import hw9_tour.city.repo.CityRepo;
import hw9_tour.city.repo.impl.CityArrayRepo;
import hw9_tour.city.service.CityDefaultService;
import hw9_tour.country.repo.CountryRepo;
import hw9_tour.country.repo.impl.CountryArrayRepo;
import hw9_tour.country.service.CountryDefaultService;
import hw9_tour.hotel.repo.HotelRepo;
import hw9_tour.hotel.repo.impl.HotelArrayRepo;
import hw9_tour.hotel.service.HotelDefaultService;
import hw9_tour.order.repo.OrderRepo;
import hw9_tour.order.repo.impl.OrderArrayRepo;
import hw9_tour.order.service.OrderDefaultService;
import hw9_tour.tourprovider.repo.TourProviderRepo;
import hw9_tour.tourprovider.repo.impl.TourProviderArrayRepo;
import hw9_tour.tourprovider.service.TourProviderDefaultService;
import hw9_tour.user.repo.UserRepo;
import hw9_tour.user.repo.impl.UserArrayRepo;
import hw9_tour.user.service.UserDefaultService;


public class ArrayServiceFactory extends ServiceFactory {

    public ArrayServiceFactory() {
        UserRepo userRepo = new UserArrayRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityArrayRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryArrayRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelArrayRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderArrayRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderArrayRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
