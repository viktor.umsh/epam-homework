package hw9_tour.storage.services;

import hw9_tour.city.repo.CityRepo;
import hw9_tour.city.repo.impl.CityListRepo;
import hw9_tour.city.service.CityDefaultService;
import hw9_tour.country.repo.CountryRepo;
import hw9_tour.country.repo.impl.CountryListRepo;
import hw9_tour.country.service.CountryDefaultService;
import hw9_tour.hotel.repo.HotelRepo;
import hw9_tour.hotel.repo.impl.HotelListRepo;
import hw9_tour.hotel.service.HotelDefaultService;
import hw9_tour.order.repo.OrderRepo;
import hw9_tour.order.repo.impl.OrderListRepo;
import hw9_tour.order.service.OrderDefaultService;
import hw9_tour.tourprovider.repo.TourProviderRepo;
import hw9_tour.tourprovider.repo.impl.TourProviderListRepo;
import hw9_tour.tourprovider.service.TourProviderDefaultService;
import hw9_tour.user.repo.UserRepo;
import hw9_tour.user.repo.impl.UserListRepo;
import hw9_tour.user.service.UserDefaultService;


public class ListServiceFactory extends ServiceFactory {
    public ListServiceFactory() {
        UserRepo userRepo = new UserListRepo();
        userService = new UserDefaultService(userRepo);

        CityRepo cityRepo = new CityListRepo();
        cityService = new CityDefaultService(cityRepo);

        CountryRepo countryRepo = new CountryListRepo();
        countryService = new CountryDefaultService(countryRepo);

        HotelRepo hotelRepo = new HotelListRepo();
        hotelService = new HotelDefaultService(hotelRepo);

        OrderRepo orderRepo = new OrderListRepo();
        orderService = new OrderDefaultService(orderRepo);

        TourProviderRepo tourProviderRepo = new TourProviderListRepo();
        tourProviderService = new TourProviderDefaultService(tourProviderRepo);
    }
}
