package hw9_tour.storage;

import hw9_tour.base.sort.BaseField;
import hw9_tour.city.sort.CitySortedField;
import hw9_tour.country.sort.CountrySortedField;
import hw9_tour.hotel.sort.HotelSortedField;
import hw9_tour.order.sort.OrderSortedField;
import hw9_tour.tourprovider.sort.TourProviderSortedField;
import hw9_tour.user.sort.UserSortedField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum EssenceType {
    COUNTRY {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(CountrySortedField.values()));// Arrays.asList());
        }
    },
    CITY {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(CitySortedField.values()));
        }
    },
    HOTEL {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(HotelSortedField.values()));
        }
    },
    TOUR_PROVIDER {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(TourProviderSortedField.values()));
        }
    },
    ORDER {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(OrderSortedField.values()));
        }
    },
    USER {
        @Override
        public List<BaseField> getSortedFields() {
            return new ArrayList<BaseField>(Arrays.asList(UserSortedField.values()));
        }
    };


    public abstract List<BaseField> getSortedFields();
}
