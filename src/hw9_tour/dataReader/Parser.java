package hw9_tour.dataReader;

import hw9_tour.city.parser.CityParser;
import hw9_tour.country.parser.CountryParser;
import hw9_tour.hotel.parser.HotelParser;
import hw9_tour.order.parser.OrderParser;
import hw9_tour.tourprovider.parser.TourProviderParser;
import hw9_tour.user.parser.UserParser;

public class Parser {
    private CountryParser countryParser = new CountryParser();
    private CityParser cityParser = new CityParser();
    private HotelParser hotelParser = new HotelParser();
    private UserParser userParser = new UserParser();
    private TourProviderParser tourProviderParser = new TourProviderParser();
    private OrderParser orderParser = new OrderParser();

    public boolean parse(String whatRead, String line) {
        switch (whatRead) {
            case "Users:":
                userParser.parseString(line);
                break;
            case "Countries:":
                countryParser.parseString(line);
                break;
            case "Cities:":
                cityParser.parseString(line);
                break;
            case "Hotels:":
                hotelParser.parseString(line);
                break;
            case "TourProviders:":
                tourProviderParser.parseString(line);
                break;
            case "Orders:":
                orderParser.parseString(line);
                break;
            default:
                return false;
        }
        return true;
    }
}
