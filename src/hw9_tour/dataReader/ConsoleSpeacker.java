package hw9_tour.dataReader;

import hw9_tour.base.service.BaseService;
import hw9_tour.city.domain.City;
import hw9_tour.city.service.CityService;
import hw9_tour.city.sort.CitySortCondition;
import hw9_tour.city.sort.CitySortedField;
import hw9_tour.country.service.CountryService;
import hw9_tour.hotel.service.HotelService;
import hw9_tour.order.service.OrderService;
import hw9_tour.storage.EssenceType;
import hw9_tour.storage.services.ServicesStorage;
import hw9_tour.tourprovider.service.TourProviderService;
import hw9_tour.user.service.UserService;

import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class ConsoleSpeacker {
    private UserService userService;
    private TourProviderService tourProviderService;
    private CountryService countryService;
    private CityService cityService;
    private HotelService hotelService;
    private OrderService orderService;

    private void initServices() {
        userService = ServicesStorage.getUserService();
        tourProviderService = ServicesStorage.getTourProviderService();
        countryService = ServicesStorage.getCountryService();
        cityService = ServicesStorage.getCityService();
        hotelService = ServicesStorage.getHotelService();
        orderService = ServicesStorage.getOrderService();
    }

    public ConsoleSpeacker() {
        initServices();
    }

    private void outAll(PrintStream out) {
        System.out.println("Application out all domain data:");
        tourProviderService.printAll(out);
        countryService.printAll(out);
        cityService.printAll(out);
        hotelService.printAll(out);
        userService.printAll(out);
        orderService.printAll(out);
    }

    private void outHelp(PrintStream out) {
        out.println("Here will be help..");
    }

    private void readDataFromFile() {
        DataGenerator generator = new DataGenerator();
        generator.readFile();
    }

    public void runSpeacker() {
        Scanner in = new Scanner(System.in);
        String[] command;
        Parser parser = new Parser();
        while (true) {
            command = in.nextLine().split(" ");
            if (command[0].equals("exit")) {
                break;
            }
            if (command[0].equals("add")) {
                if (command.length == 2) {
                    if (!parser.parse(command[1], in.nextLine())) {
                        System.out.println("Incorrect added essence!");
                    }
                } else {
                    System.out.println("Command add should have argument: added essence!");
                }
                continue;
            }
            if (command[0].equals("help")) {
                outHelp(System.out);
                continue;
            }
            if (command[0].equals("outAll")) {
                outAll(System.out);
                continue;
            }
            if (command[0].equals("readFile")) {
                readDataFromFile();
                continue;
            }
            if (command[0].equals("sort")) {
                if (command.length >= 3) {
                    EssenceType essenceType = EssenceType.valueOf(command[1]);
                    BaseService service = ServicesStorage.gerService(essenceType);
                    if (EssenceType.CITY.equals(essenceType)) {
                        CitySortCondition sortCondition = new CitySortCondition();
                        sortCondition.addField(1, CitySortedField.valueOf(command[2]));
                        List<City> result = service.sort(sortCondition);
                        for (City elem : result) {
                            System.out.println(elem);
                        }
                    }
                    if (!parser.parse(command[1], in.nextLine())) {
                        System.out.println("Incorrect added essence!");
                    }
                } else {
                    System.out.println("Command should have arguments: sorted essence and fields!");
                }
            }
        }
    }
}
