package hw9_tour.country.parser;

import hw9_tour.base.parser.BasicParser;
import hw9_tour.country.domain.Country;
import hw9_tour.country.service.CountryService;
import hw9_tour.storage.services.ServicesStorage;

public class CountryParser extends BasicParser {

    private CountryService countryMemoryService = ServicesStorage.getCountryService();

    @Override
    public void parseString(String line) {
        String[] args = split(line);
        if (args.length == 1) {
            countryMemoryService.add(new Country(args[0]));
        }
    }

}
