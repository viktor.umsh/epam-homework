package hw9_tour.country.service;

import hw9_tour.base.service.BaseService;
import hw9_tour.country.domain.Country;

public abstract class CountryService extends BaseService<Country> {
}
