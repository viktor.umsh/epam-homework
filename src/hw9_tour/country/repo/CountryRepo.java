package hw9_tour.country.repo;

import hw9_tour.base.repo.BaseRepo;
import hw9_tour.country.domain.Country;

public interface CountryRepo extends BaseRepo<Country> {
}
