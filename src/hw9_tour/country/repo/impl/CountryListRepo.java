package hw9_tour.country.repo.impl;

import hw9_tour.base.repo.ListRepo;
import hw9_tour.country.domain.Country;
import hw9_tour.country.repo.CountryIdGenerator;
import hw9_tour.country.repo.CountryRepo;

import static hw9_tour.storage.data.ListDataStorage.countries;

public class CountryListRepo extends ListRepo<Country> implements CountryRepo {
    public CountryListRepo() {
        dataList = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
