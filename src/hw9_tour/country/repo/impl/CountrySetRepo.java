package hw9_tour.country.repo.impl;

import hw9_tour.base.repo.SetRepo;
import hw9_tour.country.domain.Country;
import hw9_tour.country.repo.CountryIdGenerator;
import hw9_tour.country.repo.CountryRepo;
import hw9_tour.storage.data.SetDataStore;

public class CountrySetRepo extends SetRepo<Country> implements CountryRepo {
    public CountrySetRepo() {
        dataSet = SetDataStore.countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
