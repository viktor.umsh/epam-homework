package hw9_tour.country.repo.impl;

import hw9_tour.base.repo.ArrayRepo;
import hw9_tour.country.domain.Country;
import hw9_tour.country.repo.CountryIdGenerator;
import hw9_tour.country.repo.CountryRepo;

import static hw9_tour.storage.data.ArrayDataStore.countries;

public class CountryArrayRepo extends ArrayRepo<Country> implements CountryRepo {
    public CountryArrayRepo() {
        dataArray = countries;
    }

    @Override
    protected long generateNextId() {
        return CountryIdGenerator.nextId();
    }
}
