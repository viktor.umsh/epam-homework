package hw9_tour.country.search;

import hw9_tour.base.search.BaseSearchCondition;
import hw9_tour.country.domain.Country;

public interface CountrySearchCondition extends BaseSearchCondition<Country> {
}
