package hw8;

import java.util.*;

public class DmitriyCollectionsIntersection {
    public static void main(String[] args) {
        System.out.println("------------------Own realization-----");
        demoOwnCollectionInterSection();
        System.out.println("------------------Own modified! realization-----");
        modified_DemoOwnCollectionInterSection();

        System.out.println("------------------ Not True Result 1-----");
        demoEmbeddedLangFeatureToCalcIntersection();

        System.out.println("------------------Not True Result 2-----");
        t2DemoEmbeddedLangFeatureToCalcIntersection();
    }

    private static void demoOwnCollectionInterSection() {
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "e", "f", "g"));
//        Set<String> set1 = new HashSet<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
//        Set<String> set2 = new HashSet<>(Arrays.asList("a", "a", "b", "c", "c", "e", "f", "g"));
        Set<String> intersection = new HashSet<>();
        Set<String> setWithList1Elems = new HashSet<>(list1);
        for (String str : list2) {
            boolean intersectedElement = !setWithList1Elems.add(str);
            if (intersectedElement) {
                intersection.add(str);
            }
        }

        printCollection(intersection);
    }


    private static void modified_DemoOwnCollectionInterSection() {
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "e", "f", "g"));
        List<String> intersection = new ArrayList<>();
        Set<String> setWithList1Elems = new HashSet<>(list1);
        for (String str : list2) {
            boolean intersectedElement = !setWithList1Elems.add(str);
            if (intersectedElement) {
                intersection.add(str);
            }
        }

        printCollection(intersection);
    }

    private static void demoEmbeddedLangFeatureToCalcIntersection() {
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "a", "a", "a", "b", "c", "c", "e", "f", "g"));
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        list1.retainAll(list2);
        printCollection(list1);
    }

    private static void t2DemoEmbeddedLangFeatureToCalcIntersection() {
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "a", "a", "a", "b", "c", "c", "e", "f", "g"));
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        list1.retainAll(list2);
        printCollection(list1);
    }

    private static void printCollection(Collection<String> collection) {
        for (String str : collection) {
            System.out.println(str);
        }
    }
}

/**
 * System.out:
 * <p>
 * ------------------Own realization-----
 * a
 * b
 * c
 * ------------------Own modified realization-----
 * a
 * a
 * b
 * c
 * c
 * c
 * ------------------ Not True Result 1-----
 * a
 * a
 * a
 * a
 * a
 * b
 * c
 * c
 * ------------------Not True Result 2-----
 * a
 * a
 * b
 * c
 * c
 * c
 * <p>
 * <p>
 * *********************************
 * Should be:           *
 * *********************************
 * a
 * a
 * b
 * c
 * c
 */