package hw8;

import java.util.*;

public class Intersector {

    private static List<String> containsIntersection(List<String> o1, List<String> o2) {
        List<String> result = new ArrayList<>();
        for (String cur1 : o1) {
            if (o2.contains(cur1)) {
                result.add(cur1);
            }
        }
        return result;
    }

    private static List<String> SetIntersection(List<String> o1, List<String> o2) {
        Set<String> intersection = new HashSet<>();
        Set<String> merger = new HashSet<>(o1);
        for (String cur2 : o2) {
            if (merger.contains(cur2)) {
                intersection.add(cur2);
            }
        }
        return new ArrayList<>(intersection);
    }

    private static List<String> trueIntersection(List<String> o1, List<String> o2) {
        List<String> intersection = new ArrayList<>();
        LinkedList<String> merger = new LinkedList<>(o1);
        for (String cur2 : o2) {
            if (merger.contains(cur2)) {
                intersection.add(cur2);
                merger.remove(cur2);
            }
        }
        return intersection;
    }

    private static List<String> canNotBeTrueRetainIntersection(List<String> o1, List<String> o2) {
        List<String> list1 = new ArrayList<>(o1);
        List<String> list2 = new ArrayList<>(o2);
        list2.retainAll(list1);
        list1.retainAll(list1);

        System.out.println("============== lists: ===============");
        printList(list1);
        printList(list2);

        List<String> list3 = new ArrayList<>(o1);
        List<String> list4 = new ArrayList<>(o2);
        list2.removeAll(list1);
        System.out.println("============== lists: ===============");
        printList(list1);
        printList(list2);

        return null;
    }

    private static void demo(List<String> list1, List<String> list2) {
        System.out.println("============== Input lists: ===============");
        printList(list1);
        printList(list2);

        List<String> result;
        System.out.println("============== Intersection with contains method: ===============");
        result = Intersector.containsIntersection(list1, list2);
        printList(result);

        System.out.println("============== Intersection with Set: ===============");
        result = Intersector.SetIntersection(list2, list1);
        printList(result);


        System.out.println("============== list1 retain list2: ===============");
        List<String> c1list1 = new ArrayList<>(list1);
        List<String> c1list2 = new ArrayList<>(list2);
        c1list1.retainAll(c1list2);
        printList(c1list1);

        System.out.println("============== list2 retain list1: ===============");
        List<String> c2list1 = new ArrayList<>(list1);
        List<String> c2list2 = new ArrayList<>(list2);
        c2list2.retainAll(c2list1);
        printList(c2list2);

        System.out.println("============== True intersection without Set: ===============");
        result = Intersector.trueIntersection(list2, list1);
        printList(result);

//        System.out.println("============== Input lists: ===============");
//        printList(list1);
//        printList(list2);
    }

    private static void test1() {
        System.out.println("============== Test1: Retain method can work: ===============");
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "e", "f", "g"));
        demo(list1, list2);
        System.out.println();
    }


    private static void test2() {
        System.out.println("============== Test2: Retain method can not work: ===============");
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "a", "b", "c", "c", "c", "d"));
        List<String> list2 = new ArrayList<>(Arrays.asList("a", "a", "a", "a", "a", "b", "c", "c", "e", "f", "g"));
        demo(list1, list2);
        System.out.println();

    }

    private static void printList(List<String> result) {
        for (String cur : result) {
            System.out.print(cur + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        test1();
        test2();
    }
}


/**
 * System.out:
 * <p>
 * <p>
 * ============== Input lists: ===============
 * a a b c c c d
 * a a b c c e f g
 * ============== Intersection with contains method: ===============
 * a a b c c c
 * ============== Intersection with Set: ===============
 * a b c
 * ============== list1 retain list2: ===============
 * a a b c c c
 * ============== list2 retain list1: ===============
 * a a b c c
 * ============== True intersection without Set: ===============
 * a a b c c
 * <p>
 * ============== Test2: Retain method can not work: ===============
 * ============== Input lists: ===============
 * a a b c c c d
 * a a a a a b c c e f g
 * ============== Intersection with contains method: ===============
 * a a b c c c
 * ============== Intersection with Set: ===============
 * a b c
 * ============== list1 retain list2: ===============
 * a a b c c c
 * ============== list2 retain list1: ===============
 * a a a a a b c c
 * ============== True intersection without Set: ===============
 * a a b c c
 */